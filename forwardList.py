import sys
import math
import os
import argparse

DNA={"A":0, "C":1, "G":2, "T":3}
AAs = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
AA = {'A':0, 'R':1, 'N':2, 'D':3, 'C':4, 'Q':5, 'E':6, 'G':7, 'H':8, 'I':9, 'L':10, 'K':11, 'M':12, 'F':13, 'P':14, 'S':15, 'T':16, 'W':17, 'Y':18, 'V':19}
LGfreqs=[0.079066, 0.055941, 0.041977, 0.053052, 0.012937, 0.040767, 0.071586, 0.057337, 0.022355, 0.062157, 0.099081, 0.064600, 0.022951, 0.042302, 0.044040, 0.061197, 0.053287, 0.012066, 0.034155, 0.069147 ]


parser = argparse.ArgumentParser(description='LK of a list of sequence pairs.')
parser.add_argument("-e","--epsilon", help="log-probability threshold for discarding DP matrix cells (when using such approximation)", type=float, default=-15.0)
parser.add_argument("-s","--seqsFile", help="file with the sequence pairs", default="")
parser.add_argument("-r","--resFile", help="file where to write the result", default="")
parser.add_argument("--transProbs", help="transProbs", nargs=9, type=float)
parser.add_argument("--stateProbs", help="stateProbs", nargs=3, type=float)
parser.add_argument("--probs1", help="probs1", nargs=400, type=float)
parser.add_argument("--fr", help="fr", nargs=20, type=float, default=LGfreqs)
parser.add_argument("--alpha", help="alphabet, DNA or AA (default)", default="AA")
parser.add_argument("--probsDNA", help="probsDNA", nargs=16, type=float)
parser.add_argument("--frDNA", help="frDNA", nargs=4, type=float, default=LGfreqs)
args = parser.parse_args()


epsilon=args.epsilon
alphabet=args.alpha
if alphabet=="AA":
	DNAl=AA
	nAlph=20
	probs1=args.probs1
	pi=args.frDNA
else:
	DNAl=DNA
	nAlph=4
	probs1=args.probsDNA
	pi=args.fr
transProbs=args.transProbs
#print(transProbs)
transProbs=[transProbs[0:3],transProbs[3:6],transProbs[6:9]]
#print(transProbs)
stateProbs=args.stateProbs
#print(stateProbs)
#print(pi)

phyl={}#phylogenetic probabilities (emission probabilities)
for i in DNAl.keys():
	for j in DNAl.keys():
		phyl[(i,j)]=pi[DNAl[i]]*probs1[DNAl[i]*nAlph+DNAl[j]]
pis={}
for i in DNAl.keys():
	pis[i]=pi[DNAl[i]]
	
Ls1=[]
Ls2=[]
file=open(args.seqsFile)
line=file.readline()
while line!="" and line!="\n":
	Ls1.append(line.replace("\n",""))
	line=file.readline()
	Ls2.append(line.replace("\n",""))
	line=file.readline()
file.close()



#definitions for more buoyant floats (Lunter 2007, Bioinformatics)
cBFloatRange = 20282409603651670423947251286016.0  #2.03e+31; 2^104
cBFloatRangeInv = 1.0/cBFloatRange
cBFloatRangeSqrt    = 1.0e+18          # Value between square root of the exponent, and the exponent
cBFloatRangeInvSqrt = 1.0e-18          # Square of this should still be representable, with full mantissa!
logcBFloatRange     = math.log(cBFloatRange)
cBFloatConvTableSize       = 100               # This includes many zero entries, it makes additions a bit faster
aConversionLookup = []
iBFM = 1.0
for i in range(cBFloatConvTableSize):
	aConversionLookup.append(iBFM)
	iBFM *= cBFloatRangeInv

def normBfloats(a):
	mantissa=a[0]
	expo=a[1]
	if mantissa<0.0:
		print("Error, negative mantissa")
		print(a)
		return (0.0,float('-inf'))
	if expo==float('-inf'):
		return (0.0,float('-inf'))
	if mantissa==0.0:
		print("Error, zero mantissa before normalization")
		print(a)
		return (0.0,float('-inf'))
	while (mantissa > cBFloatRangeSqrt):
		mantissa *= cBFloatRangeInv
		expo+=1
	while (mantissa < cBFloatRangeInvSqrt):
		mantissa *= cBFloatRange
		expo-=1
	if mantissa==0.0:
		print("Error, zero mantissa after normalization")
		print(a)
		return (0.0,float('-inf'))
	return (mantissa,expo)
	

def showBfloat( a ):
	return a[1]*logcBFloatRange+math.log(a[0])

def addBfloats(a, b) :
	if a[1]>b[1]:
		if a[1]>=(b[1]+cBFloatConvTableSize):
			return a
		else:
			return (a[0] + b[0]*aConversionLookup[ a[1] - b[1] ], a[1])
	if a[1]<=(b[1]-cBFloatConvTableSize):
		return b
	else:
		return (b[0] + a[0]*aConversionLookup[ b[1] - a[1] ], b[1])


def biggerBfloat(a,b):
	if a[1] > b[1]:
		if a[1] >= (b[1] + cBFloatConvTableSize):
			return True
		else:
			return a[0] > (b[0] * aConversionLookup[ a[1] - b[1] ])
	if a[1] <= (b[1] - cBFloatConvTableSize):
		return False
	else:
		return (a[0] * aConversionLookup[ b[1] - a[1] ]) > b[0];

#Find SMALL likelihood forward-Needleman-Wunsch using only 3 states and neglecting low-likelihood cells.
def forward1pair(s1, s2):
	states=["M","I","D"]
	nS=len(states)
	
	#initialization matrix and first cell
	LC=[]
	l1=len(s1)
	l2=len(s2)
	bottom1=0
	bottom2=0
	bottoms=[]
	for i1 in range(l1+1):
		LC.append([])
		bottoms.append(0)
	LC[0].append([])
	for h in range(nS):
		LC[0][0].append((stateProbs[h],0))
				
	#iteration over matrix cells
	for totLoop in range(l1+l2):
		totN=totLoop+1
		steps=min(l1,l2,totN,l1+l2-totN)
		if totN<=l1:
			i1=totN
			i2=0
		else:
			i1=l1
			i2=totN-l1
		if i2<bottom2:
			i1-=(bottom2-i2)
			steps-=(bottom2-i2)
			i2=bottom2
		if (i1-steps)<bottom1:
			steps-=(bottom1-(i1-steps))
		firstI2=i2
		lastI1=i1-steps
		bestL=float("-inf")
		i1+=1
		i2-=1
		scoresL=[]
		for s in range(steps+1):
			i1-=1
			i2+=1
			ind2=i2-bottoms[i1]
			if i1==bottom1: #initialization first row
				LC[i1].append([])
				
				ran1=[2]
				if i2>1:
					ran2=[2]
				else:
					ran2=range(nS)
					
			elif i2==bottom2: #initialization first column
				LC[i1]=[[]]
				bottoms[i1]=i2
				ind2=0

				ran1=[1]
				if i1>1:
					ran2=[1]
				else:
					ran2=range(nS)
					
			else:	#middle cells
				LC[i1].append([])
					
				ran1=range(nS)
				ran2=range(nS)
				
			nLC=[]
			for h in range(nS):
				nLC.append([])
				for h2 in range(nS):
					nLC[h].append((0.0,float("-inf")))
			tot=0.0
			for h in ran1:
				newL=0.0
				if h==2:
					r1=0
					r2=1
					phylL=pis[s2[i2-1]]
				elif h==1:
					r1=1
					r2=0
					phylL=pis[s1[i1-1]]
				else:
					r1=1
					r2=1
					phylL=phyl[(s2[i2-1],s1[i1-1])]
				ind2p=i2-(r2+bottoms[i1-r1])
				for h2 in ran2:
					if transProbs[h2][h]>1.0e-18:
						nLC[h][h2]=(LC[i1-r1][ind2p][h2][0]*transProbs[h2][h]*phylL,LC[i1-r1][ind2p][h2][1])

			maxLL=(0.0,float("-inf"))
			for h in range(nS):
				newL=nLC[h][0]
				newL=addBfloats(newL,nLC[h][1])
				newL=addBfloats(newL,nLC[h][2])
				finScore=normBfloats(newL)
				LC[i1][ind2].append(finScore)
				if biggerBfloat(finScore,maxLL):
					maxLL=finScore
				
			#based on the LK score of the best path, we will discard the cell
			maxLog=showBfloat(maxLL)		
			scoresL.append(maxLog)
			if maxLog>bestL:
				bestL=maxLog
			
		i2=0	
		i1=0
		le=len(scoresL)
		while (le-(i1+i2))>5 and (scoresL[i2]-bestL<epsilon or scoresL[le-(i1+1)]-bestL<epsilon):
			if scoresL[i2]<scoresL[le-(i1+1)]:
				bottom2=firstI2+i2+1
				i2+=1
			else:
				bottom1=lastI1+i1+1
				i1+=1	
			
	#final likelihood
	newL=LC[l1][-1][0]
	newL=addBfloats(newL,LC[l1][-1][1])
	newL=addBfloats(newL,LC[l1][-1][2])
	finalLK=showBfloat(newL)
	#print("Final LK: "+str(finalLK))
	return finalLK




LK=0.0
for i in range(len(Ls1)):
	s1a=Ls1[i]
	s2a=Ls2[i]
	LK+=forward1pair(s1a, s2a)

if args.resFile!="":
	oFile=open(args.resFile,"w")
	oFile.write(str(LK)+"\n")
	oFile.close()

exit()

