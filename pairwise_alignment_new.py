import sys
import math
import time
import os
import argparse

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D 


DNA={"A":0, "C":1, "G":2, "T":3}
AAs = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
AA = {'A':0, 'R':1, 'N':2, 'D':3, 'C':4, 'Q':5, 'E':6, 'G':7, 'H':8, 'I':9, 'L':10, 'K':11, 'M':12, 'F':13, 'P':14, 'S':15, 'T':16, 'W':17, 'Y':18, 'V':19}
LGfreqs=[0.079066, 0.055941, 0.041977, 0.053052, 0.012937, 0.040767, 0.071586, 0.057337, 0.022355, 0.062157, 0.099081, 0.064600, 0.022951, 0.042302, 0.044040, 0.061197, 0.053287, 0.012066, 0.034155, 0.069147 ]

pypath="/Applications/pypy2-v6.0.0-osx64/bin/pypy"


def getProbs(t1, t2, ri, rd, gi, gd, model="HKY", freqs=[0.25,0.25,0.25,0.25], kappa=3.0, rates=[0.2,0.4,0.2,0.2,0.4,0.2], ancestral=True, iModelType='cumIndels', pypy=True):
	if pypy:
		tempFile="pairHMM_tempFile.txt"
		command=["python pairHMM_parameters.py -o "+tempFile+" --t1 "+str(t1)+" --t2 "+str(t2)+" --ri "+str(ri)+" --rd "+str(rd)+" --gi "+str(gi)+" --gd "+str(gd)+" --model "+str(model)+" --iModel "+str(iModelType)]
		if model=="LG":
			command.append(" --LGfrequencies ")
		else:
			command.append(" --frequencies ")
		for i in range(len(freqs)):
			command.append(str(freqs[i])+" ")
		if ancestral:
			command.append(" --ancestral ")
		command.append(" --kappa "+str(kappa)+" --rates ")
		for i in range(len(rates)):
			command.append(str(rates[i])+" ")
		comm="".join(command)
		os.system(comm)
		
		file=open(tempFile)
		line=file.readline()
		transProbs2=line.split()
		nS=int(math.sqrt(len(transProbs2)-1))
		transProbs=[]
		for i in range(nS):
			transProbs.append([])
			for j in range(nS):
				transProbs[i].append(float(transProbs2[nS*i+j+1]))
			
		line=file.readline()
		stateProbs2=line.split()
		stateProbs=[]
		for i in range(len(stateProbs2)-1):
			stateProbs.append(float(stateProbs2[i+1]))
		line=file.readline()
		probs12=line.split()
		nS2=int(math.sqrt(len(probs12)-1))
		probs1=[]
		for i in range(nS2):
			probs1.append([])
			for j in range(nS2):
				probs1[i].append(float(probs12[nS2*i+j+1]))
		if ancestral:
			file.close()
			return transProbs, stateProbs, probs1
		else:
			line=file.readline()
			probs22=line.split()
			probs2=[]
			for i in range(nS2):
				probs2.append([])
				for j in range(nS2):
					probs2[i].append(float(probs22[nS2*i+j+1]))
			file.close()
			return transProbs, stateProbs, probs1, probs2
	
	else:
		#print("else")
		import pairHMM_parameters
		#print("imported")
		if ancestral:
			#print("ancestral")
			return pairHMM_parameters.getAllProbs3(t1,ri,rd,gi,gd, model, freqs, kappa, rates, ancestral, iModelType=iModelType)
		else:
			#print("non anc")
			return pairHMM_parameters.getAllProbs(t1,t2,ri,rd,gi,gd, model, freqs, kappa, rates, ancestral, iModelType=iModelType)
		








#likelihood of pairwise alignment
def SMALL2fwd(s1, s2, transProbs, stateProbsN, probs1, probs2, pi, alphabet="DNA"):
	s1=s1.replace("_","-")
	s2=s2.replace("_","-")
	states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	
	stateProbs2N=[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
	
	if len(s1)!=len(s2):
		print("ERROR: the SMALL2fwd function needs two aligned sequences (therefore of the same length)")
		exit()
	#consistent states
	consS=[[0],[2,3,5],[1,4,6,7]]
	LogPhyloL=0.0
	totNorm=0.0
	
	for i in range(len(s1)):
		if s1[i]!="-":
			if s2[i]!="-":
				currS=0
				phyloL2=0.0
				for j in range(4):
					phyloL2+=pi[j]*probs1[j][DNAl[s1[i]]]*probs2[j][DNAl[s2[i]]]
				LogPhyloL+=math.log(phyloL2)
			else:
				currS=1
				LogPhyloL+=math.log(pi[DNAl[s1[i]]])
		else:
			if s2[i]!="-":
				currS=2
				LogPhyloL+=math.log(pi[DNAl[s2[i]]])
			else:
				print("ERROR: the SMALL2fwd function for now does not allow columns with gaps in both sequences. Rogue column: "+str(i+1))
				exit()
			
		#log version
		for j in range(nS):
			stateProbs2N[j]=0.0
		for j in consS[currS]:
			for k in range(nS):
				stateProbs2N[j]+=stateProbsN[k]*transProbs[k][j]
		
		norm=0.0
		for j in range(nS):
			norm+=stateProbs2N[j]
		if norm<0.00000001:
			print(norm)
			print(stateProbs2N)
			print(stateProbsN)
			print(transProbs)
		totNorm+=math.log(norm)
		for j in range(nS):
			stateProbsN[j]=stateProbs2N[j]/norm
			
	totalL=LogPhyloL+totNorm
	return totalL
	











#Calculate likelihood of alignment with forward algorithm, assuming that the second sequence is ancestral to the first one (only 3 states)
def SMALL2fwd3(s1, s2, transProbs, stateProbsN, probs1, pi, alphabet="DNA"):
	s1=s1.replace("_","-")
	s2=s2.replace("_","-")
	states=["M","I","D"]
	nS=len(states)
	
	if len(s1)!=len(s2):
		print("ERROR: the SMALL2fwd function needs two aligned sequences (therefore of the same length)")
		exit()
	LogPhyloL=0.0
	totNorm=0.0
	mLog=math.log
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	
	#define log vectors to save on number of logarithm operations
	piLogs=[]
	for i in range(len(pi)):
		piLogs.append(mLog(pi[i]))
	probs1Log=[]
	for i in range(len(probs1)):
		probs1Log.append([])
		for j in range(len(probs1[i])):
			probs1Log[i].append(mLog(probs1[i][j]))
	transProbsLog=[]
	for i in range(len(transProbs)):
		transProbsLog.append([])
		for j in range(len(transProbs[i])):
			if transProbs[i][j]<0.000000000001:
				transProbsLog[i].append(float("-inf"))
			else:
				transProbsLog[i].append(mLog(transProbs[i][j]))
	
	for i in range(len(s1)):
		c1=s1[i]
		c2=s2[i]
		if c1!="-":
			if c2!="-":
				currS=0
				LogPhyloL+=piLogs[DNAl[c2]] + probs1Log[DNAl[c2]][DNAl[c1]]
			else:
				currS=1
				LogPhyloL+=piLogs[DNAl[c1]]
		else:
			if s2[i]!="-":
				currS=2
				LogPhyloL+=piLogs[DNAl[c2]]
			else:
				print("ERROR: the SMALL2fwd function does not allow columns with gaps in both sequences (they are implicitly accounted for by the model). Rogue column: "+str(i+1))
				exit()
		
		if i==0:
			norm=stateProbsN[0]*transProbs[0][currS]+stateProbsN[1]*transProbs[1][currS]+stateProbsN[2]*transProbs[2][currS]
			totNorm+=mLog(norm)
		else:
			totNorm+=transProbsLog[prevS][currS]
		prevS=currS
			
	totalL=LogPhyloL+totNorm
	return totalL
	
	








def res31(h):
	if h==2:
		return 0
	else:
		return 1
def res32(h):
	if h==1:
		return 0
	else:
		return 1








#Find ML alignment with SMALL version of Needleman-Wunsch, now with 3 states only (assume second sequence is ancestral)
def SMALLneedle3(s1, s2, transProbs, stateProbs, probs1, pi, alphabet="DNA"):
	states=["M","I","D"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	#define log vectors to save on number of logarithm operations
	mLog=math.log
	piLogs={}
	for i in DNAl.keys():
		piLogs[i]=mLog(pi[DNAl[i]])
	probs1Log={}
	for i in DNAl.keys():
		for j in DNAl.keys():
			probs1Log[(i,j)]=mLog(probs1[DNAl[i]][DNAl[j]])
	transProbsLog=[]
	for i in range(len(transProbs)):
		transProbsLog.append([])
		for j in range(len(transProbs[i])):
			if transProbs[i][j]<0.000000000001:
				transProbsLog[i].append(float("-inf"))
			else:
				transProbsLog[i].append(mLog(transProbs[i][j]))
	
	B=[] #for backtracking
	LAC=[] #for likelihoods of best paths
	l1=len(s1)
	l2=len(s2)
	for i1 in range(l1+1):
		LAC.append([])
		B.append([])
		for i2 in range(l2+1):
			LAC[i1].append([])
			B[i1].append([])
			if i1==0 and i2==0: #initialization first entry
				for h in range(nS):
					LAC[0][0].append(stateProbs[h])
					B[0][0].append("")
			elif i1==0: #initialization first row
				for h in range(nS):
					LAC[0][i2].append(float("-inf"))
				if i2>1:
					newLC=LAC[0][i2-1][2] + transProbsLog[2][2] + piLogs[s2[i2-1]]
				else:
					newLC=0.0
					for h2 in range(nS):
						newLC+=LAC[0][i2-1][h2]*transProbs[h2][2]*pi[DNAl[s2[i2-1]]]
					newLC=mLog(newLC)
				LAC[0][i2][2]=newLC
				B[0][i2]=["","",2]
			elif i2==0: #initialization first column
				for h in range(nS):
					LAC[i1][0].append(float("-inf"))
				if i1>1:
					newLC=LAC[i1-1][0][1] + transProbsLog[1][1] + piLogs[s1[i1-1]]
				else:
					newLC=0.0
					for h2 in range(nS):
						newLC+=LAC[i1-1][0][h2]*transProbs[h2][1]*pi[DNAl[s1[i1-1]]]
					newLC=mLog(newLC)
				LAC[i1][0][1]=newLC
				B[i1][0]=["",1,""]
				
			else: #actual iteration step
				LAC[i1].append([])
				B[i1].append([])
				for h in range(3):
					newLC=[]
					if i1==1 and i2==1 and h==0:
						phyloL=pi[DNAl[s2[i2-1]]] * probs1[DNAl[s2[i2-1]]][DNAl[s1[i1-1]]]
						tot=0.0
						for h2 in range(3):
							tot+=LAC[i1-1][i2-1][h2] * transProbs[h2][h]
						LAC[i1][i2].append(mLog(tot*phyloL))
						B[i1][i2].append(0)
					else:
						if h==2:
							phyloL=piLogs[s2[i2-1]]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1][i2-1][h2] + transProbsLog[h2][h])
						elif h==1:
							phyloL=piLogs[s1[i1-1]]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1-1][i2][h2] + transProbsLog[h2][h])
						else:
							phyloL=piLogs[s2[i2-1]] + probs1Log[(s2[i2-1],s1[i1-1])]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1-1][i2-1][h2] + transProbsLog[h2][h])
						maxiC=0
						if newLC[1]>newLC[0]:
							maxiC=1
						if newLC[2]>newLC[maxiC]:
							maxiC=2
						B[i1][i2].append(maxiC)
						LAC[i1][i2].append(newLC[maxiC])
	
	maxiC=0
	if LAC[l1][l2][1]>LAC[l1][l2][0]:
		maxiC=1
	if LAC[l1][l2][2]>LAC[l1][l2][maxiC]:
		maxiC=2
	finalC=LAC[l1][l2][maxiC]
	#print "likelihood of NW: "+str(final)
	#print "from logLK:"+str(math.exp(finalC))
	
	#Now backtracking
	maxi=maxiC
	newS1=[]
	newS2=[]
	ind1=l1
	ind2=l2
	while ind1>0 or ind2>0:
		if maxi==0:
			newS1.append(s1[ind1-1])
			newS2.append(s2[ind2-1])
		elif maxi==1:
			newS1.append(s1[ind1-1])
			newS2.append("-")
		elif maxi==2:
			newS2.append(s2[ind2-1])
			newS1.append("-")
		newMaxi=B[ind1][ind2][maxi]
		if maxi==0:
			ind1-=1
			ind2-=1
		elif maxi==1:
			ind1-=1
		elif maxi==2:
			ind2-=1
		#oldMaxi=maxi
		maxi=newMaxi
	newS1.reverse()
	newS2.reverse()
	
	#finalC=finalC-math.log(stateProbs[maxi])-math.log(transProbs[maxi][oldMaxi])+math.log(stateProbs[oldMaxi])
	print("logLK: "+str(finalC))
	#print(math.exp(finalC))
	return [finalC, "".join(newS1), "".join(newS2)]
	
	
	

	





#Find ML alignment with SMALL version of Needleman-Wunsch, now with 3 states only (second sequence ancestral) now avoiding ininfluent matrix cells (up to a log-likelihood threshold epsilon)
def SMALLneedle3Fast(s1, s2, transProbs, stateProbs, probs1, pi, epsilon=-9.0, alphabet="DNA",fileHeatMap=""):
	if fileHeatMap!="":
		fileHeat=open(fileHeatMap,"w")
	states=["M","I","D"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	#define log vectors to save on number of logarithm operations
	mLog=math.log
	piLogs={}
	for i in DNAl.keys():
		piLogs[i]=mLog(pi[DNAl[i]])
	probs1Log={}
	for i in DNAl.keys():
		for j in DNAl.keys():
			probs1Log[(i,j)]=mLog(probs1[DNAl[i]][DNAl[j]])
	transProbsLog=[]
	for i in range(len(transProbs)):
		transProbsLog.append([])
		for j in range(len(transProbs[i])):
			if transProbs[i][j]<0.000000000001:
				transProbsLog[i].append(float("-inf"))
			else:
				transProbsLog[i].append(mLog(transProbs[i][j]))
	
	B=[]
	LAC=[]
	l1=len(s1)
	l2=len(s2)
	bottom1=0
	bottom2=0
	bottoms=[]
	for i1 in range(l1+1):
		LAC.append([])
		B.append([])
		bottoms.append(0)
	LAC[0].append([])
	B[0].append([])
	for h in range(nS):
		LAC[0][0].append(stateProbs[h])
		B[0][0].append("")
	
	maxStepsSoFar=0
	for totLoop in range(l1+l2):
		totN=totLoop+1
		steps=min(l1,l2,totN,l1+l2-totN)
		if totN<=l1:
			i1=totN
			i2=0
		else:
			i1=l1
			i2=totN-l1
		if i2<bottom2:
			i1-=(bottom2-i2)
			steps-=(bottom2-i2)
			i2=bottom2
		if (i1-steps)<bottom1:
			steps-=(bottom1-(i1-steps))
		firstI2=i2
		lastI1=i1-steps
		bestL=float("-inf")
		if fileHeatMap!="":
			fileHeat.write(str(firstI2)+"\t"+str(steps+1)+"\n")
		i1+=1
		i2-=1
		scoresL=[]
		if steps>maxStepsSoFar:
			maxStepsSoFar=steps
		for s in range(steps+1):
			i1-=1
			i2+=1
			ind2=i2-bottoms[i1]
			if i1==bottom1: #initialization first row
				LAC[i1].append([])
				B[i1].append([])
				if i2==1:
					newLC=0.0
					for h2 in range(nS):
						newLC+=LAC[0][0][h2]*transProbs[h2][2]*pi[DNAl[s2[i2-1]]]
					maxS=mLog(newLC)
					maxi=2
				else:
					phyl=piLogs[s2[i2-1]]
					maxi=0
					maxS=float("-inf")
					for h2 in range(nS):
						newLC=LAC[i1][ind2-1][h2] + transProbsLog[h2][2] + phyl
						if newLC>maxS:
							maxi=h2
							maxS=newLC
				B[i1][ind2]=["","",maxi]
				LAC[i1][ind2]=[float("-inf"),float("-inf"),maxS]
				if maxS>bestL:
					bestL=maxS
				scoresL.append(maxS)
				
			elif i2==bottom2: #initialization first column
				LAC[i1]=[[]]
				B[i1]=[[]]
				bottoms[i1]=i2
				ind2=0
				ind2p=i2-bottoms[i1-1]
				#for h in range(nS):
				#	LAC[i1][0].append(float("-inf"))	
				if i1==1:
					newLC=0.0
					for h2 in range(nS):
						newLC+=LAC[0][0][h2]*transProbs[h2][1]*pi[DNAl[s1[i1-1]]]
					maxS=mLog(newLC)
					maxi=1
				else:
					phyl=piLogs[s1[i1-1]]
					maxi=0
					maxS=float("-inf")
					for h2 in range(nS):
						newLC=LAC[i1-1][ind2p][h2] + transProbsLog[h2][1] + phyl
						if newLC>maxS:
							maxi=h2
							maxS=newLC
				B[i1][0]=["",maxi,""]
				LAC[i1][0]=[float("-inf"),maxS,float("-inf")]
				bestL=maxS
				scoresL.append(maxS)
			else: #actual iteration step
				LAC[i1].append([])
				B[i1].append([])
				bestS=float("-inf")
				for h in range(3):
					newLC=[]
					if i1==1 and i2==1 and h==0:
						phyloL=pi[DNAl[s2[i2-1]]] * probs1[DNAl[s2[i2-1]]][DNAl[s1[i1-1]]]
						tot=0.0
						for h2 in range(3):
							tot+=LAC[0][0][h2] * transProbs[h2][h]
						LAC[i1][i2].append(mLog(tot*phyloL))
						B[i1][i2].append(0)
						bestS=mLog(tot*phyloL)
					else:
						if h==2:
							ind2p=i2-(1+bottoms[i1])
							phyloL=piLogs[s2[i2-1]]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1][ind2p][h2] + transProbsLog[h2][h])
						elif h==1:
							ind2p=i2-(bottoms[i1-1])
							phyloL=piLogs[s1[i1-1]]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1-1][ind2p][h2] + transProbsLog[h2][h])
						else:
							ind2p=i2-(1+bottoms[i1-1])
							phyloL=piLogs[s2[i2-1]] + probs1Log[(s2[i2-1],s1[i1-1])]
							for h2 in range(3):
								newLC.append(phyloL + LAC[i1-1][ind2p][h2] + transProbsLog[h2][h])
						maxiC=0
						if newLC[1]>newLC[0]:
							maxiC=1
						if newLC[2]>newLC[maxiC]:
							maxiC=2
						B[i1][ind2].append(maxiC)
						score=newLC[maxiC]
						if score>bestS:
							bestS=score
						LAC[i1][ind2].append(score)
				scoresL.append(bestS)
				if bestS>bestL:
					bestL=bestS			

		#keep at least a number of cells in this list, something like 5, so that you don't get to final probabilities of 0.0
		i2=0	
		i1=0
		le=len(scoresL)
		while (le-(i1+i2))>5 and (scoresL[i2]-bestL<epsilon or scoresL[le-(i1+1)]-bestL<epsilon):
			if scoresL[i2]<scoresL[le-(i1+1)]:
				bottom2=firstI2+i2+1
				i2+=1
			else:
				bottom1=lastI1+i1+1
				i1+=1	
			
	maxiC=0
	maxV=float("-inf")
	#print LAC[l1][-1]
	for sc in range(3):
		if LAC[l1][-1][sc]>maxV:
			maxV=LAC[l1][-1][sc]
			maxiC=sc
	#print("max band size for eps="+str(epsilon)+": "+str(maxStepsSoFar))
	
	#ML
	finalC=LAC[l1][-1][maxiC]
	
	#Now backtracking
	maxi=maxiC
	newS1=[]
	newS2=[]
	ind1=l1
	ind2=l2
	while ind1>0 or ind2>0:
		if maxi==0:
			newS1.append(s1[ind1-1])
			newS2.append(s2[ind2-1])
		elif maxi==1:
			newS1.append(s1[ind1-1])
			newS2.append("-")
		elif maxi==2:
			newS2.append(s2[ind2-1])
			newS1.append("-")
		newMaxi=B[ind1][ind2-bottoms[ind1]][maxi]
		if maxi==0:
			ind1-=1
			ind2-=1
		elif maxi==1:
			ind1-=1
		elif maxi==2:
			ind2-=1
		oldMaxi=maxi
		maxi=newMaxi
	newS1.reverse()
	newS2.reverse()
	
	print("logLK: "+str(finalC))
	#print(math.exp(finalC))
	if fileHeatMap!="":
		fileHeat.close()
	
	return [finalC, "".join(newS1), "".join(newS2)]
	
	
	




	

#is there a residue at column 1?
def res1(h):
	if h==1 or h==4 or h==6 or h==7:
		return 0
	else:
		return 1
def res2(h):
	if h==2 or h==3 or h==5:
		return 0
	else:
		return 1






#Find ML alignment with SMALL version of Needleman-Wunsch, ignoring matrix cells with low likelihood
def SMALLneedleFast(s1, s2, transProbs, stateProbs, probs1, probs2, pi, epsilon=-9.0, alphabet="DNA"):
	states=["M","I","D"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA

	states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
	nS=len(states)

	#consistent states (emission probabilities)
	consS=[[0],[2,3,5],[1,4,6,7]]
	cons2=[0,2,1,1,2,1,2,2]
	
	B=[]
	LAC=[]
	LC=[]
	l1=len(s1)
	l2=len(s2)
	bottom1=0
	bottom2=0
	bottoms=[]
	for i1 in range(l1+1):
		LAC.append([])
		LC.append([])
		B.append([])
		bottoms.append(0)
	LAC[0].append([])
	LC[0].append([])
	B[0].append([])
	for h in range(nS):
		LC[0][0].append([stateProbs[h],0.0])
		B[0][0].append("")
	LAC[0][0].append([stateProbs[0],0.0])
	LAC[0][0].append([stateProbs[2]+stateProbs[3]+stateProbs[5],0.0])
	LAC[0][0].append([stateProbs[1]+stateProbs[4]+stateProbs[6],0.0])
	
	for totLoop in range(l1+l2):
		totN=totLoop+1
		steps=min(l1,l2,totN,l1+l2-totN)
		if totN<=l1:
			i1=totN
			i2=0
		else:
			i1=l1
			i2=totN-l1
		if i2<bottom2:
			i1-=(bottom2-i2)
			steps-=(bottom2-i2)
			i2=bottom2
		if (i1-steps)<bottom1:
			steps-=(bottom1-(i1-steps))
		firstI2=i2
		lastI1=i1-steps
		bestL=float("-inf")
		i1+=1
		i2-=1
		scoresL=[]
		for s in range(steps+1):
			i1-=1
			i2+=1
			ind2=i2-bottoms[i1]
			if i1==bottom1: #initialization first row
				LAC[i1].append([])
				LC[i1].append([])
				B[i1].append([])
				for h in range(3):
					LAC[i1][ind2].append([0.0,0.0])
				for h in range(nS):
					LC[i1][ind2].append([0.0,0.0])
				tot=0.0
				phyl=pi[DNAl[s2[i2-1]]]
				for h in consS[2]:
					newLC=[0.0,0.0]
					if i2>1:
						ran=consS[2]
					else:
						ran=range(nS)
					for h2 in ran:
						newLC[0]+=LC[i1][ind2-1][h2][0]*transProbs[h2][h]*phyl
						newLC[1]=LC[i1][ind2-1][h2][1]
					LAC[i1][ind2][2][0]+=newLC[0]
					LAC[i1][ind2][2][1]=newLC[1]
					LC[i1][ind2][h][0]=newLC[0]
					LC[i1][ind2][h][1]=newLC[1]
					tot+=newLC[0]
					
				#update logarithmic scaling if necessary			
				if tot<0.1:
					sc=math.log(tot)
					for h in consS[2]:
						LC[i1][ind2][h][0]=LC[i1][ind2][h][0]/tot
						LC[i1][ind2][h][1]+=sc
					LAC[i1][ind2][2][0]=LAC[i1][ind2][2][0]/tot
					LAC[i1][ind2][2][1]+=sc
				
				B[i1][ind2]=["","",2]
				S=LAC[i1][ind2][2][1]+math.log(LAC[i1][ind2][2][0])
				if S>bestL:
					bestL=S
				scoresL.append(S)
				
				
			elif i2==bottom2: #initialization first column
				LAC[i1]=[[]]
				LC[i1]=[[]]
				B[i1]=[[]]
				bottoms[i1]=i2
				ind2=0
				for h in range(3):
					LAC[i1][ind2].append([0.0,0.0])
				for h in range(nS):
					LC[i1][ind2].append([0.0,0.0])

				tot=0.0
				phyl=pi[DNAl[s1[i1-1]]]
				ind2p=i2-(bottoms[i1-1])
				for h in consS[1]:
					newLC=[0.0,0.0]
					if i1>1:
						ran=consS[1]
					else:
						ran=range(nS)
					for h2 in ran:
						newLC[0]+=LC[i1-1][ind2p][h2][0]*transProbs[h2][h]*phyl
						newLC[1]=LC[i1-1][ind2p][h2][1]
					LAC[i1][ind2][1][0]+=newLC[0]
					LAC[i1][ind2][1][1]=newLC[1]
					LC[i1][ind2][h][0]=newLC[0]
					LC[i1][ind2][h][1]=newLC[1]
					tot+=newLC[0]
					
				#update logarithmic scaling if necessary			
				if tot<0.1:
					sc=math.log(tot)
					for h in consS[1]:
						LC[i1][ind2][h][0]=LC[i1][ind2][h][0]/tot
						LC[i1][ind2][h][1]+=sc
					LAC[i1][ind2][1][0]=LAC[i1][ind2][1][0]/tot
					LAC[i1][ind2][1][1]+=sc
				
				B[i1][ind2]=["",1,""]
				S=LAC[i1][ind2][1][1]+math.log(LAC[i1][ind2][1][0])
				bestL=S
				scoresL.append(S)
				
			else: #actual iteration step
				LAC[i1].append([])
				LC[i1].append([])
				B[i1].append([])
				
				tot=0.0
				newLC=[]
				newLAC=[]
				for A2 in range(3):
					newLAC.append([])
					for A in range(3):
						newLAC[A2].append([0.0,0.0])
				phyloL=0.0
				for j in range(4):
					phyloL+=pi[j]*probs1[j][DNAl[s1[i1-1]]]*probs2[j][DNAl[s2[i2-1]]]
				phyl=[0.0,pi[DNAl[s1[i1-1]]],pi[DNAl[s2[i2-1]]],phyloL]
				for h2 in range(nS):
					newLC.append([])
					for h in range(nS):
						r1=res1(h)
						r2=res2(h)
						ind2p=i2-(r2+bottoms[i1-r1])
						newLC[h2].append([LC[i1-r1][ind2p][h2][0]*transProbs[h2][h],LC[i1-r1][ind2p][h2][1]])
						newLC[h2][h][0]*=phyl[r2*2+r1]
						LK=newLC[h2][h][0]
						tot+=LK
						newLAC[cons2[h2]][cons2[h]][0]+=LK
						newLAC[cons2[h2]][cons2[h]][1]=newLC[h2][h][1]
							
					
				#update logarithmic scaling if necessary			
				if tot<0.1:
					sc=math.log(tot)
					for h in range(nS):
						for h2 in range(nS):
							newLC[h2][h][0]=newLC[h2][h][0]/tot
							newLC[h2][h][1]=newLC[h2][h][1]+sc
					for A in range(3):
						for A2 in range(3):
							newLAC[A2][A][0]=newLAC[A2][A][0]/tot
							newLAC[A2][A][1]=newLAC[A2][A][1]+sc
							
				ind2=i2-bottoms[i1]
				#Store in DP matrix		
				for h in range(nS):
					LC[i1][ind2].append([0.0,0.0])
				bestS=float("-inf")
				for A in range(3):
					maxiC=0
					maxS=float("-inf")
					if i1==1 and i2==1 and A==0:
						totP=newLAC[0][A][0]+newLAC[1][A][0]+newLAC[2][A][0]
						maxS=math.log(totP)
						if maxS>bestS:
							bestS=maxS
						LAC[i1][ind2].append([totP/tot,sc])
						for h in consS[A]:
							for h2 in range(nS):
								LC[i1][ind2][h][0]+=newLC[h2][h][0]
								LC[i1][ind2][h][1]=newLC[h2][h][1]
					else:
						for i in range(3):
							if newLAC[i][A][0]<0.000000000001:
								sco=float("-inf")
							else:
								sco=newLAC[i][A][1]+math.log(newLAC[i][A][0])
							if sco>maxS:
								maxS=sco
								maxiC=i
							if sco>bestS:
								bestS=sco
						LAC[i1][ind2].append(newLAC[maxiC][A])
						for h in consS[A]:
							for h2 in consS[maxiC]:
								LC[i1][ind2][h][0]+=newLC[h2][h][0]
								LC[i1][ind2][h][1]=newLC[h2][h][1]
					B[i1][ind2].append(maxiC)
					
							
				#based on the LK score of the best path, we will discard the cell			
				scoresL.append(bestS)
				if bestS>bestL:
					bestL=bestS
			
		i2=0	
		i1=0
		le=len(scoresL)
		while (le-(i1+i2))>5 and (scoresL[i2]-bestL<epsilon or scoresL[le-(i1+1)]-bestL<epsilon):
			if scoresL[i2]<scoresL[le-(i1+1)]:
				bottom2=firstI2+i2+1
				i2+=1
			else:
				bottom1=lastI1+i1+1
				i1+=1	
					
	maxiC=0
	maxV=float("-inf")
	#print LAC[l1][-1]
	for sc in range(3):
		if LAC[l1][-1][sc][0]>0.000000001:
			if LAC[l1][-1][sc][1]+math.log(LAC[l1][-1][sc][0])>maxV:
				maxV=LAC[l1][-1][sc][1]+math.log(LAC[l1][-1][sc][0])
				maxiC=sc
	
	#ML
	finalC=LAC[l1][-1][maxiC][1]+math.log(LAC[l1][-1][maxiC][0])
	
	#Now backtracking
	maxi=maxiC
	newS1=[]
	newS2=[]
	ind1=l1
	ind2=l2
	while ind1>0 or ind2>0:
		if maxi==0:
			newS1.append(s1[ind1-1])
			newS2.append(s2[ind2-1])
		elif maxi==1:
			newS1.append(s1[ind1-1])
			newS2.append("-")
		elif maxi==2:
			newS2.append(s2[ind2-1])
			newS1.append("-")
		newMaxi=B[ind1][ind2-bottoms[ind1]][maxi]
		if maxi==0:
			ind1-=1
			ind2-=1
		elif maxi==1:
			ind1-=1
		elif maxi==2:
			ind2-=1
		oldMaxi=maxi
		maxi=newMaxi
	newS1.reverse()
	newS2.reverse()
	
	#finalC=finalC-math.log(stateProbs[maxi])-math.log(transProbs[maxi][oldMaxi])+math.log(stateProbs[oldMaxi])
	print("logLK: "+str(finalC))
	#print(math.exp(finalC))
	
	return [finalC, "".join(newS1), "".join(newS2)]
	
	





	
	
	
	
	
	
	
	
	
	
		
		
		





		
#Find ML alignment with SMALL version of Needleman-Wunsch - new version with 8 states to account for PID>PI*PD
def SMALLneedle(s1, s2, transProbs, stateProbs, probs1, probs2, pi, alphabet="DNA"):
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA			
				
	states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
	nS=len(states)

	consS=[[0],[2,3,5],[1,4,6,7]]
	
	B=[]
	LAC=[]
	LC=[]
	l1=len(s1)
	l2=len(s2)
	for i1 in range(l1+1):
		LAC.append([])
		LC.append([])
		B.append([])
		for i2 in range(l2+1):
			LC[i1].append([])
			LAC[i1].append([])
			B[i1].append([])
			if i1==0 and i2==0: #initialization first entry
				for h in range(nS):
					LC[0][0].append([stateProbs[h],0.0])
				LAC[0][0].append([stateProbs[0],0.0])
				LAC[0][0].append([stateProbs[2]+stateProbs[3]+stateProbs[5],0.0])
				LAC[0][0].append([stateProbs[1]+stateProbs[4]+stateProbs[6]+stateProbs[7],0.0])
				B[0][0].append("")
				B[0][0].append("")
				B[0][0].append("")
			elif i1==0: #initialization first row
				for h in range(nS):
					LC[0][i2].append([0.0,0.0])
				LAC[0][i2].append([0.0,0.0])
				LAC[0][i2].append([0.0,0.0])
				LAC[0][i2].append([0.0,0.0])
				tot=0.0
				for h in consS[2]:
					newLC=[0.0,0.0]
					if i2>1:
						for h2 in consS[2]:
							newLC[0]+=LC[0][i2-1][h2][0]*transProbs[h2][h]*pi[DNAl[s2[i2-1]]]
							newLC[1]=LC[0][i2-1][h2][1]
					else:
						for h2 in range(nS):
							newLC[0]+=LC[0][i2-1][h2][0]*transProbs[h2][h]*pi[DNAl[s2[i2-1]]]
							newLC[1]=LC[0][i2-1][h2][1]
					LAC[0][i2][2][0]+=newLC[0]
					LAC[0][i2][2][1]=newLC[1]
					LC[0][i2][h][0]=newLC[0]
					LC[0][i2][h][1]=newLC[1]
					tot+=newLC[0]
				#print "tot: "+str(tot)
				for h in consS[2]:
					LC[0][i2][h][0]=LC[0][i2][h][0]/tot
					LC[0][i2][h][1]+=math.log(tot)
				LAC[0][i2][2][0]=LAC[0][i2][2][0]/tot
				LAC[0][i2][2][1]+=math.log(tot)
				B[0][i2].append("")
				B[0][i2].append("")
				B[0][i2].append(2)
			elif i2==0: #initialization first column
				for h in range(nS):
					LC[i1][0].append([0.0,0.0])
				LAC[i1][0].append([0.0,0.0])
				LAC[i1][0].append([0.0,0.0])
				LAC[i1][0].append([0.0,0.0])
				tot=0.0
				for h in consS[1]:
					newLC=[0.0,0.0]
					if i1>1:
						for h2 in consS[1]:
							newLC[0]+=LC[i1-1][0][h2][0]*transProbs[h2][h]*pi[DNAl[s1[i1-1]]]
							newLC[1]=LC[i1-1][0][h2][1]
					else:
						for h2 in range(nS):
							newLC[0]+=LC[i1-1][0][h2][0]*transProbs[h2][h]*pi[DNAl[s1[i1-1]]]
							newLC[1]=LC[i1-1][0][h2][1]
					LAC[i1][0][1][0]+=newLC[0]
					LAC[i1][0][1][1]=newLC[1]
					LC[i1][0][h][0]=newLC[0]
					LC[i1][0][h][1]=newLC[1]
					tot+=newLC[0]
				for h in consS[1]:
					LC[i1][0][h][0]=LC[i1][0][h][0]/tot
					LC[i1][0][h][1]+=math.log(tot)
				LAC[i1][0][1][0]=LAC[i1][0][1][0]/tot
				LAC[i1][0][1][1]+=math.log(tot)
				B[i1][0].append("")
				B[i1][0].append(1)
				B[i1][0].append("")
				
			else: #actual iteration step
				newLC=[]
				for h2 in range(nS):
					newLC.append([])
					for h in range(nS):
						r1=res1(h)
						r2=res2(h)
						newLC[h2].append([LC[i1-r1][i2-r2][h2][0]*transProbs[h2][h],LC[i1-r1][i2-r2][h2][1]])
						if r1==0:
							newLC[h2][h][0]*=pi[DNAl[s2[i2-1]]]
						elif r2==0:
							newLC[h2][h][0]*=pi[DNAl[s1[i1-1]]]
						else:
							phyloL=0.0
							for j in range(4):
								phyloL+=pi[j]*probs1[j][DNAl[s1[i1-1]]]*probs2[j][DNAl[s2[i2-1]]]
							newLC[h2][h][0]*=phyloL
						
				tot=0.0
				for h in range(nS):
					for h2 in range(nS):
						tot+=newLC[h2][h][0]
				
				for h in range(nS):
					for h2 in range(nS):
						newLC[h2][h][0]=newLC[h2][h][0]/tot
						newLC[h2][h][1]=newLC[h2][h][1]+math.log(tot)
				
				newLAC=[]
				for A2 in range(3):
					newLAC.append([])
					for A in range(3):
						newLAC[A2].append([0.0,0.0])
						for h2 in consS[A2]:
							for h in consS[A]:
								newLAC[A2][A][0]+=newLC[h2][h][0]
								newLAC[A2][A][1]=newLC[h2][h][1]
								
				for h in range(nS):
					LC[i1][i2].append([0.0,0.0])
				for A in range(3):
					maxiC=0
					scores=[]
					
					if i1==1 and i2==1 and A==0:
						totP=newLAC[0][A][0]+newLAC[1][A][0]+newLAC[2][A][0]
						for i in range(3):
							if totP<0.000000000001:
								scores.append(float("-inf"))
							else:
								scores.append(newLAC[i][A][1]+math.log(totP))
						maxiC=0
						B[i1][i2].append(maxiC)
						LAC[i1][i2].append([totP,newLAC[0][A][1]])
						for h in consS[A]:
							for h2 in range(nS):
								LC[i1][i2][h][0]+=newLC[h2][h][0]
								LC[i1][i2][h][1]=newLC[h2][h][1]
					
					else:	
						for i in range(3):
							if newLAC[i][A][0]<0.000000000001:
								scores.append(float("-inf"))
							else:
								scores.append(newLAC[i][A][1]+math.log(newLAC[i][A][0]))
						if scores[1]>scores[0]:
							maxiC=1
						if scores[2]>scores[0] and scores[2]>scores[1]:
							maxiC=2
						B[i1][i2].append(maxiC)
						LAC[i1][i2].append(newLAC[maxiC][A])
						for h in consS[A]:
							for h2 in consS[maxiC]:
								LC[i1][i2][h][0]+=newLC[h2][h][0]
								LC[i1][i2][h][1]=newLC[h2][h][1]

	maxiC=0
	maxV=float("-inf")
	#print LAC[l1][-1]
	for sc in range(3):
		if LAC[l1][l2][sc][0]>0.000000001:
			if LAC[l1][l2][sc][1]+math.log(LAC[l1][l2][sc][0])>maxV:
				maxV=LAC[l1][l2][sc][1]+math.log(LAC[l1][l2][sc][0])
				maxiC=sc
	
	#ML
	finalC=LAC[l1][l2][maxiC][1]+math.log(LAC[l1][l2][maxiC][0])
	#print "likelihood of NW: "+str(final)
	#print "from logLK:"+str(math.exp(finalC))
	
	#Now backtracking
	maxi=maxiC
	newS1=[]
	newS2=[]
	ind1=l1
	ind2=l2
	while ind1>0 or ind2>0:
		if maxi==0:
			newS1.append(s1[ind1-1])
			newS2.append(s2[ind2-1])
		elif maxi==1:
			newS1.append(s1[ind1-1])
			newS2.append("-")
		elif maxi==2:
			newS2.append(s2[ind2-1])
			newS1.append("-")
		newMaxi=B[ind1][ind2][maxi]
		if maxi==0:
			ind1-=1
			ind2-=1
		elif maxi==1:
			ind1-=1
		elif maxi==2:
			ind2-=1
		oldMaxi=maxi
		maxi=newMaxi
	newS1.reverse()
	newS2.reverse()

	print("logLK: "+str(finalC))
	#print(math.exp(finalC))
	
	return [finalC, "".join(newS1), "".join(newS2)]
	
	
	
	
	
	
	








#Find SMALL likelihood forward-Needleman-Wunsch - new version with 8 states
def forwardNW(s1, s2, transProbs, stateProbs, probs1, probs2, pi, alphabet="DNA"):
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA	
	
	states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
	nS=len(states)

	consS=[[0],[2,3,5],[1,4,6,7]]
	
	phyl=[]#phylogenetic probabilities (emission probabilities)
	for i1 in range(4):
		phyl.append([])
		for i2 in range(4):
			phyl[i1].append(0.0)
			for j in range(4):
				phyl[i1][i2]+=pi[j]*probs1[j][i1]*probs2[j][i2]
	
	LC=[]
	l1=len(s1)
	l2=len(s2)
	for i1 in range(l1+1):
		LC.append([])
		for i2 in range(l2+1):
			LC[i1].append([])
			if i1==0 and i2==0: #initialization first entry
				for h in range(nS):
					LC[0][0].append([stateProbs[h],0.0])

			else: #actual iteration step
				if i2==0:
					ran1=consS[1]
					if i1>1:
						ran2=consS[1]
					else:
						ran2=range(nS)
				elif i1==0:
					ran1=consS[2]
					if i2>1:
						ran2=consS[2]
					else:
						ran2=range(nS)
				else:
					ran1=range(nS)
					ran2=range(nS)
				ave=0.0
				num=0
				for h in ran1:
					r1=res1(h)
					r2=res2(h)
					for h2 in ran2:
						ave+=LC[i1-r1][i2-r2][h2][1]
						num+=1
				ave=ave/num
				nLC=[]
				tot=0.0
				for h in range(nS):
					newL=0.0
					nLC.append([])
					r1=res1(h)
					r2=res2(h)
					if (h in ran1):
						factor=math.exp(LC[i1-r1][i2-r2][0][1]-ave)
					for h2 in range(nS):
						nLC[h].append(0.0)
						if transProbs[h2][h]>0.000000000001 and (h in ran1) and (h2 in ran2):
							nLC[h][h2]=LC[i1-r1][i2-r2][h2][0]*transProbs[h2][h]*factor
							if r1==0:
								nLC[h][h2]*=pi[DNAl[s2[i2-1]]]
							elif r2==0:
								nLC[h][h2]*=pi[DNAl[s1[i1-1]]]
							else:
								nLC[h][h2]*=phyl[DNAl[s1[i1-1]]][DNAl[s2[i2-1]]]
							tot+=nLC[h][h2]
				if tot>0.000000000001:
					for h in range(nS):
						for h2 in range(nS):
							nLC[h][h2]=nLC[h][h2]/tot
					ave+=math.log(tot)
				else:
					for h in range(nS):
						for h2 in range(nS):
							nLC[h][h2]=0.0
					ave+=float("-inf")
				#print "tot: "+str(tot)
				for h in range(nS):
					newL=0.0
					for h2 in range(nS):
						newL+=nLC[h][h2]
					LC[i1][i2].append([newL,ave])

	fin=LC[l1][l2][0][1]#+math.log(finalC)
	print "finalC: "+str(fin)
	#print "exp finalC: "+str(math.exp(fin))
	#print LC
	return [fin, LC]



















#definitions for more buoyant floats (Lunter 2007, Bioinformatics)
cBFloatRange = 20282409603651670423947251286016.0  #2.03e+31; 2^104
cBFloatRangeInv = 1.0/cBFloatRange
cBFloatRangeSqrt    = 1.0e+18          # Value between square root of the exponent, and the exponent
cBFloatRangeInvSqrt = 1.0e-18          # Square of this should still be representable, with full mantissa!
logcBFloatRange     = math.log(cBFloatRange)
cBFloatConvTableSize       = 100               # This includes many zero entries, it makes additions a bit faster
aConversionLookup = []
iBFM = 1.0
for i in range(cBFloatConvTableSize):
	aConversionLookup.append(iBFM)
	iBFM *= cBFloatRangeInv

def normBfloats(a):
	mantissa=a[0]
	expo=a[1]
	if mantissa<0.0:
		print("Error, negative mantissa")
		print(a)
		return (0.0,float('-inf'))
	if expo==float('-inf'):
		return (0.0,float('-inf'))
	if mantissa==0.0:
		print("Error, zero mantissa before normalization")
		print(a)
		return (0.0,float('-inf'))
	while (mantissa > cBFloatRangeSqrt):
		mantissa *= cBFloatRangeInv
		expo+=1
	while (mantissa < cBFloatRangeInvSqrt):
		mantissa *= cBFloatRange
		expo-=1
	if mantissa==0.0:
		print("Error, zero mantissa after normalization")
		print(a)
		return (0.0,float('-inf'))
	return (mantissa,expo)
	

def showBfloat( a ):
	return a[1]*logcBFloatRange+math.log(a[0])

def addBfloats(a, b) :
	if a[1]>b[1]:
		if a[1]>=(b[1]+cBFloatConvTableSize):
			return a
		else:
			return (a[0] + b[0]*aConversionLookup[ a[1] - b[1] ], a[1])
	if a[1]<=(b[1]-cBFloatConvTableSize):
		return b
	else:
		return (b[0] + a[0]*aConversionLookup[ b[1] - a[1] ], b[1])


def biggerBfloat(a,b):
	if a[1] > b[1]:
		if a[1] >= (b[1] + cBFloatConvTableSize):
			return True
		else:
			return a[0] > (b[0] * aConversionLookup[ a[1] - b[1] ])
	if a[1] <= (b[1] - cBFloatConvTableSize):
		return False
	else:
		return (a[0] * aConversionLookup[ b[1] - a[1] ]) > b[0];






#Find SMALL likelihood forward-Needleman-Wunsch when the second sequence is ancestral to the first one (so 3 hidden states in total)
def forwardNW3(s1, s2, transProbs, stateProbs, probs1, pi, alphabet="DNA"):
	states=["M","I","D"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	
	phyl={}#phylogenetic probabilities (emission probabilities)
	for i in DNAl.keys():
		for j in DNAl.keys():
			phyl[(i,j)]=pi[DNAl[i]]*probs1[DNAl[i]][DNAl[j]]
	pis={}
	for i in DNAl.keys():
		pis[i]=pi[DNAl[i]]
	
	LC=[]
	l1=len(s1)
	l2=len(s2)
	for i1 in range(l1+1):
		LC.append([])
		for i2 in range(l2+1):
			LC[i1].append([])
			if i1==0 and i2==0: #initialization first entry
				for h in range(nS):
					LC[0][0].append((stateProbs[h],0))
			else: #actual iteration step
				if i2==0:
					ran1=[1]
					if i1>1:
						ran2=[1]
					else:
						ran2=range(nS)
				elif i1==0:
					ran1=[2]
					if i2>1:
						ran2=[2]
					else:
						ran2=range(nS)
				else:
					ran1=range(nS)
					ran2=range(nS)
				nLC=[]
				for h in range(nS):
					newL=0.0
					nLC.append([])
					if h==2:
						r1=0
						r2=1
						phylL=pis[s2[i2-1]]
					elif h==1:
						r1=1
						r2=0
						phylL=pis[s1[i1-1]]
					else:
						r1=1
						r2=1
						phylL=phyl[(s2[i2-1],s1[i1-1])]
					for h2 in range(nS):
						if transProbs[h2][h]>1.0e-18 and (h in ran1) and (h2 in ran2):
							nLC[h].append((LC[i1-r1][i2-r2][h2][0]*transProbs[h2][h]*phylL,LC[i1-r1][i2-r2][h2][1]))
						else:
							nLC[h].append((0.0,float('-inf')))
				for h in range(nS):
					newL=nLC[h][0]
					newL=addBfloats(newL,nLC[h][1])
					newL=addBfloats(newL,nLC[h][2])
					LC[i1][i2].append(normBfloats(newL))

	newL=LC[l1][l2][0]
	newL=addBfloats(newL,LC[l1][l2][1])
	newL=addBfloats(newL,LC[l1][l2][2])
	finalLK=showBfloat(newL)
	print("Final LK: "+str(finalLK))
	return [finalLK, LC]











#Find SMALL likelihood forward-Needleman-Wunsch using only 3 states and neglecting low-likelihood cells.
def forwardNW3fast(s1, s2, transProbs, stateProbs, probs1, pi, epsilon=-9.0, alphabet="DNA"):
	states=["M","I","D"]
	nS=len(states)
	
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA
	
	phyl={}#phylogenetic probabilities (emission probabilities)
	for i in DNAl.keys():
		for j in DNAl.keys():
			phyl[(i,j)]=pi[DNAl[i]]*probs1[DNAl[i]][DNAl[j]]
	pis={}
	for i in DNAl.keys():
		pis[i]=pi[DNAl[i]]
	
	#initialization matrix and first cell
	LC=[]
	l1=len(s1)
	l2=len(s2)
	bottom1=0
	bottom2=0
	bottoms=[]
	for i1 in range(l1+1):
		LC.append([])
		bottoms.append(0)
	LC[0].append([])
	for h in range(nS):
		LC[0][0].append((stateProbs[h],0))
				
	#iteration over matrix cells
	for totLoop in range(l1+l2):
		totN=totLoop+1
		steps=min(l1,l2,totN,l1+l2-totN)
		if totN<=l1:
			i1=totN
			i2=0
		else:
			i1=l1
			i2=totN-l1
		if i2<bottom2:
			i1-=(bottom2-i2)
			steps-=(bottom2-i2)
			i2=bottom2
		if (i1-steps)<bottom1:
			steps-=(bottom1-(i1-steps))
		firstI2=i2
		lastI1=i1-steps
		bestL=float("-inf")
		i1+=1
		i2-=1
		scoresL=[]
		for s in range(steps+1):
			i1-=1
			i2+=1
			ind2=i2-bottoms[i1]
			if i1==bottom1: #initialization first row
				LC[i1].append([])
				
				ran1=[2]
				if i2>1:
					ran2=[2]
				else:
					ran2=range(nS)
					
			elif i2==bottom2: #initialization first column
				LC[i1]=[[]]
				bottoms[i1]=i2
				ind2=0

				ran1=[1]
				if i1>1:
					ran2=[1]
				else:
					ran2=range(nS)
					
			else:	#middle cells
				LC[i1].append([])
					
				ran1=range(nS)
				ran2=range(nS)
				
			nLC=[]
			for h in range(nS):
				nLC.append([])
				for h2 in range(nS):
					nLC[h].append((0.0,float("-inf")))
			tot=0.0
			for h in ran1:
				newL=0.0
				if h==2:
					r1=0
					r2=1
					phylL=pis[s2[i2-1]]
				elif h==1:
					r1=1
					r2=0
					phylL=pis[s1[i1-1]]
				else:
					r1=1
					r2=1
					phylL=phyl[(s2[i2-1],s1[i1-1])]
				ind2p=i2-(r2+bottoms[i1-r1])
				for h2 in ran2:
					if transProbs[h2][h]>1.0e-18:
						nLC[h][h2]=(LC[i1-r1][ind2p][h2][0]*transProbs[h2][h]*phylL,LC[i1-r1][ind2p][h2][1])

			maxLL=(0.0,float("-inf"))
			for h in range(nS):
				newL=nLC[h][0]
				newL=addBfloats(newL,nLC[h][1])
				newL=addBfloats(newL,nLC[h][2])
				finScore=normBfloats(newL)
				LC[i1][ind2].append(finScore)
				if biggerBfloat(finScore,maxLL):
					maxLL=finScore
				
			#based on the LK score of the best path, we will discard the cell
			maxLog=showBfloat(maxLL)		
			scoresL.append(maxLog)
			if maxLog>bestL:
				bestL=maxLog
			
		i2=0	
		i1=0
		le=len(scoresL)
		while (le-(i1+i2))>5 and (scoresL[i2]-bestL<epsilon or scoresL[le-(i1+1)]-bestL<epsilon):
			if scoresL[i2]<scoresL[le-(i1+1)]:
				bottom2=firstI2+i2+1
				i2+=1
			else:
				bottom1=lastI1+i1+1
				i1+=1	
			
	#final likelihood
	newL=LC[l1][-1][0]
	newL=addBfloats(newL,LC[l1][-1][1])
	newL=addBfloats(newL,LC[l1][-1][2])
	finalLK=showBfloat(newL)
	#print("Final LK: "+str(finalLK))
	return [finalLK, LC]













































#Find SMALL likelihood forward-Needleman-Wunsch
def forwardNWfast(s1, s2, transProbs, stateProbs, probs1, probs2, pi, epsilon=-9.0, alphabet="DNA"):
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA	
	
	states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
	nS=len(states)
	
	consS=[[0],[2,3,5],[1,4,6,7]]
	cons2=[0,2,1,1,2,1,2,2]
	
	#initialization matrix and first cell
	LC=[]
	l1=len(s1)
	l2=len(s2)
	bottom1=0
	bottom2=0
	bottoms=[]
	for i1 in range(l1+1):
		LC.append([])
		bottoms.append(0)
	LC[0].append([])
	for h in range(nS):
		LC[0][0].append([stateProbs[h],0.0])
	
	phyl=[]#phylogenetic probabilities (emission probabilities)
	for i1 in range(4):
		phyl.append([])
		for i2 in range(4):
			phyl[i1].append(0.0)
			for j in range(4):
				phyl[i1][i2]+=pi[j]*probs1[j][i1]*probs2[j][i2]
				
	#iteration over matrix cells
	for totLoop in range(l1+l2):
		totN=totLoop+1
		steps=min(l1,l2,totN,l1+l2-totN)
		if totN<=l1:
			i1=totN
			i2=0
		else:
			i1=l1
			i2=totN-l1
		if i2<bottom2:
			i1-=(bottom2-i2)
			steps-=(bottom2-i2)
			i2=bottom2
		if (i1-steps)<bottom1:
			steps-=(bottom1-(i1-steps))
		firstI2=i2
		lastI1=i1-steps
		bestL=float("-inf")
		i1+=1
		i2-=1
		scoresL=[]
		for s in range(steps+1):
			i1-=1
			i2+=1
			ind2=i2-bottoms[i1]
			if i1==bottom1: #initialization first row
				LC[i1].append([])
				
				ran1=consS[2]
				if i2>1:
					ran2=consS[2]
				else:
					ran2=range(nS)
					
			elif i2==bottom2: #initialization first column
				LC[i1]=[[]]
				bottoms[i1]=i2
				ind2=0

				ran1=consS[1]
				if i1>1:
					ran2=consS[1]
				else:
					ran2=range(nS)
					
			else:	#middle cells
				LC[i1].append([])
					
				ran1=range(nS)
				ran2=range(nS)
				
			maxLL=float("-inf")
			for h in ran1:
				r1=res1(h)
				r2=res2(h)
				ind2p=i2-(r2+bottoms[i1-r1])
				for h2 in ran2:
					val=LC[i1-r1][ind2p][h2][1]
					if val>maxLL:
						maxLL=val
			nLC=[]
			for h in range(nS):
				nLC.append([])
				for h2 in range(nS):
					nLC[h].append(0.0)
			tot=0.0
			for h in ran1:
				newL=0.0
				r1=res1(h)
				r2=res2(h)
				if r1==0:
					phylL=pi[DNAl[s2[i2-1]]]
				elif r2==0:
					phylL=pi[DNAl[s1[i1-1]]]
				else:
					phylL=phyl[DNAl[s1[i1-1]]][DNAl[s2[i2-1]]]
				ind2p=i2-(r2+bottoms[i1-r1])
				factor=math.exp(LC[i1-r1][ind2p][0][1]-maxLL)
				for h2 in ran2:
					if transProbs[h2][h]>0.000000000001:
						nLC[h][h2]=LC[i1-r1][ind2p][h2][0]*transProbs[h2][h]*factor*phylL
						tot+=nLC[h][h2]
			if tot>0.000000000001:
				if tot<0.1:
					for h in ran1:
						for h2 in ran2:
							nLC[h][h2]=nLC[h][h2]/tot
					maxLL+=math.log(tot)
					S=maxLL
				else:
					S=maxLL+math.log(tot)
			else:
				maxLL=float("-inf")
				S=maxLL
			#print "tot: "+str(tot)
			for h in range(nS):
				newL=0.0
				for h2 in range(nS):
					newL+=nLC[h][h2]
				LC[i1][ind2].append([newL,maxLL])
				
			#based on the LK score of the best path, we will discard the cell			
			scoresL.append(S)
			if S>bestL:
				bestL=S
		
		i2=0	
		i1=0
		le=len(scoresL)
		while (le-(i1+i2))>5 and (scoresL[i2]-bestL<epsilon or scoresL[le-(i1+1)]-bestL<epsilon):
			if scoresL[i2]<scoresL[le-(i1+1)]:
				bottom2=firstI2+i2+1
				i2+=1
			else:
				bottom1=lastI1+i1+1
				i1+=1	
			
	#final likelihood
	tot=0.0
	for h2 in range(nS):
		tot+=LC[l1][-1][h2][0]
	fin=LC[l1][-1][0][1]+math.log(tot)
	print("Final LK: "+str(fin))
	return [fin, LC]








































#Find SMALL likelihood forward-Needleman-Wunsch
def backwardNW(s1, s2, transProbs, stateProbs, probs1, probs2, pi, alphabet="DNA"):
	if alphabet=="DNA":
		DNAl=DNA
	else:
		DNAl=AA	
	
	states=["MM","DM","MD","I-M","-MI","I-D","-DI"]
	nS=len(states)
	
	#consistent states (indel emission probabilities)
	consS=[[0],[2,3,5],[1,4,6]]
	
	phyl=[]#phylogenetic probabilities (emission probabilities)
	for i1 in range(4):
		phyl.append([])
		for i2 in range(4):
			phyl[i1].append(0.0)
			for j in range(4):
				phyl[i1][i2]+=pi[j]*probs1[j][i1]*probs2[j][i2]
	
	#L=[]
	LC=[]
	l1=len(s1)
	l2=len(s2)
	for i1 in range(l1+1):
		#L.append([])
		LC.append([])
		for i2 in range(l2+1):
			#L[i1].append([])
			LC[i1].append([])
			if i1==0 and i2==0: #initialization first entry; please be aware that indices are flipped, so 0 corresponds to l1 and l2
				for h in range(nS):
					#L[0][0].append(1.0)
					LC[0][0].append([1.0,0.0])
				
			else: #actual iteration step
				if i2==0:
					ran1=consS[1]
					if i1>1:
						ran2=consS[1]
					else:
						ran2=range(nS)
				elif i1==0:
					ran1=consS[2]
					if i2>1:
						ran2=consS[2]
					else:
						ran2=range(nS)
				else:
					ran1=range(nS)
					ran2=range(nS)
				ave=0.0
				num=0
				for h in ran1:
					r1=res1(h)
					r2=res2(h)
					for h2 in ran2:
						ave+=LC[i1-r1][i2-r2][h2][1]
						num+=1
				ave=ave/num
				nLC=[]
				tot=0.0
				
				
				
				for h in range(nS):
					#newL=0.0
					nLC.append([])
					r1=res1(h)
					r2=res2(h)
					if (h in ran1):
						factor=math.exp(LC[i1-r1][i2-r2][0][1]-ave)
					for h2 in range(nS):
						nLC[h].append(0.0)
						if transProbs[h][h2]>0.000000000001 and (h in ran1) and (h2 in ran2):
							#nL=L[i1-r1][i2-r2][h2]*transProbs[h][h2]
							nLC[h][h2]=LC[i1-r1][i2-r2][h2][0]*transProbs[h][h2]*factor
							if r1==0:
								#nL*=pi[DNA[s2[l2-i2]]]
								nLC[h][h2]*=pi[DNAl[s2[l2-i2]]]
							elif r2==0:
								#nL*=pi[DNA[s1[l1-i1]]]
								nLC[h][h2]*=pi[DNAl[s1[l1-i1]]]
							else:
								#phyloL=phyl[DNA[s1[l1-i1]]][DNA[s2[l2-i2]]]
								#for j in range(4):
								#	phyloL+=pi[j]*probs1[j][DNA[s1[l1-i1]]]*probs2[j][DNA[s2[l2-i2]]]
								#nL*=phyloL
								nLC[h][h2]*=phyl[DNAl[s1[l1-i1]]][DNAl[s2[l2-i2]]]
								#print phyloL
							#newL+=nL
							tot+=nLC[h][h2]
					#L[i1][i2].append(newL)
				if tot<0.000001:
					for h in range(nS):
						r1=res1(h)
						r2=res2(h)
						if (h in ran1):
							factor=math.exp(LC[i1-r1][i2-r2][0][1]-ave)
							print("factor: "+str(factor))
							print(ave)
							print(LC[i1-r1][i2-r2][0][1])
						for h2 in range(nS):
							print(transProbs[h][h2])
							print("LC previous "+str(LC[i1-r1][i2-r2][h2][0]))
							print("phylo: ")
							if r1==0:
								print(pi[DNAl[s2[l2-i2]]])
							elif r2==0:
								print(pi[DNAl[s1[l1-i1]]])
							else:
								print(phyl[DNAl[s1[l1-i1]]][DNAl[s2[l2-i2]]])
				for h in range(nS):
					for h2 in range(nS):
						
						nLC[h][h2]=nLC[h][h2]/tot
				ave+=math.log(tot)
				for h in range(nS):
					newL=0.0
					for h2 in range(nS):
						newL+=nLC[h][h2]
					LC[i1][i2].append([newL,ave])
		
	final=0.0
	for h in range(nS):
		final+=LC[l1][l2][h][0]*stateProbs[h]
		
	fin=LC[l1][l2][0][1]+math.log(final)
	print "finalC: "+str(fin)
	
	return [fin, LC]
	

	
	
	
	





def freqsFromSeqs(Ls1,Ls2, alphabet="DNA"):
	if alphabet=="DNA":
		nS=4
		DNAl=DNA
	else:
		nS=20
		DNAl=AA
	fr=[]
	for i in range(nS):
		fr.append(0)
	tot=0
	for s in range(len(Ls1)):
		s1=Ls1[s]
		s2=Ls2[s]
		for i in range(len(s1)):
			if s1[i]!="-":
				fr[DNAl[s1[i]]]+=1
		for i in range(len(s2)):
			if s2[i]!="-":
				fr[DNAl[s2[i]]]+=1
	for i in range(nS):
		tot+=fr[i]
	for i in range(nS):
		fr[i]=float(fr[i])/tot
	return fr










#infer parameter values from a given alignment
def fixedAlOptimize(s1, s2, t1=0.2, epsilon=-9.0, fast=True, ancestral=True, iModelType='cumIndels', ri=0.1, gi=0.5, model="HKY", kappa=2.0, rates=[0.2,0.4,0.2,0.2,0.4,0.2], optMethod="Nelder-Mead", pypy=True, tree=False, brlens=[]):
	if t1<0.0 or ri<0.0 or gi<0.0 or gi>1.0 or kappa<0.0 or kappa>10.0:
		print("In fixedAlOptimize one of the starting parameters is nonsense, returning -infinity log likelihood!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa))
		return [[], float("-inf")]
	print("\n\n"+"Initial parameters fixedAlOptimize: "+str(t1)+" "+str(ri)+" "+str(gi))
	if isinstance(s1, list):
		Ls1=s1
		Ls2=s2
	else:
		Ls1=[s1]
		Ls2=[s2]
	if len(Ls1)!=len(Ls2):
		print("ERROR: alignment lists of different lengths!")
		exit()
	if tree and len(Ls1)!=len(brlens):
		print("ERROR: alignment lists of different length than branch length list!")
		exit()
	
	if model=="LG":
		alphabet="AA"
		theta_start = [t1,ri,gi]
	else:
		alphabet="DNA"
		theta_start = [t1,ri,gi,kappa]
		
	if tree:
		theta_start = [ri,gi]
		
	#nuc frequencies from observed frequencies
	fr=freqsFromSeqs(Ls1,Ls2, alphabet=alphabet)
		
	#likelihood function (for now just HKY is allowed)
	def like(theta):
		#print(theta)
		L=0.0
		if not tree:
			if ancestral:
				if model=="LG":
					transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=3.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
				else:
					transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=theta[3], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
			else:
				transProbs, stateProbsN, probs1, probs2 =getProbs(theta[0],theta[0],theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=theta[3], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
		
			if len(transProbs)<2:
				return float("inf")
		for s in range(len(Ls1)):
			if tree:
				if ancestral:
					if model=="LG":
						transProbs, stateProbsN, probs1 =getProbs(2*brlens[s],0.0,theta[0],theta[0],theta[1],theta[1], model=model, freqs=fr, kappa=3.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
					else:
						transProbs, stateProbsN, probs1 =getProbs(2*brlens[s],0.0,theta[0],theta[0],theta[1],theta[1], model=model, freqs=fr, kappa=theta[2], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
				else:
					transProbs, stateProbsN, probs1, probs2 =getProbs(brlens[s],theta[0]*brlens[s],theta[0],theta[0],theta[1],theta[1], model=model, freqs=fr, kappa=theta[2], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
		
				if len(transProbs)<2:
					return float("inf")
			s1=Ls1[s]
			s2=Ls2[s]
			if ancestral:
				L+=-SMALL2fwd3(s1,s2, transProbs, stateProbsN, probs1, fr, alphabet=alphabet)
			else:
				L+=-SMALL2fwd(s1,s2, transProbs, stateProbsN, probs1, probs2, fr, alphabet=alphabet)
		#print("new LK value during optimization: "+str(L)+"\n")
		return L
		
	#like = lambda theta: -SMALL2fwd(s1=s1,s2=s2, t1=theta[0], t2=theta[0], iModelType=iModelType, eModelType=eModelType, ri=theta[1], rd=theta[1], gi=theta[2], gd=theta[2], model="HKY", freqs=fr, pi=fr, kappa=theta[3], rates=rates)
	if tree:
		bnds = ((0.0, 10.0), (0.0, 10.0) , (0.0, 1.0) , (0.1, 50.0))
	else:
		bnds = ((0.0, 10.0) , (0.0, 1.0) , (0.1, 50.0))
	to=0.01
	met=["Nelder-Mead","CG","BFGS","L-BFGS-B","TNC","COBYLA","SLSQP"]#Need Jac: ,"Newton-CG" ,"dogleg" ,"trust-ncg"    Not working well: ,"Powell"     Does not exist: ,"trust-exact","trust-krylov"
	m=optMethod
	#for m in met:
	#print("Fixed alignemnt parameter inference with "+m)
	start = time.time()
	#print(like(theta_start)
	from scipy.optimize import minimize
	res = minimize(like, theta_start, method = m, tol=to, bounds=bnds)
	elapsedTime = time.time() - start
	print(res.message)
	print(res.x)
	print("time: "+str(elapsedTime))
	theta=res.x
	LK=0.0
	for s in range(len(Ls1)):
		s1=Ls1[s]
		s2=Ls2[s]
		if ancestral:
			if model=="LG":
				if tree:
					transProbs, stateProbsN, probs1 =getProbs(brlens[s]*2,0.0,theta[0],theta[0],theta[1],theta[1], model=model, freqs=fr, kappa=3.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
				else:
					transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=3.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
			else:
				transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=theta[3], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=pypy)
			#transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=theta[3], rates=rates, ancestral=ancestral, iModelType=iModelType)
			if fast:
				LK+=forwardNW3fast(s1.replace("-",""), s2.replace("-",""), transProbs, stateProbsN, probs1, fr, epsilon=epsilon, alphabet=alphabet)[0]
			else:
				LK+=forwardNW3(s1.replace("-",""), s2.replace("-",""), transProbs, stateProbsN, probs1, fr, alphabet=alphabet)[0]
		else:
			if fast:
				LK+=forwardNWfast(s1.replace("-",""), s2.replace("-",""), transProbs, stateProbsN, probs1, probbs2, fr, epsilon=epsilon, alphabet=alphabet)[0]
			else:
				LK+=forwardNW(s1.replace("-",""), s2.replace("-",""), transProbs, stateProbsN, probs1, probbs2, fr, alphabet=alphabet)[0]

		#LK+=forwardNW(s1=s1.replace("-",""), s2=s2.replace("-",""), t1=theta[0], t2=theta[0], ri=theta[1], rd=theta[1], gi=theta[2], gd=theta[2], model="HKY", freqs=fr, pi=fr, kappa=theta[3], rates=rates)[0]
	print("LK: "+str(LK))
	
	return res.x, LK
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




#ML inference of parameters using the forward approach for the likelihood and the Nelder-Mead for optimization
def forwardOptimize(s1, s2, t1, epsilon=-9.0, fast=True, ancestral=True, iModelType='cumIndels', ri=0.1, gi=0.5, model="HKY", kappa=2.0, seqsFile="", rates=[0.2,0.4,0.2,0.2,0.4,0.2], optMethod="Nelder-Mead", pypy=True, tree=False, brlens=[], folderSimu="/Users/demaio/Desktop/TreeAlign/needle_simu/", baseFolder="/Users/demaio/Desktop/TreeAlign/", pypath="/Applications/pypy2-v6.0.0-osx64/bin/pypy"):
	if t1<0.0001 or ri<0.0001 or gi<0.0001 or gi>0.9999:
		print("In forwardOptimize one of the parameters is nonsense, returning -infinity log likelihood!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa))
		return [[], float("-inf")]
	print("\n\n"+"Initial parameters forwardOptimize: "+str(t1)+" "+str(ri)+" "+str(gi)+" and model "+iModelType)
	if isinstance(s1, list):
		Ls1=s1
		Ls2=s2
	else:
		Ls1=[s1]
		Ls2=[s2]
	if len(Ls1)!=len(Ls2):
		print("ERROR: alignment lists of different lengths!")
		exit()
	if tree and len(Ls1)!=len(brlens):
		print("ERROR: alignment lists of different length than branch length list!")
		exit()
		
	if model=="LG":
		alphabet="AA"
		theta_start = [t1,ri,gi]
		if iModelType=='TKF91':
			theta_start = [t1,ri]
	else:
		alphabet="DNA"
		theta_start = [t1,ri,gi,kappa]
		
	if tree:
		theta_start = [ri,gi]
		if iModelType=='TKF91':
			theta_start = [ri]
		pypy=False
		
	if tree:
		brText="_branches"
	else:
		brText=""
		
	#nuc frequencies from observed frequencies
	fr=freqsFromSeqs(Ls1,Ls2, alphabet=alphabet)
	
	if pypy:
		seqsFile=folderSimu+"seqsFilePyPy"+brText+".txt"
		sFile=open(seqsFile,"w")
		for s in range(len(Ls1)):
			s1=Ls1[s]
			s2=Ls2[s]
			sFile.write(s1+"\n")
			sFile.write(s2+"\n")
		sFile.close()
		
	#likelihood function (for now just HKY is allowed)
	def like(theta):
		L=0.0
		#print(theta)
		if not tree:
			if theta[0]<0.0001 or theta[1]<0.0001:
				return float("inf")
			if ancestral:
				if iModelType=='TKF91':
					transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],0.5,0.5, model=model, freqs=fr, kappa=2.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=False)
				else:
					if theta[2]<0.0001 or theta[2]>0.9999:
						return float("inf")
					transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=2.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=False)
			else:
				transProbs, stateProbsN, probs1, probs2 =getProbs(theta[0],theta[0],theta[1],theta[1],theta[2],theta[2], model=model, freqs=fr, kappa=theta[3], rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=False)
			if len(transProbs)<2:
				return float("inf")
		
		if pypy:
			transPstr=[]
			for i1 in range(3):
				for i2 in range(3):
					transPstr.append(str(transProbs[i1][i2]))
			transPstr=" ".join(transPstr)
			statePstr=[]
			for tr in stateProbsN:
				statePstr.append(str(tr)+" ")
			statePstr=" ".join(statePstr)
			Pstr1=[]
			for i1 in range(20):
				for i2 in range(20):
					Pstr1.append(str(probs1[i1][i2]))
			Pstr1=" ".join(Pstr1)
			resFile=folderSimu+"resultFilePyPy.txt"
			os.system(pypath+" "+baseFolder+"forwardList.py -e "+str(epsilon)+" -s "+seqsFile+" -r "+resFile+" --transProbs "+transPstr+" --stateProbs "+statePstr+" --probs1 "+Pstr1) #+" --fr "+frPstr
			reFile=open(resFile)
			line=reFile.readline()
			reFile.close()
			L=-float(line)
			#print("new LK value during optimization: "+str(L)+"\n")
			return L
		
		for s in range(len(Ls1)):
			if tree:
				if theta[0]<0.0001:
					return float("inf")
				if iModelType=='TKF91':
					transProbs, stateProbsN, probs1 =getProbs(brlens[s]*2,0.0,theta[0],theta[0],0.5,0.5, model=model, freqs=fr, kappa=2.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=False)
				else:
					if theta[1]<0.0001 or theta[1]>0.9999:
						return float("inf")
					transProbs, stateProbsN, probs1 =getProbs(brlens[s]*2,0.0,theta[0],theta[0],theta[1],theta[1], model=model, freqs=fr, kappa=2.0, rates=rates, ancestral=ancestral, iModelType=iModelType, pypy=False)
				if len(transProbs)<2:
					return float("inf")
		
			s1=Ls1[s]
			s2=Ls2[s]
			if ancestral:
				if fast:
					L+=-forwardNW3fast(s1,s2, transProbs, stateProbsN, probs1, fr, epsilon=epsilon, alphabet=alphabet)[0]
				else:
					L+=-forwardNW3(s1,s2, transProbs, stateProbsN, probs1, fr, alphabet=alphabet)[0]
			else:
				if fast:
					L+=-forwardNWfast(s1,s2, transProbs, stateProbsN, probs1, probs2, fr, epsilon=epsilon, alphabet=alphabet)[0]
				else:
					L+=-forwardNW(s1,s2, transProbs, stateProbsN, probs1, probs2, fr, alphabet=alphabet)[0]
		#print("new LK value during optimization: "+str(LK)+"\n")
		return L

		
	#likelihood function (for now just HKY is allowed)
	#like = lambda theta: -forwardNW(s1=s1, s2=s2, t1=theta[0], t2=theta[0], iModelType=iModelType, eModelType=eModelType, ri=theta[1], rd=theta[1], gi=theta[2], gd=theta[2], model="HKY", freqs=fr, pi=fr, kappa=theta[3], rates=rates)[0]
	bnds = ((0.0, 10.0), (0.0, 10.0) , (0.0, 1.0) , (0.1, 50.0))
	if tree:
		bnds = ((0.0, 10.0) , (0.0, 1.0) , (0.1, 50.0))
	to=0.02
	met=["Nelder-Mead","CG","BFGS","L-BFGS-B","TNC","COBYLA","SLSQP"]#Need Jac: ,"Newton-CG" ,"dogleg" ,"trust-ncg"    Not working well: ,"Powell"     Does not exist: ,"trust-exact","trust-krylov"
	m=optMethod
	#for m in met:
	#print("Forward-NW parameter inference with "+m)
	start = time.time()
	
	#print(like(theta_start)
	#minimize NOT IN PYPY!!!!
	#if not pypy:
	from scipy.optimize import minimize
	res = minimize(like, theta_start, method = m, tol=to, bounds=bnds)
	elapsedTime = time.time() - start
	print(res.message)
	print(res.x)
	print("time: "+str(elapsedTime))
	#print("LK at max:")
	LK=-like(res.x)
	print(LK)
	#print "LK at truth:"
	
	return res.x, LK










#ML inference of parameters in H-C real data
def forwardOptimizeHC(s1, s2, t1, epsilon=-9.0, fast=True, ancestral=True, iModelType='cumIndels', ri=0.1, gi=0.95, model="HKY", kappa=3.0, seqsFile="", rates=[0.2,0.4,0.2,0.2,0.4,0.2], optMethod="Nelder-Mead", pypy=True, tree=False, brlens=[]):
	if t1<0.0001 or ri<0.0001 or gi<0.0001 or gi>0.9999:
		print("In forwardOptimize one of the parameters is nonsense, returning -infinity log likelihood!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa))
		return [[], float("-inf")]
	print("Initial parameters forwardOptimize: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa)+" and model "+iModelType)
	if isinstance(s1, list):
		Ls1=s1
		Ls2=s2
	else:
		Ls1=[s1]
		Ls2=[s2]
	if len(Ls1)!=len(Ls2):
		print("ERROR: alignment lists of different lengths!")
		exit()
		
	alphabet="DNA"
	theta_start = [t1,ri,gi,kappa]
		

	brText=""
		
	#nuc frequencies from observed frequencies
	fr=freqsFromSeqs(Ls1,Ls2, alphabet="DNA")
	frDNA=str(fr[0])+" "+str(fr[1])+" "+str(fr[2])+" "+str(fr[3])
	#print(frDNA)
	
	if pypy:
		seqsFile="/Users/demaio/Desktop/TreeAlign/needle_simu/seqsFilePyPy"+brText+".txt"
		sFile=open(seqsFile,"w")
		for s in range(len(Ls1)):
			s1=Ls1[s]
			s2=Ls2[s]
			sFile.write(s1+"\n")
			sFile.write(s2+"\n")
		sFile.close()
		
	#likelihood function (for now just HKY is allowed)
	def like(theta):
		#print(theta)
		start = time.time()
		L=0.0
		#print(theta)
		if theta[0]<0.0001 or theta[1]<0.0001:
			return float("inf")
		if theta[2]<0.0001 or theta[2]>0.9999:
			return float("inf")
		if theta[3]<0.0001 or theta[3]>50.0:
			return float("inf")
		transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,0.0,theta[1],theta[1],theta[2],theta[2], model="HKY", freqs=fr, kappa=theta[3], rates=rates, ancestral=True, iModelType='cumIndels', pypy=False)
		#print(probs1)
		if len(transProbs)<2:
			return float("inf")
		
		if pypy:
			transPstr=[]
			for i1 in range(3):
				for i2 in range(3):
					transPstr.append(str(transProbs[i1][i2]))
			transPstr=" ".join(transPstr)
			statePstr=[]
			for tr in stateProbsN:
				statePstr.append(str(tr)+" ")
			statePstr=" ".join(statePstr)
			Pstr1=[]
			for i1 in range(4):
				for i2 in range(4):
					Pstr1.append(str(probs1[i1][i2]))
			Pstr1=" ".join(Pstr1)
			resFile="/Users/demaio/Desktop/TreeAlign/needle_simu/resultFilePyPy.txt"
			runEps=1
			if runEps:
				#epss=[-10.0,-20.0,-30.0,-40.0,-50.0,-60.0,-70.0,-80.0,-90.0,-100.0]
				epss=[-110.0,-120.0]
				epss=[-130.0,-140.0]
				epss=[-150.0]
				epss=[-25.0,-35.0,-45.0,55.0]
				epss=[]
				for eps in epss:
					os.system(pypath+" /Users/demaio/Desktop/TreeAlign/forwardList.py -e "+str(eps)+" -s "+seqsFile+" -r "+resFile+" --transProbs "+transPstr+" --stateProbs "+statePstr+" --probsDNA "+Pstr1+" --alpha DNA"+" --frDNA "+frDNA) #+" --fr "+frPstr
					reFile=open(resFile)
					line=reFile.readline()
					reFile.close()
					L=-float(line)
					print("\n eps: "+str(eps))
					print(str(L))
				#2554969.41093, 3138154.72267
				#10, 55, 
				lLKs=[2554969.41093,  1561922.55282, 1559636.06206, 1559155.30321, 1559072.7811, 1559303.81983, 1558874.6724, 1558861.81877, 3138154.72267, 1558823.31753, 1558816.63416, 1558792.06083, 1558791.65481, 1558788.0548, 1558786.69378, 1558785.7831, 1558785.06429, 1558785.06429, 1558785.06429]
				epss=[10,20,25,30,35,40,45,50,55,60,70,80,90,100,110,120,130,140,150]
				lLKs=[1561922.55282, 1559636.06206, 1559155.30321, 1559072.7811, 1558874.6724, 1558861.81877, 1558823.31753, 1558816.63416, 1558792.06083, 1558791.65481, 1558788.0548, 1558786.69378, 1558785.7831, 1558785.06429, 1558785.06429, 1558785.06429]
				epss=[20,25,30,35,45,50,60,70,80,90,100,110,120,130,140,150]
				lLKs=[1561922.55282, 1559155.30321, 1558874.6724, 1558823.31753, 1558792.06083, 1558788.0548, 1558785.7831, 1558785.06429, 1558785.06429, 1558785.06429]
				epss=[20,30,45,60,80,100,120,130,140,150]
				plt.semilogy(epss, lLKs)
				plt.xticks(epss) 
				plt.yticks([1558785.06429, 1559000, 1560000, 1561000, 1562000]) 
				#plt.tick_params(axis ='y', rotation =-45) 
				plt.tight_layout()
				plt.savefig('/Users/demaio/Desktop/TreeAlign/Lks_forward_human-chimp.pdf')
				plt.close()
				exit()
			else:
				os.system(pypath+" /Users/demaio/Desktop/TreeAlign/forwardList.py -e "+str(epsilon)+" -s "+seqsFile+" -r "+resFile+" --transProbs "+transPstr+" --stateProbs "+statePstr+" --probsDNA "+Pstr1+" --alpha DNA"+" --frDNA "+frDNA) #+" --fr "+frPstr
				reFile=open(resFile)
				line=reFile.readline()
				reFile.close()
				L=-float(line)
				#print("new LK value during optimization: "+str(L)+"\n")
				return L
		
		for s in range(len(Ls1)):
			s1=Ls1[s]
			s2=Ls2[s]
			L+=-forwardNW3fast(s1,s2, transProbs, stateProbsN, probs1, fr, epsilon=epsilon, alphabet=alphabet)[0]
		elapsedTime = time.time() - start
		print("time: ")
		print(elapsedTime)
		#print("new LK value during optimization: "+str(LK)+"\n")
		return L
		
	#likelihood function (for now just HKY is allowed)
	#like = lambda theta: -forwardNW(s1=s1, s2=s2, t1=theta[0], t2=theta[0], iModelType=iModelType, eModelType=eModelType, ri=theta[1], rd=theta[1], gi=theta[2], gd=theta[2], model="HKY", freqs=fr, pi=fr, kappa=theta[3], rates=rates)[0]
	bnds = ((0.0, 10.0), (0.0, 10.0) , (0.0, 1.0) , (0.1, 50.0))
	to=0.02
	met=["Nelder-Mead","CG","BFGS","L-BFGS-B","TNC","COBYLA","SLSQP"]#Need Jac: ,"Newton-CG" ,"dogleg" ,"trust-ncg"    Not working well: ,"Powell"     Does not exist: ,"trust-exact","trust-krylov"
	m=optMethod
	#for m in met:
	#print("Forward-NW parameter inference with "+m)
	start = time.time()
	
	#print(like(theta_start)
	#minimize NOT IN PYPY!!!!
	#if not pypy:
	from scipy.optimize import minimize
	res = minimize(like, theta_start, method = m, tol=to, bounds=bnds)
	elapsedTime = time.time() - start
	print(res.message)
	print(res.x)
	print("time: "+str(elapsedTime))
	#print("LK at max:")
	LK=-like(res.x)
	print(LK)
	#print "LK at truth:"
	
	return res.x, LK
	
	
	
	




def readSeq(stri):
	file=open(stri)
	lis=[]
	line=file.readline()
	line=file.readline()
	while line!="" and line!="\n":
		lis.append(line.replace("\n",""))
		line=file.readline()
	return "".join(lis)







#def main():
#if args.main:
if __name__ == "__main__":
	#print("main body")
	
	parser = argparse.ArgumentParser(description='Calculate the SMALL of a pairwise alignment, or find the ML alignment of two sequences using the SMALL Needleman-Wunsch.')
	parser.add_argument("--t1", help="lenght of the branch (in expected number of substitutions) of the first sequence", type=float, default=0.1)
	parser.add_argument("--t2", help="lenght of the branch of the second sequence", type=float, default=0.1)
	parser.add_argument("--s1",default="", help='First sequence.')
	parser.add_argument("--s2",default="", help='Second sequence.')

	parser.add_argument("--iModel", help="pairHMM model", default="cumIndels", choices=['cumIndels','TKF91','TKF92','RS07','PRANK'])
	parser.add_argument("--ri", help="instantaneous insertion rate", type=float, default=0.2)
	parser.add_argument("--rd", help="instantaneous deletion rate", type=float, default=0.2)
	parser.add_argument("--gi", help="instantaneous insertion extension probability", type=float, default=0.75)
	parser.add_argument("--gd", help="instantaneous deletion extension probability", type=float, default=0.75)
	
	parser.add_argument("--pypy", help="Avoid scipy to run in pypy", action="store_true")

	parser.add_argument("-o","--outputFile", help="file where to write optimization results", default="")

	parser.add_argument("-e","--epsilon", help="log-probability threshold for discarding DP matrix cells (when using such approximation)", type=float, default=-9.0)
	parser.add_argument("--ancestral", help="The second sequence is ancestral, so run the Markov chain with only 3 states", action="store_true")
	parser.add_argument("--fast", help="Use the fast dynamic programming variants that neglect matrix cells with low likelihood", action="store_true")
	parser.add_argument("--fixAl", help="Run likelihood calculation on a fixed alignment", action="store_true")
	parser.add_argument("--fixPars", help="Run alignment with fixed parameters", action="store_true")
	parser.add_argument("--forwardNW", help="Run foward likelihood calculation over all possible alignments", action="store_true")
	parser.add_argument("--backwardNW", help="Run foward likelihood calculation over all possible alignments", action="store_true")
	parser.add_argument("--forwardOptimize", help="Find ML parameter values by maximizing the forward likelihood", action="store_true")
	parser.add_argument("--fixedOptimize", help="Find ML parameter values by maximizing the forward likelihood", action="store_true")
	parser.add_argument("--BaumWelchNW", help="Find ML parameter values using Baum-Welch", action="store_true")

	parser.add_argument("-m","--model", help="nucleotide substitution model, can by HKY, JC, GTR or LG", default="HKY", choices=['JC', 'HKY', 'GTR','LG'])
	parser.add_argument("-f","--frequencies", help="equilibrium nucleotide frequencies (only used with HKY and GTR models)", nargs=4, type=float, default=[0.3,0.2,0.2,0.3])
	parser.add_argument("-p","--rootFrequencies", help="root nucleotide frequencies (only used with HKY and GTR models)", nargs=4, type=float, default=[0.3,0.2,0.2,0.3])
	parser.add_argument("-k","--kappa", help="kappa rate for the HKY model (only used if the HKY model is selected)", type=float, default=3.0)
	parser.add_argument("-r","--rates", help="rates of the GTR moel (only used if the GTR model is selected). These are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C. The order of the rates is AC, AG, AT, CG, CT, GT.", nargs=6, type=float, default=[0.2,0.4,0.2,0.2,0.4,0.2])
	
	parser.add_argument("--hc", help="Run human-chimp alignment", action="store_true")
	
	args = parser.parse_args()
	
	if args.model=='LG':
		alphabet="AA"
	else:
		alphabet="DNA"
	
	if args.model=='LG' and len(args.rootFrequencies)==4:
		pi=LGfreqs
		freqs=LGfreqs
		#print("Using LG frequencies")
	else:
		pi=args.rootFrequencies
		freqs=args.frequencies
		
	if args.hc:
		#s1=readSeq("/Users/demaio/Desktop/Homo_sapiens.GRCh38.dna.chromosome.20_noNends.fa")
		#s2=readSeq("/Users/demaio/Desktop/Pan_troglodytes.Pan_tro_3.0.dna.chromosome.20.fa")
		file=open("/Users/demaio/Desktop/TreeAlign/hg38.panTro6.synNet.maf")
		line=file.readline()
		line=file.readline()
		line=file.readline()
		lengths=[]
		maxi=0
		while line!="" and line!="\n":
			linelist=line.split()
			length=int(linelist[3])
			lengths.append(length)
			if length>maxi:
				maxi=length
				print(length)
				s1=line
				line=file.readline()
				s2=line
			else:
				line=file.readline()
			line=file.readline()
			line=file.readline()
			line=file.readline()
		print(len(lengths))
		print(maxi)
		file.close()
		name1=s1[:60]
		name2=s2[:60]
		print(name1)
		print(name2)
		lis1=s1.split()
		lis2=s2.split()
		seq1=lis1[6].replace("a","A").replace("c","C").replace("g","G").replace("t","T")
		seq1un=seq1.replace("-","")
		seq2=lis2[6].replace("a","A").replace("c","C").replace("g","G").replace("t","T")
		seq2un=seq2.replace("-","")
		print(len(seq1un))
		print(len(seq2un))
		As=seq1un.count("A")+seq1un.count("a")+seq2un.count("A")+seq2un.count("a")
		Cs=seq1un.count("C")+seq1un.count("c")+seq2un.count("C")+seq2un.count("c")
		Gs=seq1un.count("G")+seq1un.count("g")+seq2un.count("G")+seq2un.count("g")
		Ts=seq1un.count("T")+seq1un.count("t")+seq2un.count("T")+seq2un.count("t")
		tot=As+Cs+Gs+Ts
		freqs=[float(As)/tot, float(Cs)/tot, float(Gs)/tot, float(Ts)/tot]
		print(freqs)
		print(tot)
		pi=freqs
		runInference=0
		if runInference:
			thetas=[[0.00587804, 0.06566545, 0.75894506, 4.50074038]]
			# divs=[0.0058578,0.004,0.008]
# 			divs=[0.008]
# 			rs=[0.060765,0.03,0.09]
# 			rs=[0.09]
# 			gs=[0.75865,0.6,0.9]
# 			gs=[0.9]
# 			kappas=[4.432304,2.5,6.5]
# 			kappas=[6.5]
			#met=["Nelder-Mead","CG","BFGS","L-BFGS-B","TNC","COBYLA","SLSQP"]
			the=0
			for the in range(len(thetas)):
			#for method in range(len(met)):
				print("Running parameter inference: with starting values: "+str(thetas[the][0])+" "+str(thetas[the][1])+" "+str(thetas[the][2])+" "+str(thetas[the][3]))
				start=time.time()
				theta, LK = forwardOptimizeHC(seq1un,seq2un, thetas[the][0], epsilon=-100.0, fast=True, ancestral=True, iModelType="cumIndels", ri=thetas[the][1], gi=thetas[the][2], model="HKY", kappa=thetas[the][3], rates=args.rates, optMethod="Nelder-Mead", pypy=True)
				elapsedTime = time.time() - start
				print("time for human-chimp parameter inference: "+str(elapsedTime)+"\n")
				print(theta)
				print(LK)
		else:
			theta=[0.0058578, 0.060765, 0.75865, 4.432304]
			theta=[0.00587804, 0.06566545, 0.75894506, 4.50074038]

		transProbs, stateProbsN, probs1 =getProbs(theta[0]*2,theta[0],theta[1],theta[1],theta[2],theta[2], model="HKY", freqs=freqs, kappa=theta[3], rates=args.rates, ancestral=True, iModelType=args.iModel, pypy=args.pypy)
		print("Now running pairwise alignment:")
		epsV=-45.0
		epss=[-10.0,-15.0,-20.0,-25.0,-30.0,-35.0,-40.0,-45.0,-50.0,-55.0,-60.0]
		epss=[-40,-65.0,-70.0,-80.0,-90.0,-75]
		epss=[]
		for epsV in epss:
			start=time.time()
			lk, seq1al, seq2al = SMALLneedle3Fast(seq1un,seq2un, transProbs, stateProbsN, probs1, pi, epsilon=epsV, alphabet="DNA")
			elapsedTime = time.time() - start
			print("time for alignment human-chimp with "+str(epsV)+" threshold: "+str(elapsedTime)+"\n")
			oFile=open("/Users/demaio/Desktop/TreeAlign/hg38.panTro6_longestSynteny_alignments"+str(epsV)+".txt","w")
			oFile.write("Parameters inferred:\n")
			for th in theta:
				oFile.write(str(th)+" ")
			oFile.write("\n\n")
			oFile.write("Original alignment:\n")
			oFile.write(name1.replace(" AGAAAATACACTGGT "," ")+" "+seq1+"\n")
			oFile.write(name2.replace(" AGAAAATACACTGGT "," ")+" "+seq2+"\n\n")
			oFile.write("CumIndel alignment:\n")
			oFile.write(name1.replace(" AGAAAATACACTGGT "," ")+" "+seq1al+"\n")
			oFile.write(name2.replace(" AGAAAATACACTGGT "," ")+" "+seq2al+"\n\n")
			oFile.close()
		lLKs=[764859.607312,763462.981809,763300.409149,763232.364477,763205.132341,763200.061678,763197.670879,763197.670879,763197.670879,763191.104099,763191.104099,763191.104099,763191.104099,763191.104099,763191.104099]
		epss=[10,15,20,25,30,35,40,45,50,55,60,65,70,75,80]
		plt.semilogy(epss, lLKs)
		plt.xticks(epss) 
		plt.yticks([763191.104099, 763500, 764000, 764500, 765000]) 
		#plt.tick_params(axis ='y', rotation =-45) 
		plt.tight_layout()
		plt.savefig('/Users/demaio/Desktop/TreeAlign/Lks_Needle_human-chimp.pdf')
		plt.close()
		exit()
		exit()
	
	#start=time.time()
	if (not args.forwardOptimize) and (not args.fixedOptimize):
		if args.ancestral:
			transProbs, stateProbsN, probs1 =getProbs(args.t1,args.t2,args.ri,args.rd,args.gi,args.gd, model=args.model, freqs=freqs, kappa=args.kappa, rates=args.rates, ancestral=args.ancestral, iModelType=args.iModel, pypy=args.pypy)
		else:
			transProbs, stateProbsN, probs1, probs2 =getProbs(args.t1,args.t2,args.ri,args.rd,args.gi,args.gd, model=args.model, freqs=freqs, kappa=args.kappa, rates=args.rates, ancestral=args.ancestral, iModelType=args.iModel, pypy=args.pypy)
	#elapsedTime = time.time() - start
	#print("time to calculate probabilities: "+str(elapsedTime)+"\n")
	
	if args.fixAl:
		if args.ancestral:
			print(SMALL2fwd3(args.s1,args.s2, transProbs, stateProbsN, probs1, pi, alphabet=alphabet))
		else:
			print(SMALL2fwd(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi, alphabet=alphabet))

	if args.fixPars:
		if args.ancestral:
			if args.fast:
				SMALLneedle3Fast(args.s1,args.s2, transProbs, stateProbsN, probs1, pi, epsilon=args.epsilon, alphabet=alphabet)
			else:
				SMALLneedle3(args.s1,args.s2, transProbs, stateProbsN, probs1, pi, alphabet=alphabet)
		else:
			if args.fast:
				print(SMALLneedleFast(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi, epsilon=args.epsilon))
			else:
				print(SMALLneedle(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi))
				
	if args.forwardNW:
		if args.ancestral:
			if args.fast:
				forwardNW3fast(args.s1,args.s2, transProbs, stateProbsN, probs1, pi, epsilon=args.epsilon, alphabet=alphabet)
			else:
				forwardNW3(args.s1,args.s2, transProbs, stateProbsN, probs1, pi, alphabet=alphabet)
		else:
			if args.fast:
				print(forwardNWfast(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi, epsilon=args.epsilon)[0])
			else:
				print(forwardNW(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi)[0])
				
	if args.backwardNW:
		if args.ancestral:
			print("backward with 3 states not yet implemented")
		else:
			if args.fast:
				print("backward fast not yet implemented")
			else:
				print(backwardNW(args.s1,args.s2, transProbs, stateProbsN, probs1, probs2, pi)[0])

	if args.forwardOptimize:
		if args.outputFile!="":
			fileO=open(args.outputFile,"w")
			theta, LK = forwardOptimize(args.s1,args.s2, args.t1, epsilon=args.epsilon, fast=args.fast, ancestral=args.ancestral, iModelType=args.iModel, ri=args.ri, gi=args.gi, model=args.model, kappa=args.kappa, rates=args.rates, optMethod="Nelder-Mead", pypy=args.pypy)
			for th in theta:
				fileO.write(str(th)+" ")
			fileO.write("\n")
			fileO.close()
		else:
			print(forwardOptimize(args.s1,args.s2, args.t1, epsilon=args.epsilon, fast=args.fast, ancestral=args.ancestral, iModelType=args.iModel, ri=args.ri, gi=args.gi, model=args.model, kappa=args.kappa, rates=args.rates, optMethod="Nelder-Mead", pypy=args.pypy))
	
	if args.fixedOptimize:
		print(fixedAlOptimize(args.s1,args.s2, args.t1, epsilon=args.epsilon, fast=args.fast, ancestral=args.ancestral, iModelType=args.iModel, ri=args.ri, gi=args.gi, model=args.model, kappa=args.kappa, rates=args.rates, optMethod="Nelder-Mead", pypy=args.pypy))
	
	
	if (not args.BaumWelchNW) and (not args.forwardOptimize)  and (not args.fixedOptimize) and (not args.backwardNW) and (not args.forwardNW) and (not args.fixPars) and (not args.fixAl):
	
		print(len("AAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"))
	
		start=time.time()
		print(SMALL2fwd3("RSTRSTAAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"*10,"RSTRST---------TTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTT---------"*10, transProbs, stateProbsN, probs1, pi, alphabet=alphabet))
		elapsedTime = time.time() - start
		print("time for likelihood of an alignment, 3 states: "+str(elapsedTime)+"\n")
	
		start=time.time()
		print(SMALLneedle3("RSTRSTAAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"*2,"RSTRSTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTT"*2, transProbs, stateProbsN, probs1, pi, alphabet=alphabet))
		elapsedTime = time.time() - start
		print("time for alignment, 3 states: "+str(elapsedTime)+"\n")
	
		start=time.time()
		for jk in range(3):
			SMALLneedle3Fast("RSTRSTAAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"*2,"RSTRSTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTT"*2, transProbs, stateProbsN, probs1, pi, epsilon=args.epsilon, alphabet=alphabet)
		elapsedTime = time.time() - start
		print("time for fast alignment, 3 states: "+str(elapsedTime)+"\n")
		
		start=time.time()
		print(forwardNW3("RSTRSTAAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"*2,"RSTRSTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTT"*2, transProbs, stateProbsN, probs1, pi, alphabet=alphabet)[0])
		elapsedTime = time.time() - start
		print("time new forward NW 3: "+str(elapsedTime)+"\n")
		
		start=time.time()
		print(forwardNW3fast("RSTRSTAAAAAAAAATTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTAAAAAAAAA"*2,"RSTRSTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTTTGAGTT"*2, transProbs, stateProbsN, probs1, pi, epsilon=args.epsilon, alphabet=alphabet)[0])
		elapsedTime = time.time() - start
		print("time forward 3 fast: "+str(elapsedTime)+"\n")
		
		
	
	exit()


