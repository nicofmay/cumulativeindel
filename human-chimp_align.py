import sys
import math
import time
import os
import argparse



DNA={"A":0, "C":1, "G":2, "T":3}
AAs = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
AA = {'A':0, 'R':1, 'N':2, 'D':3, 'C':4, 'Q':5, 'E':6, 'G':7, 'H':8, 'I':9, 'L':10, 'K':11, 'M':12, 'F':13, 'P':14, 'S':15, 'T':16, 'W':17, 'Y':18, 'V':19}
LGfreqs=[0.079066, 0.055941, 0.041977, 0.053052, 0.012937, 0.040767, 0.071586, 0.057337, 0.022355, 0.062157, 0.099081, 0.064600, 0.022951, 0.042302, 0.044040, 0.061197, 0.053287, 0.012066, 0.034155, 0.069147 ]

pypath="/Applications/pypy2-v6.0.0-osx64/bin/pypy"


#iFile=open("/Users/demaio/Desktop/hg38.panTro6_longestSynteny_alignments.txt")
iFile=open("hg38.panTro6_longestSynteny_alignments-80.0.txt")

line=iFile.readline()
pars=iFile.readline()
line=iFile.readline()
line=iFile.readline()
line=iFile.readline()
linelist=line.split()
seq1=linelist[7]
line=iFile.readline()
linelist=line.split()
seq2=linelist[7]

line=iFile.readline()
line=iFile.readline()
line=iFile.readline()
linelist=line.split()
seq1CI=linelist[7]
line=iFile.readline()
linelist=line.split()
seq2CI=linelist[7]
iFile.close()

print("kappa=1 transi/transv")
transv=0.314*(0.179+0.325) + 0.179*(0.314+0.182) + 0.182*(0.179+0.325) + 0.325*(0.314+0.182)
transi=0.314*0.182 + 0.179*0.325 + 0.182*0.314 + 0.325*0.179
print(transi)
print(transv)
kappaNat=transi/transv
print(transi/transv)


print("#Gaps in the four sequences:")
print(seq1.count("-"))
print(seq2.count("-"))
print(seq1CI.count("-"))
print(seq2CI.count("-"))
print("Lengths of the two alignments:")
print(len(seq1))
print(len(seq1CI))
print("Lengths of the two sequences:")
print(len(seq1.replace("-","")))
print(len(seq2.replace("-","")))
print("#substitutions in the two alignments:")
numsub1=0
numTransi=0
numTransv=0
mCols=0
oldM1=1
oldM2=1
gappedC=0
numInd=0
lengths1=[]
lengths2=[]
len1=0
len2=0
for i in range(len(seq1)):
	if seq1[i]!="-" and seq2[i]!="-":
		if oldM1==0:
			oldM1=1
			lengths1.append(len1)
			len1=0
		if oldM2==0:
			oldM2=1	
			lengths2.append(len2)
			len2=0
		mCols+=1
		if seq1[i]!=seq2[i]:
			numsub1+=1
			if (seq1[i]=="A" and seq2[i]=="G") or (seq1[i]=="G" and seq2[i]=="A") or (seq1[i]=="C" and seq2[i]=="T") or (seq1[i]=="T" and seq2[i]=="C"):
				numTransi+=1
			else:
				numTransv+=1
	else:
		gappedC+=1
		if oldM1==1 and seq1[i]=="-":
			numInd+=1
			oldM1=0
			len1=1
		elif oldM1==0 and seq1[i]!="-":
			oldM1=1
			lengths1.append(len1)
			len1=0
		elif oldM1==0 and seq1[i]=="-":
			len1+=1
		if oldM2==1 and seq2[i]=="-":
			numInd+=1
			oldM2=0
			len2=1
		elif oldM2==0 and seq2[i]!="-":
			oldM2=1
			lengths2.append(len2)
			len2=0
		elif oldM2==0 and seq2[i]=="-":
			len2+=1

print(lengths1)
print(lengths2)
		
print(numsub1)
print(numTransi)
print(numTransv)
print(kappaNat)
print("t1= "+str(float(numsub1)/(2*mCols)))
print(float(numTransi)/numTransv)
print("kappa1:"+str((float(numTransi)/numTransv)/(kappaNat)))
print(numInd)
print(gappedC)
print("r1: "+str(float(numInd)/(2.0*numsub1)))
print("g1: "+str(1.0-1.0/(float(gappedC)/numInd)))
numTransi2=0
numTransv2=0
numsub2=0
mCols2=0
oldM1=1
oldM2=1
gappedC2=0
numInd2=0
for i in range(len(seq1CI)):
	if seq1CI[i]!="-" and seq2CI[i]!="-":
		if oldM1==0:
			oldM1=1
		if oldM2==0:
			oldM2=1	
		mCols2+=1
		if seq1CI[i]!=seq2CI[i]:
			numsub2+=1
			if (seq1CI[i]=="A" and seq2CI[i]=="G") or (seq1CI[i]=="G" and seq2CI[i]=="A") or (seq1CI[i]=="C" and seq2CI[i]=="T") or (seq1CI[i]=="T" and seq2CI[i]=="C"):
				numTransi2+=1
			else:
				numTransv2+=1
	else:
		gappedC2+=1
		if oldM1==1 and seq1CI[i]=="-":
			numInd2+=1
			oldM1=0
		elif oldM1==0 and seq1CI[i]!="-":
			oldM1=1
		if oldM2==1 and seq2CI[i]=="-":
			numInd2+=1
			oldM2=0
		elif oldM2==0 and seq2CI[i]!="-":
			oldM2=1
print(numsub2)
print("t2= "+str(float(numsub2)/(2*mCols2)))
print(float(numTransi2)/numTransv2)
print("kappa2:"+str((float(numTransi2)/numTransv2)/(kappaNat)))
print(numInd2)
print("r2: "+str(float(numInd2)/(2.0*numsub2)))
print("g2: "+str(1.0-1.0/(float(gappedC2)/numInd2)))

i=0
i1=0
i2=0
j=0
j1=0
j2=0
lastdiff=0
lastdiff2=0
xlen=200
while(i<len(seq1)-1):
	
	
	if (seq1[i]!=seq1CI[j] or seq2[i]!=seq2CI[j]) and (i-lastdiff>(xlen/2)) and (j-lastdiff2>(xlen/2)):
		nsubs1=0
		nindels1=0
		nsubs2=0
		nindels2=0
		for inew in range(xlen):
			if seq1[i-xlen/2+inew]!=seq2[i-xlen/2+inew] and seq1[i-xlen/2+inew]!="-" and seq2[i-xlen/2+inew]!="-":
				nsubs1+=1
			if seq1CI[j-xlen/2+inew]!=seq2CI[j-xlen/2+inew] and seq1CI[j-xlen/2+inew]!="-" and seq2CI[j-xlen/2+inew]!="-":
				nsubs2+=1
			if seq1[i-xlen/2+inew]=="-" or seq2[i-xlen/2+inew]=="-":
				nindels1+=1
			if seq1CI[j-xlen/2+inew]=="-" or seq2CI[j-xlen/2+inew]=="-":
				nindels2+=1
		if nsubs1!=nsubs2 or nindels1!=nindels2:
			lastdiff=i
			lastdiff2=j
			if i==154871:
				print("Difference: "+str(i)+" "+str(j))
				print(seq1[i-xlen/2:i+3*xlen/2])
				print(seq2[i-xlen/2:i+3*xlen/2])
				print(seq1CI[j-xlen/2:j+3*xlen/2])
				print(seq2CI[j-xlen/2:j+3*xlen/2])
			if i> 459900 and i< 460321:
				if i<459950:
					print("Difference: "+str(i)+" "+str(j))
					file=open("human-chimp_alignments/alignmentLASTZ_"+str(i)+".fst","w")
					print(seq1[i-xlen/2:i+3*xlen/2])
					print(seq2[i-xlen/2:i+3*xlen/2])
					file.write(">H\n"+seq1[i-xlen/2:i+3*xlen/2]+"\n")
					file.write(">C\n"+seq2[i-xlen/2:i+3*xlen/2]+"\n")
					file.close()
					file=open("human-chimp_alignments/alignment_CumIndel_"+str(j)+".fst","w")
					print(seq1CI[j-xlen/2:j+3*xlen/2])
					print(seq2CI[j-xlen/2:j+3*xlen/2])
					file.write(">H\n"+seq1CI[j-xlen/2:j+3*xlen/2]+"\n")
					file.write(">C\n"+seq2CI[j-xlen/2:j+3*xlen/2]+"\n")
					file.close()
			else:
				print("Difference: "+str(i)+" "+str(j))
				file=open("human-chimp_alignments/alignmentLASTZ_"+str(i)+".fst","w")
				print(seq1[i-xlen/2:i+xlen/2])
				print(seq2[i-xlen/2:i+xlen/2])
				file.write(">H\n"+seq1[i-xlen/2:i+xlen/2]+"\n")
				file.write(">C\n"+seq2[i-xlen/2:i+xlen/2]+"\n")
				file.close()
				file=open("human-chimp_alignments/alignment_CumIndel_"+str(j)+".fst","w")
				print(seq1CI[j-xlen/2:j+xlen/2])
				print(seq2CI[j-xlen/2:j+xlen/2])
				file.write(">H\n"+seq1CI[j-xlen/2:j+xlen/2]+"\n")
				file.write(">C\n"+seq2CI[j-xlen/2:j+xlen/2]+"\n")
				file.close()
			print(" ")
	if i1>j1 and i2>j2:
		j+=1
		if seq1CI[j]!="-":
			j1+=1
		if seq2CI[j]!="-":
			j2+=1
	elif i1<j1 and i2<j2:
		i+=1
		if seq1[i]!="-":
			i1+=1
		if seq2[i]!="-":
			i2+=1
	else:
		i+=1
		if seq1[i]!="-":
			i1+=1
		if seq2[i]!="-":
			i2+=1
		j+=1
		if seq1CI[j]!="-":
			j1+=1
		if seq2CI[j]!="-":
			j2+=1
	
exit()



