import dendropy
#import sys
import math
#import time
#import os
import numpy as np
from scipy.linalg import expm
#from scipy.linalg import norm
#from scipy.optimize import minimize
from discreteMarkovChain import markovChain
#from scipy import integrate as integ


DNA={"A":0, "C":1, "G":2, "T":3}
AAs = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
AA = {'A':0, 'R':1, 'N':2, 'D':3, 'C':4, 'Q':5, 'E':6, 'G':7, 'H':8, 'I':9, 'L':10, 'K':11, 'M':12, 'F':13, 'P':14, 'S':15, 'T':16, 'W':17, 'Y':18, 'V':19}
LGfreqs=[0.079066, 0.055941, 0.041977, 0.053052, 0.012937, 0.040767, 0.071586, 0.057337, 0.022355, 0.062157, 0.099081, 0.064600, 0.022951, 0.042302, 0.044040, 0.061197, 0.053287, 0.012066, 0.034155, 0.069147 ]



#NICOLA: ALLOW other pairHMMs
#define function to get transition probabilities for other pairHMMs
def getTransProbsOthers(t1,ri,rd,gi,gd,iModelType='TKF91'):
	if t1<0.0 or ri<0.0 or rd<0.0 or gi<0.0 or gd<0.0 or gi>1.0 or gd>1.0:
		print("One of the pairHMM parameters is nonsense!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
		return []
	
	if iModelType=='TKF91':
		#parameters: ri (lambda) and rd (mu)
		#states=["M","I","D"]
		if ri==rd:
			btM=ri*t1/(1.0+ri*t1)
		else:
			btM=ri*(1.0-math.exp((ri-rd)*t1))/(rd-ri*math.exp((ri-rd)*t1))
		gtM=1.0-math.exp(-rd*t1)
		if t1<0.000000001:
			btD=0.0 #1.0/(1.0+ri*t1)
		else:
			btD=1.0-rd*btM/(gtM*ri)
		transProbs=[[(1.0-btM)*(1.0-gtM),  btM,  (1.0-btM)*gtM],
		[(1.0-btM)*(1.0-gtM),  btM,  (1.0-btM)*gtM],
		[(1.0-btD)*(1.0-gtM),  btD,  (1.0-btD)*gtM]]
		return transProbs

		
	elif iModelType=='TKF92':
		#parameters: ri (lambda), rd (mu) and gi (epsilon)
		#states=["M","I","D"]
		tScale=t1/(1.0-gi)
		gtM=1.0-math.exp(-rd*tScale)
		if ri==rd:
			#btM=ri*t1/(1.0+ri*tScale) #replace t1 with tScale?
			btM=ri*tScale/(1.0+ri*tScale) #replace t1 with tScale?
		else:
			#btM=ri*(1.0-math.exp((ri-rd)*tScale))/(rd/(1.0-gi)-ri*math.exp((ri-rd)*tScale)/(1.0-gi)) #what's up with the last /(1-gi)?
			btM=ri*(1.0-math.exp((ri-rd)*tScale))/(rd-ri*math.exp((ri-rd)*tScale))
		if t1<0.000000001:
			btD=0.0 #1.0/(1.0+ri*tScale)
		else:
			btD=1.0-rd*btM/(gtM*ri)
		transProbs=[[(1.0-gi)*(1.0-btM)*(1.0-gtM) + gi,  (1.0-gi)*btM,  (1.0-gi)*(1.0-btM)*gtM],
		[(1.0-gi)*(1.0-btM)*(1.0-gtM),  (1.0-gi)*btM + gi,  (1.0-gi)*(1.0-btM)*gtM],
		[(1.0-gi)*(1.0-btD)*(1.0-gtM),  (1.0-gi)*btD,  (1.0-gi)*(1.0-btD)*gtM + gi]]
		return transProbs
	
	
	elif iModelType=='RS07':
		#parameters: gi (epsilon) and ri (lambda)
		#states=["M","I","D"]
		delta1=1.0-math.exp(-ri*t1/(1.0-gi))
		delta=delta1/(1.0+delta1)
		transProbs=[[gi + (1.0-gi)*(1.0-2*delta),  (1.0-gi)*delta,  (1.0-gi)*delta],
		[(1.0-gi)*(1.0-2*delta),  gi +(1.0-gi)*delta,  (1.0-gi)*delta],
		[(1.0-gi)*(1.0-2*delta),  (1.0-gi)*delta,  gi +(1.0-gi)*delta]]
		return transProbs
	
	
	elif iModelType=='PRANK':
		#parameters: gd (gamma), gi (epsilon) and ri (r)
		#states=["M","I","D"]
		delta=1.0-math.exp(-ri*t1/(1.0-gd))
		if delta>=0.5 or delta<=0.0:
			delta=0.0000004
			#return []
		transProbs=[[gd + (1.0-gd)*(1.0-2*delta),  (1.0-gd)*delta,  (1.0-gd)*delta],
		[(1.0-gi)*(1.0-2*delta),  gi +(1.0-gi)*delta,  (1.0-gi)*delta],
		[(1.0-gi)*(1.0-2*delta),  (1.0-gi)*delta,  gi +(1.0-gi)*delta]]
		return transProbs
		
	elif iModelType=='PRANK0':
		#parameters: gd (gamma), gi (epsilon) and ri (r)
		#states=["M","I","D"]
		delta=1.0-math.exp(-ri*t1)
		if delta>=0.5 or delta<=0.0:
			delta=0.0000004
			#return []
		transProbs=[[(1.0-2*delta),  delta,  delta],
		[(1.0-gi)*(1.0-2*delta),  gi +(1.0-gi)*delta,  (1.0-gi)*delta],
		[(1.0-gi)*(1.0-2*delta),  (1.0-gi)*delta,  gi +(1.0-gi)*delta]]
		return transProbs
	
	
	else:
		print("Error: indel model (pairHMM) not recognized!")
		return []
	



	

	




def getIndelProbsSingle(t1,ri,rd,gi,gd):
	from scipy import integrate as integ
	probs=[]
	ngd=1.0-gd
	ngi=1.0-gi
	y0=[0.0,0.0,0.0,0.0,1.0]
	dLen=rd/ngd
	iLen=ri/ngi
	alpha=-dLen
	def func2(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] surviving homology columns followed by deletions, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
		if y[0]<0.000000000001:
			gti=gi
		else:
			gti=1.0-y[0]/y[2]
		nPiUp=y[4]-y[0]
		denom2=1.0/(1.0-gti*gd)
		Pi=y[0]/y[4]
		nPi=1.0-Pi
		denom=denom2/(1.0-gd*nPi)
		PineritIn=Pi*ngd*denom
		PinsLost=(1.0-gti)*ngd*denom
		return [  nPiUp*ri - y[0]*dLen + nPiUp*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + rd*(y[0]-y[3])*gd*denom2      ,    (y[4]+y[2])*iLen - y[2]*dLen     ,    (y[1]-y[3])*ri - y[3]*dLen + (y[0]-y[3])*rd*denom2 + nPiUp*rd*PineritIn + (y[0]-y[3])*rd*PineritIn*gd*(1.0-gti)*denom2 - y[3]*rd*PinsLost      ,    y[4]*alpha       ]   #
	if t1>0.005:
		tapp1 = np.arange(0,t1, 0.001)
	else:
		tapp1 = np.arange(0,t1, t1/5)
	y2 = integ.odeint(func2, y0, tapp1)
	y1l=y2[len(y2)-1]
	probs.append(y1l[0]/y1l[4])
	probs.append(y1l[1]/y1l[4])
	probs.append(y1l[3]/y1l[4])
	probs.append(1.0-y1l[0]/y1l[2])
	probs.append(1.0-y1l[1]/(1.0-y1l[4]))
	#print(y1l[4])
	#print(y1l[0])
	#print(y1l[1])
	#print(y1l[3])
	#print(y1l[2])
	#exit()
	return probs


# def getIndelProbsSingle(t1,ri,rd,gi,gd):
# 	from scipy import integrate as integ
# 	probs=[]
# 	y0=[0.0,0.0,0.0,0.0,100.0,0.0]
# 	def func(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] total deleted ancestral bases, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
# 		dLen=rd/(1.0-gd)
# 		if y[0]<0.000000000001:
# 			gti=gi
# 		else:
# 			gti=1.0-y[0]/y[2]
# 		PineritIn=(y[0]/y[4])*(1.0-gd)/((1.0-(1.0-y[0]/y[4])*gd)*(1.0-gd*gti))
# 		PinsLost=(1.0-gti)*(1.0-gd)/((1.0-gd*(1.0-y[0]/y[4]))*(1.0-gti*gd))
# 		return [  (y[4]-y[0])*ri - y[0]*dLen + (y[4]-y[0])*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + rd*(y[0]-y[5])*gd/(1.0-gd*gti)      ,    (y[4]+y[2])*ri/(1.0-gi) - y[2]*dLen     ,    y[4]*dLen       ,    -y[4]*dLen     ,      (y[1]-y[5])*ri - y[5]*dLen + (y[0]-y[5])*rd/(1.0-gti*gd) + (y[4]-y[0])*rd*PineritIn + (y[0]-y[5])*rd*PineritIn*gd*(1.0-gti)/(1.0-gti*gd) - y[5]*rd*PinsLost  ]   #
# 	if t1>0.005:
# 		tapp1 = np.arange(0,t1, 0.001)
# 	else:
# 		tapp1 = np.arange(0,t1, t1/5)
# 	y1 = integ.odeint(func, y0, tapp1)
# 	y1l=y1[len(y1)-1]
# 	probs.append(y1l[0]/y1l[4])
# 	probs.append(y1l[1]/y1l[4])
# 	probs.append(y1l[5]/y1l[4])
# 	probs.append(1.0-y1l[0]/y1l[2])
# 	probs.append(1.0-y1l[1]/y1l[3])
# 	return probs
	
	

	
 
#transition probabilities between hidden states in the simplified pairwise case
def getTransProbsPair(PI, PD, PID, gi, gd):
	#states=["M","I","D"]
	transProbs=[[1.0+PID-(PI+PD),  PI-PID,  PD],
	[(1.0-gi),  gi,  0.0],
	[(1.0-gd)*(PD-PID)/PD,  (1.0-gd)*PID/PD,  gd]]
	return transProbs





#transition probabilities between hidden states
#Fix to put deletions first!!!
def getTransProbs(PI1, PI2, PD1, PD2, PID1, PID2, gi1, gi2, gd1, gd2):
	PMM1=1.0-(PI1+PD1)+PID1
	PMM2=1.0-(PI2+PD2)+PID2
	PMI1=PI1
	PMI2=PI2
	PMD1=PD1-PID1
	PMD2=PD2-PID2
	PIM1=(1.0-gi1)*(PI1-PID1)/PI1
	PIM2=(1.0-gi2)*(PI2-PID2)/PI2
	PII1=gi1
	PII2=gi2
	PID1=(1.0-gi1)*PID1/PI1
	PID2=(1.0-gi2)*PID2/PI2
	PDM1=1.0-gd1
	PDM2=1.0-gd2
	PDD1=gd1
	PDD2=gd2
	#states: MM, DM, MD, I-M, -MI, I-D, -DI, -II
	#print "getTransProbs parameters: "+str(PI1)+" "+str(PI2)+" "+str(PD1)+" "+str(PD2)+" "+str(gi1)+" "+str(gi2)+" "+str(gd1)+" "+str(gd2)+" "
	transProbs=[[PMM1*PMM2,  PMD1*PMM2,  PMM1*PMD2,  PMI1,  (1.0-PI1)*PMI2, 0.0, 0.0, 0.0],
	[ PDM1*PMM2,  PDD1*PMM2, PDM1*PMD2,  0.0,  0.0, 0.0, PMI2, 0.0],
	[ PMM1*PDM2,  PMD1*PDM2, PMM1*PDD2,  0.0,  0.0, PMI1, 0.0, 0.0],
	
	[ PIM1*PMM2, PID1*PMM2,  PIM1*PMD2,  PII1,  0.0, 0.0, 0.0, (1.0-gi1)*PMI2],
	[ PMM1*PIM2/(1.0-PI1),  PMD1*PIM2/(1.0-PI1),  PMM1*PID2/(1.0-PI1), 0.0,  PII2, 0.0, 0.0, 0.0],
	
	[ PIM1*PDM2, PID1*PDM2,  PIM1*PDD2,  0.0,  0.0, PII1, 0.0, 0.0],
	[ PDM1*PIM2, PDD1*PIM2, PDM1*PID2, 0.0,  0.0, 0.0, PII2, 0.0],
	
	[ PIM1*PIM2/(1.0-gi1), PID1*PIM2/(1.0-gi1), PIM1*PID2/(1.0-gi1), 0.0, 0.0, 0.0, 0.0, PII2]]
	
	#correcting for only-gap columns
	toDD=[PMD1*PMD2, PDD1*PMD2, PMD1*PDD2, PID1*PMD2, PMD1*PID2/(1.0-PI1), PID1*PDD2, PDD1*PID2, PID1*PID2/(1.0-gi1) ]
	fromDD=[ PDM1*PDM2, PDD1*PDM2, PDM1*PDD2, 0.0 , 0.0 , 0.0 , 0.0, 0.0 ]
	factor=1.0/(1.0-gd1*gd2)
	for j in range(8):
		for k in range(8):
			transProbs[j][k] = transProbs[j][k] + toDD[j]*fromDD[k]*factor
	return transProbs
	
	
	
	
	


#Nucleotide substitution model
def nucModel(modelType="JC", freqs=[0.25,0.25,0.25,0.25], pi=[0.25,0.25,0.25,0.25], kappa=1.0, rates=[0.2,0.4,0.2,0.2,0.4,0.2]):
	#root frequencies
	s=sum(freqs)
	if s!=1.0:
		for i in range(4):
			freqs[i]=freqs[i]/s
	#Equilibrium frequencies
	s2=sum(pi)
	if s2!=1.0:
		for i in range(4):
			pi[i]=pi[i]/s2
	#Rate matrix
	if modelType=="JC":
		import dendropy
		model = dendropy.model.discrete.Jc69(state_alphabet=None, rng=None)
		qMatrix=model.qmatrix(rate=1.0)
		freqs=[0.25,0.25,0.25,0.25]
		pi=[0.25,0.25,0.25,0.25]
	elif modelType=="HKY":
		import dendropy
		model = dendropy.model.discrete.Hky85(kappa, base_freqs=freqs, state_alphabet=None, rng=None)
		qMatrix=model.qmatrix(rate=1.0)
		#print(freqs)
		#print(qMatrix)
	elif modelType=="GTR":
		#rates of a GTR model: these are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C.
		# the order of the rates is AC, AG, AT, CG, CT, GT.
		qMatrix2=[[0.0,rates[0]*freqs[1],rates[1]*freqs[2],rates[2]*freqs[3]], [rates[0]*freqs[0],0.0,rates[3]*freqs[2],rates[4]*freqs[3]], [rates[1]*freqs[0],rates[3]*freqs[1],0.0,rates[5]*freqs[3]], [rates[2]*freqs[0],rates[4]*freqs[1],rates[5]*freqs[2],0.0]]
		qMatrix2[0][0]=-(rates[0]*freqs[1]+rates[1]*freqs[2]+rates[2]*freqs[3])
		qMatrix2[1][1]=-(rates[0]*freqs[0]+rates[3]*freqs[2]+rates[4]*freqs[3])
		qMatrix2[2][2]=-(rates[1]*freqs[0]+rates[3]*freqs[1]+rates[5]*freqs[3])
		qMatrix2[3][3]=-(rates[2]*freqs[0]+rates[4]*freqs[1]+rates[5]*freqs[2])
		totRate=-freqs[0]*qMatrix2[0][0]-freqs[1]*qMatrix2[1][1]-freqs[2]*qMatrix2[2][2]-freqs[3]*qMatrix2[3][3]
		for i in range(4):
			for j in range(4):
				qMatrix2[i][j]=qMatrix2[i][j]/totRate
		qMatrix=[qMatrix2]
	elif modelType=="LG":
		LGrates=[[0,0.425093,0.276818,0.395144,2.489084,0.969894,1.038545,2.06604,0.358858,0.14983,0.395337,0.536518,1.124035,0.253701,1.177651,4.727182,2.139501,0.180717,0.218959,2.54787],
		[0.425093,0,0.751878,0.123954,0.534551,2.807908,0.36397,0.390192,2.426601,0.126991,0.301848,6.326067,0.484133,0.052722,0.332533,0.858151,0.578987,0.593607,0.31444,0.170887],
		[0.276818,0.751878,0,5.076149,0.528768,1.695752,0.541712,1.437645,4.509238,0.191503,0.068427,2.145078,0.371004,0.089525,0.161787,4.008358,2.000679,0.045376,0.612025,0.083688],
		[0.395144,0.123954,5.076149,0,0.062556,0.523386,5.24387,0.844926,0.927114,0.01069,0.015076,0.282959,0.025548,0.017416,0.394456,1.240275,0.42586,0.02989,0.135107,0.037967],
		[2.489084,0.534551,0.528768,0.062556,0,0.084808,0.003499,0.569265,0.640543,0.320627,0.594007,0.013266,0.89368,1.105251,0.075382,2.784478,1.14348,0.670128,1.165532,1.959291],
		[0.969894,2.807908,1.695752,0.523386,0.084808,0,4.128591,0.267959,4.813505,0.072854,0.582457,3.234294,1.672569,0.035855,0.624294,1.223828,1.080136,0.236199,0.257336,0.210332],
		[1.038545,0.36397,0.541712,5.24387,0.003499,4.128591,0,0.348847,0.423881,0.044265,0.069673,1.807177,0.173735,0.018811,0.419409,0.611973,0.604545,0.077852,0.120037,0.245034],
		[2.06604,0.390192,1.437645,0.844926,0.569265,0.267959,0.348847,0,0.311484,0.008705,0.044261,0.296636,0.139538,0.089586,0.196961,1.73999,0.129836,0.268491,0.054679,0.076701],
		[0.358858,2.426601,4.509238,0.927114,0.640543,4.813505,0.423881,0.311484,0,0.108882,0.366317,0.697264,0.442472,0.682139,0.508851,0.990012,0.584262,0.597054,5.306834,0.119013],
		[0.14983,0.126991,0.191503,0.01069,0.320627,0.072854,0.044265,0.008705,0.108882,0,4.145067,0.159069,4.273607,1.112727,0.078281,0.064105,1.033739,0.11166,0.232523,10.649107],
		[0.395337,0.301848,0.068427,0.015076,0.594007,0.582457,0.069673,0.044261,0.366317,4.145067,0,0.1375,6.312358,2.592692,0.24906,0.182287,0.302936,0.619632,0.299648,1.702745],
		[0.536518,6.326067,2.145078,0.282959,0.013266,3.234294,1.807177,0.296636,0.697264,0.159069,0.1375,0,0.656604,0.023918,0.390322,0.748683,1.136863,0.049906,0.131932,0.185202],
		[1.124035,0.484133,0.371004,0.025548,0.89368,1.672569,0.173735,0.139538,0.442472,4.273607,6.312358,0.656604,0,1.798853,0.099849,0.34696,2.020366,0.696175,0.481306,1.898718],
		[0.253701,0.052722,0.089525,0.017416,1.105251,0.035855,0.018811,0.089586,0.682139,1.112727,2.592692,0.023918,1.798853,0,0.094464,0.361819,0.165001,2.457121,7.803902,0.654683],
		[1.177651,0.332533,0.161787,0.394456,0.075382,0.624294,0.419409,0.196961,0.508851,0.078281,0.24906,0.390322,0.099849,0.094464,0,1.338132,0.571468,0.095131,0.089613,0.296501],
		[4.727182,0.858151,4.008358,1.240275,2.784478,1.223828,0.611973,1.73999,0.990012,0.064105,0.182287,0.748683,0.34696,0.361819,1.338132,0,6.472279,0.248862,0.400547,0.098369],
		[2.139501,0.578987,2.000679,0.42586,1.14348,1.080136,0.604545,0.129836,0.584262,1.033739,0.302936,1.136863,2.020366,0.165001,0.571468,6.472279,0,0.140825,0.245841,2.188158],
		[0.180717,0.593607,0.045376,0.02989,0.670128,0.236199,0.077852,0.268491,0.597054,0.11166,0.619632,0.049906,0.696175,2.457121,0.095131,0.248862,0.140825,0,3.151815,0.18951],
		[0.218959,0.31444,0.612025,0.135107,1.165532,0.257336,0.120037,0.054679,5.306834,0.232523,0.299648,0.131932,0.481306,7.803902,0.089613,0.400547,0.245841,3.151815,0,0.249313],
		[2.54787,0.170887,0.083688,0.037967,1.959291,0.210332,0.245034,0.076701,0.119013,10.649107,1.702745,0.185202,1.898718,0.654683,0.296501,0.098369,2.188158,0.18951,0.249313,0]]
		qMatrix=[]
		totRate=0.0
		for i in range(20):
			qMatrix.append([])
			for j in range(20):
				if i==j:
					qMatrix[i].append(0.0)
				else:
					qMatrix[i].append(LGrates[i][j]*freqs[j])
					totRate+=qMatrix[i][j]*freqs[i]
		for i in range(20):
			for j in range(20):
				qMatrix[i][j]=qMatrix[i][j]/totRate
		for i in range(20):
			qMatrix[i][i]=-sum(qMatrix[i])
	else:
		print("Substitution model not supported!")
		exit()		
	print("Equillibrium frequences:")
	print(freqs)
	print("Q matrix:")
	print(qMatrix)
	print("Root frequencies:")
	print(pi)
	return [pi,freqs,qMatrix]




#probability of any mutation occurring on this branch 
def mutProbs(bl, modelType="JC", freqs=[0.25,0.25,0.25,0.25], kappa=1.0, rates=[0.2,0.4,0.2,0.2,0.4,0.2]):
	newM=np.ones((4, 4))
	if modelType=="JC":
		stay=0.25+0.75*math.exp(-bl*4/3.0)
		leave=0.25-0.25*math.exp(-bl*4/3.0)
		for i in range(4):
			for j in range(4):
				if i==j:
					newM[i][j]=stay
				else:
					newM[i][j]=leave
		return newM
	elif modelType=="HKY":
		model = dendropy.model.discrete.Hky85(kappa=kappa, base_freqs=freqs, state_alphabet=None, rng=None)
		newM=model.pmatrix(bl, rate=1.0)
		return newM
	elif modelType=="GTR":
		freqs2=freqs   #[0.25,0.25,0.25,0.25]
		qMatrix3=[[0.0,rates[0]*freqs2[1],rates[1]*freqs2[2],rates[2]*freqs2[3]], [rates[0]*freqs2[0],0.0,rates[3]*freqs2[2],rates[4]*freqs2[3]], [rates[1]*freqs2[0],rates[3]*freqs2[1],0.0,rates[5]*freqs2[3]], [rates[2]*freqs2[0],rates[4]*freqs2[1],rates[5]*freqs2[2],0.0]]
		qMatrix3[0][0]=-(rates[0]*freqs2[1]+rates[1]*freqs2[2]+rates[2]*freqs2[3])
		qMatrix3[1][1]=-(rates[0]*freqs2[0]+rates[3]*freqs2[2]+rates[4]*freqs2[3])
		qMatrix3[2][2]=-(rates[1]*freqs2[0]+rates[3]*freqs2[1]+rates[5]*freqs2[3])
		qMatrix3[3][3]=-(rates[2]*freqs2[0]+rates[4]*freqs2[1]+rates[5]*freqs2[2])
		totRate=-freqs2[0]*qMatrix3[0][0]-freqs2[1]*qMatrix3[1][1]-freqs2[2]*qMatrix3[2][2]-freqs2[3]*qMatrix3[3][3]
		for i in range(4):
			for j in range(4):
				newM[i][j]=qMatrix3[i][j]*bl/totRate
		return expm(newM)
	elif modelType=="LG":
		LGrates=[[0,0.425093,0.276818,0.395144,2.489084,0.969894,1.038545,2.06604,0.358858,0.14983,0.395337,0.536518,1.124035,0.253701,1.177651,4.727182,2.139501,0.180717,0.218959,2.54787],
		[0.425093,0,0.751878,0.123954,0.534551,2.807908,0.36397,0.390192,2.426601,0.126991,0.301848,6.326067,0.484133,0.052722,0.332533,0.858151,0.578987,0.593607,0.31444,0.170887],
		[0.276818,0.751878,0,5.076149,0.528768,1.695752,0.541712,1.437645,4.509238,0.191503,0.068427,2.145078,0.371004,0.089525,0.161787,4.008358,2.000679,0.045376,0.612025,0.083688],
		[0.395144,0.123954,5.076149,0,0.062556,0.523386,5.24387,0.844926,0.927114,0.01069,0.015076,0.282959,0.025548,0.017416,0.394456,1.240275,0.42586,0.02989,0.135107,0.037967],
		[2.489084,0.534551,0.528768,0.062556,0,0.084808,0.003499,0.569265,0.640543,0.320627,0.594007,0.013266,0.89368,1.105251,0.075382,2.784478,1.14348,0.670128,1.165532,1.959291],
		[0.969894,2.807908,1.695752,0.523386,0.084808,0,4.128591,0.267959,4.813505,0.072854,0.582457,3.234294,1.672569,0.035855,0.624294,1.223828,1.080136,0.236199,0.257336,0.210332],
		[1.038545,0.36397,0.541712,5.24387,0.003499,4.128591,0,0.348847,0.423881,0.044265,0.069673,1.807177,0.173735,0.018811,0.419409,0.611973,0.604545,0.077852,0.120037,0.245034],
		[2.06604,0.390192,1.437645,0.844926,0.569265,0.267959,0.348847,0,0.311484,0.008705,0.044261,0.296636,0.139538,0.089586,0.196961,1.73999,0.129836,0.268491,0.054679,0.076701],
		[0.358858,2.426601,4.509238,0.927114,0.640543,4.813505,0.423881,0.311484,0,0.108882,0.366317,0.697264,0.442472,0.682139,0.508851,0.990012,0.584262,0.597054,5.306834,0.119013],
		[0.14983,0.126991,0.191503,0.01069,0.320627,0.072854,0.044265,0.008705,0.108882,0,4.145067,0.159069,4.273607,1.112727,0.078281,0.064105,1.033739,0.11166,0.232523,10.649107],
		[0.395337,0.301848,0.068427,0.015076,0.594007,0.582457,0.069673,0.044261,0.366317,4.145067,0,0.1375,6.312358,2.592692,0.24906,0.182287,0.302936,0.619632,0.299648,1.702745],
		[0.536518,6.326067,2.145078,0.282959,0.013266,3.234294,1.807177,0.296636,0.697264,0.159069,0.1375,0,0.656604,0.023918,0.390322,0.748683,1.136863,0.049906,0.131932,0.185202],
		[1.124035,0.484133,0.371004,0.025548,0.89368,1.672569,0.173735,0.139538,0.442472,4.273607,6.312358,0.656604,0,1.798853,0.099849,0.34696,2.020366,0.696175,0.481306,1.898718],
		[0.253701,0.052722,0.089525,0.017416,1.105251,0.035855,0.018811,0.089586,0.682139,1.112727,2.592692,0.023918,1.798853,0,0.094464,0.361819,0.165001,2.457121,7.803902,0.654683],
		[1.177651,0.332533,0.161787,0.394456,0.075382,0.624294,0.419409,0.196961,0.508851,0.078281,0.24906,0.390322,0.099849,0.094464,0,1.338132,0.571468,0.095131,0.089613,0.296501],
		[4.727182,0.858151,4.008358,1.240275,2.784478,1.223828,0.611973,1.73999,0.990012,0.064105,0.182287,0.748683,0.34696,0.361819,1.338132,0,6.472279,0.248862,0.400547,0.098369],
		[2.139501,0.578987,2.000679,0.42586,1.14348,1.080136,0.604545,0.129836,0.584262,1.033739,0.302936,1.136863,2.020366,0.165001,0.571468,6.472279,0,0.140825,0.245841,2.188158],
		[0.180717,0.593607,0.045376,0.02989,0.670128,0.236199,0.077852,0.268491,0.597054,0.11166,0.619632,0.049906,0.696175,2.457121,0.095131,0.248862,0.140825,0,3.151815,0.18951],
		[0.218959,0.31444,0.612025,0.135107,1.165532,0.257336,0.120037,0.054679,5.306834,0.232523,0.299648,0.131932,0.481306,7.803902,0.089613,0.400547,0.245841,3.151815,0,0.249313],
		[2.54787,0.170887,0.083688,0.037967,1.959291,0.210332,0.245034,0.076701,0.119013,10.649107,1.702745,0.185202,1.898718,0.654683,0.296501,0.098369,2.188158,0.18951,0.249313,0]]
		qMatrix=[]
		totRate=0.0
		if len(freqs)!=20:
			freqs=LGfreqs
		newM=np.ones((20, 20))
		for i in range(20):
			qMatrix.append([])
			for j in range(20):
				if i==j:
					qMatrix[i].append(0.0)
				else:
					qMatrix[i].append(LGrates[i][j]*freqs[j])
					totRate+=qMatrix[i][j]*freqs[i]
		norm=bl/totRate
		for i in range(20):
			for j in range(20):
				newM[i][j]=qMatrix[i][j]*norm
		for i in range(20):
			newM[i][i]=-sum(newM[i])
		return expm(newM)
	else:
		print("Substitution model not supported!")
		exit()	






def getAllProbs3(t1,ri,rd,gi,gd, model, freqs, kappa, rates, ancestral,iModelType='cumIndels'):
	if t1<0.0 or ri<0.0 or rd<0.0 or gi<0.0 or gd<0.0 or gi>1.0 or gd>1.0:
		print("One of the pairHMM parameters is nonsense!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
		return [], [], []
	if iModelType=='cumIndels':
		PI1, PD1, PID1, gi1, gd1 = getIndelProbsSingle(t1,ri,rd,gi,gd)
		#print(PI1)
		#print(PD1)
		#print(PID1)
		#print(gi1)
		#print(gd1)
		transProbs = getTransProbsPair(PI1, PD1, PID1, gi1, gd1)
	else:
		transProbs = getTransProbsOthers(t1,ri,rd,gi,gd,iModelType=iModelType)
	#stateProbs2N=[1.0,0.0,0.0]
	if len(transProbs)<2:
		return [],[],[]
	P = np.array(transProbs)
	mc = markovChain(P)
	mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
	stateProbs=mc.pi
	probs1=mutProbs(t1, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
	#print(probs1)
	if transProbs[0][0]>0.999999:
		print("transprobs are nonsense (no indels)")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
		print("transprobs: ")
		print(transProbs)
		if iModelType=='cumIndels':
			print("indel probs: "+str(PI1)+" "+str(PD1)+" "+str(PID1)+" "+str(gi1)+" "+str(gd1))
		return [],[],[]
	for i in range(len(transProbs)):
		if sum(transProbs[i])<0.99 or sum(transProbs[i])>1.01:
			print("transprobs are nonsense!")
			print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
			print("transprobs: ")
			print(transProbs)
			return [],[],[]
	return transProbs, stateProbs, probs1





def getAllProbs(t1,t2,ri,rd,gi,gd, model, freqs, kappa, rates, ancestral,iModelType='cumIndels'):
	if t1<0.0 or ri<0.0 or rd<0.0 or gi<0.0 or gd<0.0 or gi>1.0 or gd>1.0:
		print("One of the pairHMM parameters is nonsense!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
		return [], [], [], []
	if iModelType!='cumIndels':
		print("other pairHMM's not yet implemented with 2 descendant sequences")
		exit()
	PI1, PD1, PID1, gi1, gd1 = getIndelProbsSingle(t1,ri,rd,gi,gd)
	PI2, PD2, PID2, gi2, gd2 = getIndelProbsSingle(t2,ri,rd,gi,gd)
	transProbs=getTransProbs(PI1, PI2, PD1, PD2, PID1, PID2, gi1, gi2, gd1, gd2)
	#stateProbs=[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
	P = np.array(transProbs)
	mc = markovChain(P)
	mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
	stateProbs=mc.pi

	#phylogenetic probabilities	
	probs1=mutProbs(t1, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
	probs2=mutProbs(t2, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
	if transProbs[0][0]>0.999999 :
		print("transprobs are nonsense (no indels)")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa))
		print("transprobs: ")
		print(transProbs)
		print("indel probs: "+str(PI1)+" "+str(PD1)+" "+str(PID1)+" "+str(gi1)+" "+str(gd1))
		return [],[],[],[]
	for i in range(len(transProbs)):
		if sum(transProbs[i])<0.99 or sum(transProbs[i])>1.01:
			print("transprobs are nonsense!")
			print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" "+str(kappa))
			print("transprobs: ")
			print(transProbs)
			return [],[],[],[]
	return transProbs, stateProbs, probs1, probs2



#def main():
if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser(description='Calculate pairHMM parameters.')
	parser.add_argument("-o","--oFile", help="file where to write parameters", default="")
	parser.add_argument("--t1", help="lenght of the branch (in expected number of substitutions) of the first sequence", type=float, default=0.1)
	parser.add_argument("--t2", help="lenght of the branch of the second sequence", type=float, default=0.1)

	parser.add_argument("--iModel", help="pairHMM model", default="cumIndels", choices=['cumIndels','TKF91','TKF92','RS07','PRANK'])
	parser.add_argument("--ri", help="instantaneous insertion rate", type=float, default=0.2)
	parser.add_argument("--rd", help="instantaneous deletion rate", type=float, default=0.2)
	parser.add_argument("--gi", help="instantaneous insertion extension probability", type=float, default=0.75)
	parser.add_argument("--gd", help="instantaneous deletion extension probability", type=float, default=0.75)

	parser.add_argument("--ancestral", help="The second sequence is ancestral, so run the Markov chain with only 3 states", action="store_true")

	parser.add_argument("-m","--model", help="nucleotide substitution model, can by HKY, JC, or GTR", default="HKY", choices=['JC', 'HKY', 'GTR', 'LG'])
	parser.add_argument("-f","--frequencies", help="equilibrium nucleotide frequencies (only used with HKY and GTR models)", nargs=4, type=float, default=[0.25,0.25,0.25,0.25])
	parser.add_argument("--LGfrequencies", help="equilibrium AA frequencies (only used with LG model)", nargs=20, type=float, default=LGfreqs)
	parser.add_argument("-k","--kappa", help="kappa rate for the HKY model (only used if the HKY model is selected)", type=float, default=3.0)
	parser.add_argument("-r","--rates", help="rates of the GTR moel (only used if the GTR model is selected). These are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C. The order of the rates is AC, AG, AT, CG, CT, GT.", nargs=6, type=float, default=[0.2,0.4,0.2,0.2,0.4,0.2])

	args = parser.parse_args()
	t1=args.t1
	t2=args.t2
	ri=args.ri
	rd=args.rd
	gi=args.gi
	gd=args.gd
	kappa=args.kappa
	rates=args.rates
	freqs=args.frequencies
	model=args.model
	ancestral=args.ancestral
	iModelType=args.iModel
	transBool=True
	if model=='LG':
		freqs=args.LGfrequencies
	if t1<0.0 or ri<0.0 or rd<0.0 or gi<0.0 or gd<0.0 or gi>1.0 or gd>1.0:
		print("One of the pairHMM parameters is nonsense!")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi))
		#exit()
		#transBool=False
		if args.oFile!="":
			file=open(args.oFile,"w")
			file.write("Error")
			file.write("\n")
			file.close()
		exit()
		
	if args.ancestral:
		states=["M","I","D"]
		nS=len(states)
		if iModelType=='cumIndels':
			PI1, PD1, PID1, gi1, gd1 = getIndelProbsSingle(t1,ri,rd,gi,gd)
			transProbs = getTransProbsPair(PI1, PD1, PID1, gi1, gd1)
		else:
			transProbs = getTransProbsOthers(t1,ri,rd,gi,gd,iModelType=iModelType)
		#PI1, PD1, PID1, gi1, gd1 = getIndelProbsSingle(t1,ri,rd,gi,gd)
		#transProbs = getTransProbsPair(PI1, PD1, PID1, gi1, gd1)

		#stateProbs2N=[1.0,0.0,0.0]
		P = np.array(transProbs)
		mc = markovChain(P)
		mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
		stateProbs=mc.pi
		
		probs1=mutProbs(t1, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
		
	else:
		states=["MM","DM","MD","I-M","-MI","I-D","-DI","-II"]
		nS=len(states)
		if iModelType!='cumIndels':
			print("other pairHMM's not yet implemented with 2 descendant sequences")
			exit()
		#Indel probabilities
		PI1, PD1, PID1, gi1, gd1 = getIndelProbsSingle(t1,ri,rd,gi,gd)
		PI2, PD2, PID2, gi2, gd2 = getIndelProbsSingle(t2,ri,rd,gi,gd)
		transProbs=getTransProbs(PI1, PI2, PD1, PD2, PID1, PID2, gi1, gi2, gd1, gd2)
		
		#print("transProbs")
		#print(transProbs)
		
		#stateProbs=[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
		P = np.array(transProbs)
		mc = markovChain(P)
		mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
		stateProbs=mc.pi
	
		#phylogenetic probabilities	
		probs1=mutProbs(t1, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
		probs2=mutProbs(t2, modelType=model, freqs=freqs, kappa=kappa, rates=rates)
	
	for i in range(len(transProbs)):
		if sum(transProbs[i])<0.99 or sum(transProbs[i])>1.01:
			transBool=False
			break
			#exit()
	if (not transBool) or transProbs[0][0]>0.999999:
		print("transprobs are nonsense")
		print("parameters: "+str(t1)+" "+str(ri)+" "+str(gi)+" ")
		print("transprobs: ")
		print(transProbs)
		print("indel probs: "+str(PI1)+" "+str(PD1)+" "+str(PID1)+" "+str(gi1)+" "+str(gd1))
		if args.oFile!="":
			file=open(args.oFile,"w")
			file.write("Error")
			file.write("\n")
			file.close()
		exit()
	
	if args.oFile!="":
		file=open(args.oFile,"w")
		file.write("transProbs: ")
		for i in range(len(transProbs)):
			for j in range(len(transProbs[i])):
				file.write(str(transProbs[i][j])+" ")
		file.write("\n")
		file.write("stateProbs: ")
		for i in range(len(stateProbs)):
			file.write(str(stateProbs[i])+" ")
		file.write("\n")
		file.write("phyloProbs1: ")
		for i in range(len(probs1)):
			for j in range(len(probs1[i])):
				file.write(str(probs1[i][j])+" ")
		file.write("\n")
		if not args.ancestral:
			#print(args.ancestral)
			file.write("phyloProbs2: ")
			for i in range(len(probs2)):
				for j in range(len(probs2[i])):
					file.write(str(probs2[i][j])+" ")
		file.write("\n")
		file.close()
			

	exit()








