
##script used either to simulate insert lengths or to simulate and infer pairwise alignments and to infer model parameters.

import sys
import math
import time
import os
import os.path
import argparse
import numpy as np
import scipy.stats as stats

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D 

import pairwise_alignment_new
from scipy import integrate as integ
from mpl_toolkits.mplot3d import Axes3D

from discreteMarkovChain import markovChain


pypath="pypy"

parser = argparse.ArgumentParser(description='Run simulations of alignments, parameter inference, and pairHMM parameters.')
parser.add_argument("--rate", help="indel rates", type=float, default=0.1)
parser.add_argument('--ML', dest='ML', action='store_const', const=False, default=True, help='should I perform inference of parameters and alignments from simulated data?')
parser.add_argument('--runSims', dest='runSims', action='store_const', const=False, default=True, help='should I simulate data?')
parser.add_argument('--runAl', dest='runAl', action='store_const', const=False, default=True, help='should I run alignments?')
#parser.add_argument('--fixAl', dest='fixAl', action='store_const', const=False, default=True, help='should I consider fixed alignment scenarios?')
parser.add_argument('--doPlots', dest='doPlots', action='store_const', const=False, default=True, help='should I consider fixed alignment scenarios?')
parser.add_argument('--tree', dest='tree', action='store_const', const=False, default=True, help='should I consider many species scenario?')
parser.add_argument('--inferParams', dest='inferParams', action='store_const', const=False, default=True, help='should I infer model parameters with ML?')
parser.add_argument("--startTheta", help="initial theta: from which set of values to start parameter inference - can be either 0 (default), 1 or 2", type=int, default=0)
parser.add_argument("--InModel", help="model to be used for parameter and alignment inference. There are 6 models, plus fixed alignment with cumIndel; the latter is 0 (default), the others are from 1 to 6.", type=int, default=0)

args = parser.parse_args()

ML=args.ML
folderSimu="needle_simu"
folderPlots="new_plots/"
embossPath="/EMBOSS-6.6.0/emboss/needle"
indeliblePath="/INDELibleV1.03/src/indelible"
homePath="/cumIndel/"
runSims=args.runSims
rates=[args.rate] #rates=[0.1, 0.5]
runAl=args.runAl
#runSlow=0
#fixAl=args.fixAl
InModel=args.InModel
doPlots=args.doPlots
tree=args.tree
inferParams=args.inferParams
startTheta=args.startTheta
onlyNewPrank=0
#onlyNewTKF=1
testBand=0
testBand2=0
testBand3=0



#make a graph plot of some example HMM states
plotNetwork=0
if plotNetwork:
	import igraph
	#t1,t2,ri,rd,gi,gd
	#P1I,P2I,P1D,P2D,g1I,g2I,g1D,g2D
	
	indProb = pairwise_alignment.getIndelProbsSingle(0.3,0.5,0.5,0.75,0.75)
	tProbs = pairwise_alignment.getTransProbsPair(indProb[0], indProb[1], indProb[2], indProb[3], indProb[4])
	states=["M","I","D"]
	print indProb
	print tProbs
	for i in range(3):
		print sum(tProbs[i])
	P = np.array(tProbs)
	mc = markovChain(P)
	mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
	eqProbs=mc.pi
	print eqProbs
	print sum(eqProbs)
	
	total_nodes = 3
	g = igraph.Graph(directed=True)
	g.add_vertices(total_nodes)
	g.vs["Name"]=states
	for i in range(total_nodes):
		g.vs[i]["Freq"]=eqProbs[i]*200
		
	p=[]
	eds=[]
	#graph = np.zeros((total_nodes, total_nodes))
	for i in range(total_nodes):
		for j in range(total_nodes):
			if i!=j:
				if tProbs[i][j]>0.000000001:
					eds.append((i,j))				
	g.add_edges(eds)
	for i in range(total_nodes):
		for j in range(total_nodes):
			if i!=j:
				if tProbs[i][j]>0.000000001:
					ind=g.get_eid(i,j)
					g.es[ind]["Prob"]=tProbs[i][j]*50
	layout = g.layout("circle")
	visual_style = {}
	visual_style["vertex_size"] = g.vs["Freq"]
	#>>> visual_style["vertex_color"] = [color_dict[gender] for gender in g.vs["gender"]]
	visual_style["vertex_label"] = g.vs["Name"]
	visual_style["vertex_label_size"] = 40
	visual_style["edge_width"] = g.es["Prob"]
	visual_style["edge_color"] = "blue"
	visual_style["layout"] = layout
	visual_style["bbox"] = (400, 400)
	visual_style["margin"] = 50
	out = igraph.plot(g, **visual_style)
	out.save("graph_plot_3")
		
		
	#exit()
	
	
	indProb = pairwise_alignment.getIndelProbs(5.5,5.5,0.5,0.5,0.75,0.75)
	tProbs = pairwise_alignment.getTransProbs(indProb[0], indProb[1], indProb[2], indProb[3], indProb[4], indProb[5], indProb[6], indProb[7])
	states=["(M,M)","(D,M)","(M,D)","(I,-M)","(-M,I)","(I,-D)","(-D,I)"]
	print indProb
	print tProbs
	for i in range(7):
		print sum(tProbs[i])
	P = np.array(tProbs)
	mc = markovChain(P)
	mc.computePi('linear') #We can also use 'power', 'krylov' or 'eigen'
	eqProbs=mc.pi
	print eqProbs
	print sum(eqProbs)

	total_nodes = 7
	g = igraph.Graph(directed=True)
	g.add_vertices(total_nodes)
	g.vs["Name"]=states
	for i in range(total_nodes):
		g.vs[i]["Freq"]=eqProbs[i]*100
		
		
	exit()

	p=[]
	eds=[]
	#graph = np.zeros((total_nodes, total_nodes))
	for i in range(total_nodes):
		for j in range(total_nodes):
			if i!=j:
				if tProbs[i][j]>0.000000001:
					eds.append((i,j))				
	g.add_edges(eds)
	for i in range(total_nodes):
		for j in range(total_nodes):
			if i!=j:
				if tProbs[i][j]>0.000000001:
					ind=g.get_eid(i,j)
					g.es[ind]["Prob"]=tProbs[i][j]*20
	layout = g.layout("circle")
	visual_style = {}
	visual_style["vertex_size"] = g.vs["Freq"]
	#>>> visual_style["vertex_color"] = [color_dict[gender] for gender in g.vs["gender"]]
	visual_style["vertex_label"] = g.vs["Name"]
	visual_style["vertex_label_size"] = 40
	visual_style["edge_width"] = g.es["Prob"]
	visual_style["edge_color"] = "blue"
	visual_style["layout"] = layout
	visual_style["bbox"] = (900, 900)
	visual_style["margin"] = 50
	out = igraph.plot(g, **visual_style)
	out.save("graph_plot")


	exit()


















#compare an inferred alignment (seqs) with the simulated alignment (als)
def compareAlignments(seq1, seq2, al1, al2):
	def buildHomo(al1, al2):
		homoT=[]
		ind1=0
		ind2=0
		for i in range(len(al1)):
			if al1[i]!="-":
				if al2[i]!="-":
					homoT.append([ind1,ind2])
					ind1+=1
					ind2+=1
				else:
					ind1+=1
			else:
				if al2[i]!="-":
					ind2+=1
					
		return homoT
	homoT=buildHomo(al1, al2)
	homo=buildHomo(seq1, seq2)
	found=0
	missed=0
	extra=0
	ind1=0
	ind2=0
	while ind1<len(homoT) or ind2<len(homo):
		if ind1<len(homoT) and (ind2>=len(homo) or homoT[ind1][0]<homo[ind2][0]):
			missed+=1
			ind1+=1
		elif ind2<len(homo) and (ind1>=len(homoT) or homoT[ind1][0]>homo[ind2][0]):
			extra+=1
			ind2+=1
		else:
			if homoT[ind1][1]==homo[ind2][1]:
				found+=1
				ind1+=1
				ind2+=1
			else:
				missed+=1
				extra+=1
				ind1+=1
				ind2+=1
				
	return [found, missed, extra]






















LGfreqs=[0.079066, 0.055941, 0.041977, 0.053052, 0.012937, 0.040767, 0.071586, 0.057337, 0.022355, 0.062157, 0.099081, 0.064600, 0.022951, 0.042302, 0.044040, 0.061197, 0.053287, 0.012066, 0.034155, 0.069147 ]


#Test how long it takes to run EMBOSS for pairwise alignment compared to my methods (at varying gene lengths, and maybe 3 branch lengths?)
needleTime=0
runSimsTime=0
doPlotsTime=1
runBasics=1
if needleTime:
	BLs=[0.05,0.2]
	#BLs=[0.02]
	rates=[0.05,0.2]
	#rates=[0.2]
	nReps=10
	#seqLens=[50000]
	limit=5000
	pypyLim1=0
	pypyLim2=0
	seqLens=[200,500,1000,2000,5000,10000,20000]#200,500,1000,2000,5000,10000,20000   #100,200,500,1000,2000,5000,10000,20000,50000
	for seqLen in seqLens:
		for BL in BLs:
			for rate in rates:
				if runSimsTime:
					#if seqLen<=1000 :
						#bSMALL=True
					#	fileSMALL=open("needle_simu/SMALL_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
					if runBasics: # and seqLen<=20000:
						#bEMBOSS=True
						fileEMBOSS=open("/needle_simu/EMBOSS_time_endweight_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
					if seqLen<=limit :
						fileSMALL3=open("/needle_simu/SMALL3_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
						fileFW3=open("/needle_simu/FW3_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
					#if seqLen<=5000:
					#	fileSMALLfast=open("/needle_simu/SMALLfast_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
					if runBasics:
						fileSMALL3fast=open("/needle_simu/SMALL3fast_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
						fileFW3fast=open("/needle_simu/FW3fast_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt","w")
					#fileEMBOSS2=open("/needle_simu/EMBOSS_results_"+str(BL)+"_"+str(rate)+".txt","w")
					for rep in range(nReps):
						print("\n\n\n"+str(BL)+" "+str(rate)+" "+str(seqLen)+" "+str(rep)+" ")
						file=open("/needle_simu/control.txt","w")
						file.write("[TYPE] AMINOACID 1\n")
						file.write("[MODEL]    modelname\n")
						#file.write("  [submodel]     JC  \n")
						file.write("  [submodel]  LG\n")
						file.write("  [statefreq] ")
						for i in range(20):
							file.write(str(LGfreqs[i])+" ")
						file.write("\n")
						file.write("  [indelmodel]   NB  0.75 1  \n")
						file.write("  [insertrate]   "+str(rate)+"  \n")
						file.write("  [deleterate]   "+str(rate)+" \n")
						file.write("[TREE] treename  (A:0.0,B:"+str(BL)+"); \n")
						file.write("[PARTITIONS] partitionname   \n")
						file.write("  [treename modelname "+str(seqLen)+"]  \n")
						file.write("[EVOLVE] partitionname 1 indelibleOut_times_AA_"+str(BL)+"_"+str(rate)+"_"+str(rep)+"_"+str(seqLen)+" \n")
						file.close()

						os.system("cd /needle_simu/; "+"/Users/demaio/Desktop/INDELibleV1.03/bin/indelible  > /dev/null")

						file=open("/needle_simu/indelibleOut_times_AA_"+str(BL)+"_"+str(rate)+"_"+str(rep)+"_"+str(seqLen)+".fas")
						line=file.readline()
						line=file.readline()
						seq1=line.replace("\n","") #sequences
						line=file.readline()
						line=file.readline()
						seq2=line.replace("\n","")
						file.close()
					
						seq1="".join(seq1.split())
						seq2="".join(seq2.split())
						
						
						
						if seqLen<=limit :
							start=time.time()
							if seqLen<=pypyLim1:
								os.system("python pairwise_alignment_new.py --ancestral --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --fixPars")
							else:
								os.system(pypath+" pairwise_alignment_new.py --pypy --ancestral --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --fixPars")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedle3(s1=seq1, s2=seq2, t1=2*BL, iModelType="approx", eModelType="approx", ri=rate, rd=rate, gi=0.75, gd=0.75, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=3.0) #, rates=args.rates
							elapsedTime = time.time() - start
							print "time SMALL NW 3 states: "+str(elapsedTime)+"\n"
							fileSMALL3.write(str(elapsedTime)+"\n")
							
							start=time.time()
							if seqLen<=pypyLim1:
								os.system("python pairwise_alignment_new.py --ancestral --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --forwardNW")
							else:
								os.system(pypath+" pairwise_alignment_new.py --pypy --ancestral --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --forwardNW")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedle3(s1=seq1, s2=seq2, t1=2*BL, iModelType="approx", eModelType="approx", ri=rate, rd=rate, gi=0.75, gd=0.75, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=3.0) #, rates=args.rates
							elapsedTime = time.time() - start
							print "time SMALL forward 3 states: "+str(elapsedTime)+"\n"
							fileFW3.write(str(elapsedTime)+"\n")
							#print LK
						
						if runBasics:
							start=time.time()
							if seqLen<=pypyLim2:
								os.system("python pairwise_alignment_new.py --ancestral --fast -e -15 --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --fixPars")
							else:
								os.system(pypath+" pairwise_alignment_new.py --pypy --ancestral --fast -e -15 --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --fixPars")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedle3Fast(s1=seq1, s2=seq2, t1=2*BL, epsilon=-15, iModelType="approx", eModelType="approx", ri=rate, rd=rate, gi=0.75, gd=0.75, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=3.0) #, rates=args.rates
							elapsedTime = time.time() - start
							print "time SMALL NW 3 states fast: "+str(elapsedTime)+"\n"
							fileSMALL3fast.write(str(elapsedTime)+"\n")
							#print LK
							
							start=time.time()
							if seqLen<=pypyLim2:
								os.system("python pairwise_alignment_new.py --ancestral --fast -e -15 --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --forwardNW")
							else:
								os.system(pypath+" pairwise_alignment_new.py --pypy --ancestral --fast -e -15 --s1 "+seq1+" --s2 "+seq2+" --t1 "+str(BL)+" --t2 0.0 --ri "+str(rate)+" --rd "+str(rate)+" --gi 0.75 --gd 0.75 --model LG --forwardNW")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedle3(s1=seq1, s2=seq2, t1=2*BL, iModelType="approx", eModelType="approx", ri=rate, rd=rate, gi=0.75, gd=0.75, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=3.0) #, rates=args.rates
							elapsedTime = time.time() - start
							print "time SMALL forward 3 states fast: "+str(elapsedTime)+"\n"
							fileFW3fast.write(str(elapsedTime)+"\n")
							
							if True: # seqLen<=20000 or (BL==0.02 and rate==0.2):
							#if seqLen<=20000 or (BL==0.02 and rate==0.2):
								file=open("/needle_simu/reference.fa","w")
								file.write(">A\n")
								file.write(seq1+"\n")
								file.close()
								file=open("/needle_simu/query.fa","w")
								file.write(">B\n")
								file.write(seq2+"\n")
								file.close()

								start=time.time()
								#CHANGE THIS line to make AA alignment
								os.system("cd /needle_simu/; /Applications/EMBOSS-6.6.0/emboss/needle -gapopen 10.0 -gapextend 0.5 -asequence /needle_simu/reference.fa -bsequence /needle_simu/query.fa -outfile /needle_simu/EMBOSS_out.txt -sprotein1 true -sprotein2 true -aformat3 fasta")
								elapsedTime = time.time() - start
								print "time EMBOSS: "+str(elapsedTime)+"\n"
								fileEMBOSS.write(str(elapsedTime)+"\n")
					# if seqLen<=500 or (BL==0.02 and rate==0.2 and seqLen==1000):
# 						fileSMALL.close()
# 					if seqLen<=2000 or (BL==0.02 and rate==0.2 and (seqLen==5000 or seqLen==10000)):
# 						fileSMALLfast.close()
					if seqLen<=limit: # or (BL==0.02 and rate==0.2 and seqLen==2000):
						fileSMALL3.close()
						fileFW3.close()
					if runBasics:
						fileSMALL3fast.close()
						fileFW3fast.close()
						#if runBasics and (seqLen<=20000 or (BL==0.02 and rate==0.2)):
						fileEMBOSS.close()
			
	if doPlotsTime:		
		times=[]
		xAxis=[]
		#ylims=[[100,100],[150,300],[150,300]]
		ylims=[[450,120],[150,550],[150,550]]
		for b in range(len(BLs)):
			BL=BLs[b]
			times.append([])
			#props.append([])
			xAxis.append([])
			for r in range(len(rates)):
				rate=rates[r]
				times[b].append([[],[],[],[],[]])
				xAxis[b].append([[],[],[],[],[]])
				yl=ylims[b][r]
				for s in range(len(seqLens)):
					seqLen=seqLens[s]
					#if seqLen<=500 or (r==0 and b==0 and seqLen==1000):
					#	fileSMALL=open("/needle_simu/SMALL_time_results_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					#if seqLen<=2000 or (r==0 and b==0 and (seqLen==5000 or seqLen==10000)):
					#	fileSMALLfast=open("/needle_simu/SMALLfast_time_results_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					if seqLen<=limit: # or (r==0 and b==0 and seqLen==2000):
						fileSMALL3=open("/needle_simu/SMALL3_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
						fileFW3=open("/needle_simu/FW3_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					fileSMALL3fast=open("/needle_simu/SMALL3fast_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					fileFW3fast=open("/needle_simu/FW3fast_time_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					if True:
						#if seqLen<=20000 or (r==0 and b==0):
						fileEMBOSS=open("/needle_simu/EMBOSS_time_endweight_results_AA_"+str(BL)+"_"+str(rate)+"_"+str(seqLen)+".txt")
					#fileEMBOSS2=open("/needle_simu/EMBOSS_results_"+str(BL)+"_"+str(rate)+".txt")
					
					if seqLen<=limit:
						files=[fileEMBOSS,fileSMALL3fast,fileFW3fast,fileSMALL3,fileFW3]
					else:
						files=[fileEMBOSS,fileSMALL3fast,fileFW3fast]
						
					for i in range(len(files)): #method
						times[b][r][i].append([])
						for j in range(nReps):
							line=files[i].readline()
							linelist=line.split()
							#print "  b:"+str(b)+"  r:"+str(r)+"  i:"+str(i)+"  s:"+str(s)
							#print times[b][r][i]
							times[b][r][i][s].append(float(linelist[0]))
						files[i].close()
			
			
				dataEMB=times[b][r][0]
				data3Fast=times[b][r][1]
				dataFWFast=times[b][r][2]
				data3=times[b][r][3]
				dataFW=times[b][r][4]
				ticks=seqLens

				def set_box_color(bp, color):
					plt.setp(bp['boxes'], color=color)
					plt.setp(bp['whiskers'], color=color)
					plt.setp(bp['caps'], color=color)
					plt.setp(bp['medians'], color=color)

				plt.figure()
				space=14
				#print data
				bplots=[dataEMB,data3Fast,dataFWFast,data3,dataFW]
				colors=['red','blue','green','orange','purple']
				bp1 = plt.boxplot(dataEMB, positions=np.array(xrange(len(dataEMB)))*space-2.0, sym='', widths=0.8)
				bp2 = plt.boxplot(data3Fast, positions=np.array(xrange(len(data3Fast)))*space-1.0, sym='', widths=0.8)
				bp3 = plt.boxplot(dataFWFast, positions=np.array(xrange(len(dataFWFast)))*space, sym='', widths=0.8)
				bp4 = plt.boxplot(data3, positions=np.array(xrange(len(data3)))*space+1.0, sym='', widths=0.8)
				bp5 = plt.boxplot(dataFW, positions=np.array(xrange(len(dataFW)))*space+2.0, sym='', widths=0.8)
				set_box_color(bp1, 'red') # colors are from http://colorbrewer2.org/
				set_box_color(bp2, 'blue')
				set_box_color(bp3, 'green')
				set_box_color(bp4, 'orange')
				set_box_color(bp5, 'purple')
				
				for ib in range(len(bplots)):
					medians = [np.median(d) for d in bplots[ib]]
					plt.plot(np.array(xrange(len(bplots[ib])))*space+ib-2.0, medians, c=colors[ib], lw=0.5)
				# simply plot the data as usual
				#plt.plot(np.array(xrange(len(bplots[ib])))*space+ib-2.0, mins, c="r", lw=2)
				#plt.plot([1,2,3], maxes, c="g", lw=2)

				plt.plot([], c='red', label='EMBOSS', lw=0.5)
				plt.plot([], c='blue', label='Alignment banding', lw=0.5)
				plt.plot([], c='green', label='Forward banding', lw=0.5)
				plt.plot([], c='orange', label='Alignment', lw=0.5)
				plt.plot([], c='purple', label='Forward', lw=0.5)
				plt.legend()

				plt.xticks(xrange(0, len(ticks) * space, space), ticks)
				plt.xlim(-4, len(ticks)*space-8)
				#plt.ylim(0, yl)
				plt.tight_layout()
				plt.yscale("log")
				plt.savefig("runningTimes_BL"+str(BL)+"_rate"+str(rate)+"_new2.pdf")
					
	exit()





























#Test how well it works to infer 1) parameters, followed by 2) alignments using the inferred parameters.
done=0
onlyNewPrank=0
#onlyNewTKF=1
testBand=0
testBand2=0
testBand3=0
if ML:
	#BLs=[0.3]
	BLs=[ 0.025, 0.05, 0.1, 0.2, 0.3] #0.025, 0.05, 0.1, 0.2, 0.3
	#rates=[0.1, 0.5] # 0.1, 0.5
	brText=""
	if tree:
		BLs=[0.5] #0.025, 0.05, 0.1, 0.2, 0.3
		#rates=[0.1, 0.5] # 0.1, 0.5
		import random
		brText="_branches"
	folderSimu=folderSimu+brText+str(rates[0])+"/"
	gapL=0.75
	nReps=1
	nAl=150 #20
	seqLen=1000 #100
	kappa=3.0
	#due to a typo the BL were inferred halvened: multiply the estimates by two
	factorBL=2.0
	if True:
		for BL in BLs:
			
			for rate in rates:
				
				if tree:
					#print BL
					brlens=[]
					brlensStr=[]
					if runSims:
						for a in range(nAl):
								ran=random.random()
								#print(ran)
								brlens.append(ran*BL)
								brlensStr.append(str(brlens[a]))
						brText2=" ".join(brlensStr)
					else:
						fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_simulated.txt")
						line=fileO.readline()
						line=fileO.readline()
						linelist=line.split()
						brlensStr=linelist[3:]
						for a in range(nAl):
							brlens.append(float(brlensStr[a]))
						fileO.close()
				else:
					brlens=[]
					for a in range(nAl):
						brlens.append(BL)
				
				gapP=gapL
				if runSims:
					fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_simulated.txt","w")
					fileO.write("Simulated: "+str(BL)+" "+str(rate)+" "+str(gapP)+"\n")
					if tree:
						fileO.write("Simulated branch lengths: "+brText2+"\n")
						print("Simulated branch lengths: "+brText2)
					fileO.close()
				print("Simulated: "+str(BL)+" "+str(rate)+" "+str(gapP))
				Ls11=[]
				Ls12=[]
				LK1=0.0
				Ls21=[]
				Ls22=[]
				LK2=0.0
				#if 1-tree:
				#		transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(2*BL,0.0,rate,rate,gapP,gapP, model="LG", freqs=LGfreqs, ancestral=True, pypy=False)
				for al in range(nAl):
					#if tree:
					if runSims:
						transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(2*brlens[al],0.0,rate,rate,gapP,gapP, model="LG", freqs=LGfreqs, ancestral=True, pypy=False)
						file=open(folderSimu+"control.txt","w")
						file.write("[TYPE] AMINOACID 1\n")
						file.write("[MODEL]    modelname\n")
						#file.write("  [submodel]     JC  \n")
						file.write("  [submodel]  LG \n")
						file.write("  [statefreq] ")
						for i in range(20):
							file.write(str(LGfreqs[i])+" ")
						file.write("\n")
						file.write("  [indelmodel]   NB  "+str(gapP)+" 1  \n")
						file.write("  [insertrate]   "+str(rate)+"  \n")
						file.write("  [deleterate]   "+str(rate)+" \n")
						file.write("[TREE] treename  (A:"+str(brlens[al])+",B:"+str(brlens[al])+"); \n")
						file.write("[PARTITIONS] partitionname   \n")
						file.write("  [treename modelname "+str(seqLen)+"]  \n")
						file.write("[EVOLVE] partitionname 1 indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+" \n")
						file.close()

						os.system("cd "+folderSimu+"; "+indeliblePath+"  > /dev/null")

					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+".fas")
					line=file.readline()
					line=file.readline()
					seq1=line.replace("\n","") #sequences
					line=file.readline()
					line=file.readline()
					seq2=line.replace("\n","")
					file.close()
					
					#print seq1
					#print seq2
					seq1="".join(seq1.split())
					seq2="".join(seq2.split())
					#Ls11.append(seq1[0:50])
					#Ls12.append(seq2[0:50])
					Ls21.append(seq1)
					Ls22.append(seq2)
					
					#LK1+=pairwise_alignment.forwardNWfast(s1=seq1[0:50], s2=seq2[0:50], t1=BL, t2=BL, ri=rate, rd=rate, gi=gapP, gd=gapP, model="HKY", freqs=[0.3, 0.2, 0.3, 0.2], pi=[0.3, 0.2, 0.3, 0.2], kappa=kappa, iModelType="approx", eModelType="approx")[0]
					#LK2+=pairwise_alignment_new.forwardNW3fast(s1=seq1, s2=seq2, t1=2*BL, ri=rate, rd=rate, gi=gapP, gd=gapP, model="HKY", freqs=[0.3, 0.2, 0.3, 0.2], pi=[0.3, 0.2, 0.3, 0.2], kappa=kappa, iModelType="approx", eModelType="approx")[0]
					if runSims:
						LK2+=pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-15, alphabet="AA")[0]
				#print "likelihood at truth (full model, shorter sequences): "+str(LK1)
				if runSims:
					print "likelihood at truth 3 states: "+str(LK2)

				if InModel==0 and runSims:
						for al in range(nAl):
							seq1=Ls21[al]
							seq2=Ls22[al]
							#if onlyNewPrank==0:
							file=open(folderSimu+"reference"+brText+".fa","w")
							file.write(">A\n")
							file.write(seq1+"\n")
							file.close()
							file=open(folderSimu+"query"+brText+".fa","w")
							file.write(">B\n")
							file.write(seq2+"\n")
							file.close()
							os.system("cd "+folderSimu+"; "+embossPath+" -gapopen 10.0 -gapextend 0.5 -asequence "+folderSimu+"reference"+brText+".fa -bsequence "+folderSimu+"query"+brText+".fa -outfile "+folderSimu+"EMBOSS_out"+brText+".txt -sprotein1 true -sprotein2 true -aformat3 fasta  &> /dev/null")
						print("Found EMBOSS fixed alignments\n\n")
				if inferParams:
					if tree:
						initTheta=[[0.2,0.1,0.5],[0.1,0.4,0.5],[0.1,0.2,0.9]]
					else:
						initTheta=[[0.2,0.1,0.5],[0.1,0.4,0.5],[0.1,0.1,0.9]]
					if InModel==0:
						#FIXED AL
						fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_fixedAl_"+str(startTheta)+".txt","w")
						fix1=[]
						fix2=[]
						for al in range(nAl):
							file=open(folderSimu+"EMBOSS_out"+brText+".txt")
							line=file.readline()
							line=file.readline()
							EMBOSS1=""
							while line!=">B\n":
								EMBOSS1+=line.replace("\n","")
								line=file.readline()
							line=file.readline()
							EMBOSS2=""
							while len(line.split())>0:
								EMBOSS2+=line.replace("\n","")
								line=file.readline()
							file.close()
							fix1.append(EMBOSS1)
							fix2.append(EMBOSS2)
						maxLK=float("-inf")
						bestT=[]
						#if onlyNewPrank==0:
						#for i in range(len(initTheta)):
						theta, LK = pairwise_alignment_new.fixedAlOptimize(fix1, fix2, t1=initTheta[startTheta][0], fast=True, ancestral=True, iModelType='cumIndels', ri=initTheta[startTheta][1], gi=initTheta[startTheta][2], model="LG", optMethod="Nelder-Mead", pypy=False, tree=tree==1, brlens=brlens)
						#if LK>maxLK:
						maxLK=LK
						bestT=list(theta)
						#theta, LK = pairwise_alignment.fixedAlOptimize(s1=fix1, s2=fix2, t1=0.05, ri=0.05, gi=0.7, epsilon=-15, fast=True, ancestral=True, model="HKY", kappa=2.0, optMethod="Nelder-Mead", iModelType="approx", eModelType="approx")
						#fixedAlOptimize(s1, s2, t1=0.2, epsilon=-9.0, fast=True, ancestral=True, iModelType='cumIndels', ri=0.1, gi=0.5, model="HKY", kappa=2.0, rates=[0.2,0.4,0.2,0.2,0.4,0.2], optMethod="Nelder-Mead"):
						if tree:
							fileO.write("fixed alignment optimizer cumulative indel\n"+str(maxLK)+"\n"+str(bestT[0])+" "+str(bestT[1])+"\n")
						else:
							fileO.write("fixed alignment optimizer cumulative indel\n"+str(maxLK)+"\n"+str(bestT[0])+" "+str(bestT[1])+" "+str(bestT[2])+"\n")
						print("fixed alignment optimizer cumulative indel: "+str(maxLK))
						print(bestT)
						print("\n\n")
						fileO.close()

					#FORWARD 3 states
					iModels=['TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0'] #'TKF91','cumIndels','TKF92','RS07','PRANK'
					if InModel>0:
							mod=iModels[InModel-1]
							fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+mod+"_"+str(startTheta)+".txt","w")	

							maxLK=float("-inf")
							bestT=[]
							#for i in range(len(initTheta)):
							theta, LK = pairwise_alignment_new.forwardOptimize(Ls21, Ls22, initTheta[startTheta][0], epsilon=-15.0, fast=True, ancestral=True, iModelType=mod, ri=initTheta[startTheta][1], gi=initTheta[startTheta][2], model="LG", optMethod="Nelder-Mead", pypy=True, tree=tree==1, brlens=brlens, folderSimu=folderSimu, baseFolder=homePath, pypath="pypy")
								#if LK>maxLK:
							maxLK=LK
							bestT=list(theta)
							
							fileO.write("forward optimizer "+mod+"\n"+str(maxLK)+"\n")
							for l in bestT:
								fileO.write(str(l)+" ")
							fileO.write("\n")
							print("forward optimizer "+mod+": "+str(maxLK))
							print(bestT)
							print("\n\n")
							fileO.close()
					
			
	# test that the size of the band is sufficiently large
	if testBand:
		for BL in BLs:
			if tree:
				#print BL
				brlens=[]
				brlensStr=[]
			else:
				brlens=[]
				for a in range(nAl):
					brlens.append(BL)
			
			for rate in rates:
				gapP=gapL
				#if onlyNewPrank==1:
					#fileO=open("/needle_simu/ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_morePRANK.txt","w")
				fileO2=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_morePRANK.txt")
				line=fileO2.readline()
				#fileO.write(line)
				if tree:
					line=fileO2.readline()
					#fileO.write(line)
					linelist=line.split()
					brlens=[]
					for a in range(nAl):
						brlens.append(float(linelist[3+a]))

				print("Simulated: "+str(BL)+" "+str(rate)+" "+str(gapP))
				Ls11=[]
				Ls12=[]
				LK1=0.0
				Ls21=[]
				Ls22=[]
				LK2=0.0
				#if 1-tree:
				#		transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(2*BL,0.0,rate,rate,gapP,gapP, model="LG", freqs=LGfreqs, ancestral=True, pypy=False)
				for al in range(nAl):
					#if tree:
					transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(2*brlens[al],0.0,rate,rate,gapP,gapP, model="LG", freqs=LGfreqs, ancestral=True, pypy=False)
					
					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+".fas")
					line=file.readline()
					line=file.readline()
					seq1=line.replace("\n","") #sequences
					line=file.readline()
					line=file.readline()
					seq2=line.replace("\n","")
					file.close()
					
					#print seq1
					#print seq2
					seq1="".join(seq1.split())
					seq2="".join(seq2.split())
					#Ls11.append(seq1[0:50])
					#Ls12.append(seq2[0:50])
					Ls21.append(seq1)
					Ls22.append(seq2)
					
					#LK1+=pairwise_alignment.forwardNWfast(s1=seq1[0:50], s2=seq2[0:50], t1=BL, t2=BL, ri=rate, rd=rate, gi=gapP, gd=gapP, model="HKY", freqs=[0.3, 0.2, 0.3, 0.2], pi=[0.3, 0.2, 0.3, 0.2], kappa=kappa, iModelType="approx", eModelType="approx")[0]
					#LK2+=pairwise_alignment_new.forwardNW3fast(s1=seq1, s2=seq2, t1=2*BL, ri=rate, rd=rate, gi=gapP, gd=gapP, model="HKY", freqs=[0.3, 0.2, 0.3, 0.2], pi=[0.3, 0.2, 0.3, 0.2], kappa=kappa, iModelType="approx", eModelType="approx")[0]
					print("likelihoods with and without adaptive banding 5 10 15 30 15000000")
					print(pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-5, alphabet="AA")[0])
					print(pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-10, alphabet="AA")[0])
					print(pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-15, alphabet="AA")[0])
					print(pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-30, alphabet="AA")[0])
					print(pairwise_alignment_new.forwardNW3fast(seq1,seq2, transProbs, stateProbsN, probs1, LGfreqs, epsilon=-15000000, alphabet="AA")[0])
				
	# test if long indels affect adaptive banding
	gapExtra=[100,250]
	if testBand2:
		for BL in BLs:
			for rate in rates:
				js=[1,5,3,3,3,3,3,3]
				iss=[3,2,1,2,2,2,2,2]
				fname=folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_morePRANK.txt"
				if os.path.isfile(fname):
					fileO=open(fname)
					estimates=[]
					#for j in range(7):
					estimates.append([])
					line =fileO.readline()
					linelist=line.split()
					print linelist
					for i in range(iss[0]):
						estimates[0].append(float(linelist[i+js[0]]))
					if tree:
						line =fileO.readline()
						linelist=line.split()
						brlens=[]
						for i in range(len(linelist)-3):
							brlens.append(float(linelist[3+i]))
					#estimates=[]
					for j in range(len(js)):
						estimates.append([])
						line =fileO.readline()
						if line=="":
							break
						linelist=line.split()
						print linelist
						for i in range(iss[j+1]):
							estimates[j+1].append(float(linelist[i+js[j+1]]))
					fileO.close()
				else:
					print("File "+fname+" not found!")
					exit()
				fileSMALL3=open(folderSimu+"SMALL3_piecesMissing_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+".txt","w")
				gapP=gapL

				methods=['cumIndels']
				iModels=['cumIndels']

				for al in range(nAl):
					print("\n")
					print("Simulated: "+str(2*brlens[al])+" "+str(rate)+" "+str(gapP))

					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+".fas")
					line=file.readline()
					line=file.readline()
					seq1=line.replace("\n","") #sequences
					line=file.readline()
					line=file.readline()
					seq2=line.replace("\n","")
					file.close()
					
					seq1="".join(seq1.split())
					seq2="".join(seq2.split())
					
					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+"_TRUE.phy")
					line=file.readline()
					lenS=int(line.split()[1])
					line=file.readline()
					al1=line.split()[1] # true alignment sequences
					line=file.readline()
					al2=line.split()[1]
					file.close()
					al1="".join(al1.split())
					al2="".join(al2.split())
					
					#s1n=al1.replace("-","")
					#s2n=al2.replace("-","")
					if tree:
						transProbsL=[]
						stateProbsL=[]
						probsL=[]
					
					countM=0.0
					for ig in range(len(al1)):
						if al1[ig]!="-" and al2[ig]!="-":
							countM=countM+1.0
					for i in range(len(iModels)):
						if tree:
							BLi=brlens[al]
							ratei=estimates[i+3][0]
							if i!=1:
								gapPi=estimates[i+3][1]
								transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,gapPi,gapPi, model="LG", ancestral=True, iModelType=iModels[i], pypy=False)
							else:
								transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,0.5,0.5, model="LG", ancestral=True, iModelType=iModels[i], pypy=False)
							transProbsL.append(transProbs)
							stateProbsL.append(stateProbsN)
							probsL.append(probs1)
						LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1, seq2, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-15.0, alphabet="AA")
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
						found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						fileSMALL3.write("Normal alignment "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						#fileSMALL3.write("Normal alignment "+methods[i]+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						print("Normal "+str(2*brlens[al])+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1, seq2, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-30.0, alphabet="AA")
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
						found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						fileSMALL3.write("Normal alignment30 "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						print("Normal30 "+str(2*brlens[al])+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						for gapE in gapExtra:
							seq1n=seq1[gapE:]
							seq2n=seq2
							countIG=0
							al1n=""
							al2n=""
							countM=0.0
							for ig in range(len(al1)):
								if al1[ig]!="-" and al2[ig]!="-":
									countIG+=1
									if countIG<=gapE:
										al1n+="-"
									else:
										countM=countM+1.0
										al1n+=al1[ig]
									al2n+=al2[ig]
								elif al1[ig]!="-" and al2[ig]=="-":
									countIG+=1
									if countIG>gapE:
										al1n+=al1[ig]
										al2n+=al2[ig]
								else:
									al1n+=al1[ig]
									al2n+=al2[ig]
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-15.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingFirst "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingFirst "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-30.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingFirst30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingFirst30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							
							
							seq1n=seq1[:len(seq1)/2-gapE/2]+seq1[len(seq1)/2+gapE/2:]
							seq2n=seq2
							countIG=0
							al1n=""
							al2n=""
							countM=0.0
							for ig in range(len(al1)):
								if al1[ig]!="-" and al2[ig]!="-":
									countIG+=1
									if countIG>(len(seq1)/2-gapE/2) and countIG<=(len(seq1)/2+gapE/2):
										al1n+="-"
									else:
										countM=countM+1.0
										al1n+=al1[ig]
									al2n+=al2[ig]
								elif al1[ig]!="-" and al2[ig]=="-":
									countIG+=1
									if countIG<=(len(seq1)/2-gapE/2) or countIG>(len(seq1)/2+gapE/2):
										al1n+=al1[ig]
										al2n+=al2[ig]
								else:
									al1n+=al1[ig]
									al2n+=al2[ig]
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-15.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingMiddle "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingMiddle "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-30.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingMiddle30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingMiddle30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							
							seq1n=seq1[:-gapE]
							seq2n=seq2
							countIG=0
							al1n=""
							al2n=""
							countM=0.0
							for ig in range(len(al1)):
								if al1[ig]!="-" and al2[ig]!="-":
									countIG+=1
									if countIG>=len(seq1)-gapE:
										al1n+="-"
									else:
										countM=countM+1.0
										al1n+=al1[ig]
									al2n+=al2[ig]
								elif al1[ig]!="-" and al2[ig]=="-":
									countIG+=1
									if countIG<len(seq1)-gapE:
										al1n+=al1[ig]
										al2n+=al2[ig]
								else:
									al1n+=al1[ig]
									al2n+=al2[ig]
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-15.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingLast "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingLast "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1n, seq2n, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-30.0, alphabet="AA")
							#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
							found, missed, extra = compareAlignments(SMALL1, SMALL2, al1n, al2n)
							fileSMALL3.write("missingLast30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
							print("missingLast30 "+str(gapE)+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						#exit()

				fileSMALL3.close()









	# test if long indels affect adaptive banding
	if testBand3:
		file=open(homePath+"band_sizes01.txt")
		bands15=[]
		bands30=[]
		for i in range(50):
			line=file.readline()
			linelist=line.split()
			bands15.append(int(linelist[5]))
			line=file.readline()
			linelist=line.split()
			bands30.append(int(linelist[5]))
		print("mean15: "+str(np.mean(bands15)/2)+"   median15: "+str(np.median(bands15)/2.0))
		print("mean30: "+str(np.mean(bands30)/2)+"   median30: "+str(np.median(bands30)/2.0))
		file.close()
		file=open(homePath+"band_sizes05.txt")
		bands15=[]
		bands30=[]
		for i in range(50):
			line=file.readline()
			linelist=line.split()
			bands15.append(int(linelist[5]))
			line=file.readline()
			linelist=line.split()
			bands30.append(int(linelist[5]))
		print("mean15: "+str(np.mean(bands15)/2)+"   median15: "+str(np.median(bands15)/2.0))
		print("mean30: "+str(np.mean(bands30)/2)+"   median30: "+str(np.median(bands30)/2.0))
		file.close()
		exit()
			
		fileO=open(homePath+"band_sizes01.txt")
		for BL in BLs:
			for rate in rates:
				js=[1,5,3,3,3,3,3,3]
				iss=[3,2,1,2,2,2,2,2]
				fname=folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_morePRANK.txt"
				if os.path.isfile(fname):
					fileO=open(fname)
					estimates=[]
					#for j in range(7):
					estimates.append([])
					line =fileO.readline()
					linelist=line.split()
					print linelist
					for i in range(iss[0]):
						estimates[0].append(float(linelist[i+js[0]]))
					if tree:
						line =fileO.readline()
						linelist=line.split()
						brlens=[]
						for i in range(len(linelist)-3):
							brlens.append(float(linelist[3+i]))
					#estimates=[]
					for j in range(len(js)):
						estimates.append([])
						line =fileO.readline()
						if line=="":
							break
						linelist=line.split()
						print linelist
						for i in range(iss[j+1]):
							estimates[j+1].append(float(linelist[i+js[j+1]]))
					fileO.close()
				else:
					print("File "+fname+" not found!")
					exit()
				#fileSMALL3=open("/needle_simu/SMALL3_piecesMissing_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+".txt","w")
				gapP=gapL

				methods=['cumIndels']
				iModels=['cumIndels']

				print("\n")
				print("Rate: "+str(rate))

				for al in range(nAl):
					#print("\n")
					#print("Simulated: "+str(2*brlens[al])+" "+str(rate)+" "+str(gapP))

					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+".fas")
					line=file.readline()
					line=file.readline()
					seq1=line.replace("\n","") #sequences
					line=file.readline()
					line=file.readline()
					seq2=line.replace("\n","")
					file.close()
					
					seq1="".join(seq1.split())
					seq2="".join(seq2.split())
					
					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+"_TRUE.phy")
					line=file.readline()
					lenS=int(line.split()[1])
					line=file.readline()
					al1=line.split()[1] # true alignment sequences
					line=file.readline()
					al2=line.split()[1]
					file.close()
					al1="".join(al1.split())
					al2="".join(al2.split())
					
					#s1n=al1.replace("-","")
					#s2n=al2.replace("-","")
					if tree:
						transProbsL=[]
						stateProbsL=[]
						probsL=[]
					
					countM=0.0
					for ig in range(len(al1)):
						if al1[ig]!="-" and al2[ig]!="-":
							countM=countM+1.0
					for i in range(len(iModels)):
						if tree:
							BLi=brlens[al]
							ratei=estimates[i+3][0]
							if i!=1:
								gapPi=estimates[i+3][1]
								transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,gapPi,gapPi, model="LG", ancestral=True, iModelType=iModels[i], pypy=False)
							else:
								transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,0.5,0.5, model="LG", ancestral=True, iModelType=iModels[i], pypy=False)
							transProbsL.append(transProbs)
							stateProbsL.append(stateProbsN)
							probsL.append(probs1)
						LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1, seq2, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-15.0, alphabet="AA")
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
						found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						#fileSMALL3.write("Normal alignment "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						#fileSMALL3.write("Normal alignment "+methods[i]+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						#print("Normal "+str(2*brlens[al])+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1, seq2, transProbsL[i], stateProbsL[i], probsL[i], LGfreqs, epsilon=-30.0, alphabet="AA")
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
						found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						#fileSMALL3.write("Normal alignment30 "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
						#print("Normal30 "+str(2*brlens[al])+" "+methods[i]+" "+str(found/countM)+" "+str(missed/countM)+" "+str(extra/countM)+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")

				#fileSMALL3.close()













			
	
	
	#do alignments and compare to truth
	if runAl:
		for BL in BLs:
			for rate in rates:
				#js=[1,5,3,3,3,3,3,3]
				#iss=[3,3,2,3,3,3,3,3]
				iss=[3,2,3,3,3,3,3]
				if tree:
					iss=[2,1,2,2,2,2,2]
					#iss=[3,2,1,2,2,2,2,2]
					
				if tree:
					fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_simulated.txt")
					line =fileO.readline()
					line =fileO.readline()
					linelist=line.split()
					brlens=[]
					for i in range(len(linelist)-3):
						brlens.append(float(linelist[3+i]))
					fileO.close()
				
				if InModel<7:
					estimates=[]
					#estimates.append([])
					maxLK=float("-inf")
					iModels=['TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0']
					if InModel>0:
						mod=iModels[InModel-1]
					for inTheta in range(3):
						if InModel==0:
							fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_fixedAl_"+str(inTheta)+".txt")
						else:
							fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+mod+"_"+str(inTheta)+".txt")
						line =fileO.readline()
						line =fileO.readline()
						LK=float(line.replace("\n",""))
						if LK>maxLK:
							maxLK=LK
							LKi=inTheta
						fileO.close()
					if InModel==0:
						fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_fixedAl_"+str(LKi)+".txt")
					else:
						fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+mod+"_"+str(LKi)+".txt")
					line =fileO.readline()
					line =fileO.readline()
					line =fileO.readline()
					linelist=line.split()
					for i in range(iss[InModel]):
						estimates.append(float(linelist[i])) #i+js[0]
						#estimates[InModel].append(float(linelist[i])) #i+js[0]
					fileO.close()
				
				if InModel==0:
					fileSMALL3=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+"_fixedAl.txt","w")
				elif InModel==7:
					fileSMALL3=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+"_EMBOSS.txt","w")
				else:
					fileSMALL3=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+"_"+mod+".txt","w")
					
				
				gapP=gapL
				print("Simulated: "+str(BL)+" "+str(rate)+" "+str(gapP))
				
				Ls11=[]
				Ls12=[]
				Ls21=[]
				Ls22=[]
				if InModel<7:
					print("estimates:")
					print(estimates)
				if tree:
					print("brlens")
					print(brlens)
				
				#iModels=['TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0']
				methods=['cumIndelsFixed','TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0','EMBOSS']
				iModels=['cumIndels','TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0','EMBOSS']
				if (not tree) and InModel<7:
					#transProbsL=[]
					#stateProbsL=[]
					#probsL=[]
					#for i in range(len(iModels)):
					BLi=estimates[0]
					ratei=estimates[1]
					if InModel!=1:
						gapPi=estimates[2]
						transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,gapPi,gapPi, model="LG", ancestral=True, iModelType=iModels[InModel], pypy=False)
					else:
						transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,0.5,0.5, model="LG", ancestral=True, iModelType=iModels[InModel], pypy=False)
					transProbsL=transProbs
					stateProbsL=stateProbsN
					probsL=probs1
				
				
				for al in range(nAl):

					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+".fas")
					line=file.readline()
					line=file.readline()
					seq1=line.replace("\n","") #sequences
					line=file.readline()
					line=file.readline()
					seq2=line.replace("\n","")
					file.close()
					
					#print seq1
					#print seq2
					seq1="".join(seq1.split())
					seq2="".join(seq2.split())
					
					file=open(folderSimu+"indelibleOut_ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+str(al)+"_TRUE.phy")
					line=file.readline()
					lenS=int(line.split()[1])
					line=file.readline()
					al1=line.split()[1] # true alignment sequences
					line=file.readline()
					al2=line.split()[1]
					file.close()
					al1="".join(al1.split())
					al2="".join(al2.split())

					if tree and InModel<7:
						BLi=brlens[al]
						ratei=estimates[0]
						if InModel!=1:
							gapPi=estimates[1]
							transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,gapPi,gapPi, model="LG", ancestral=True, iModelType=iModels[InModel], pypy=False)
						else:
							transProbs, stateProbsN, probs1 =pairwise_alignment_new.getProbs(BLi*2,0.0,ratei,ratei,0.5,0.5, model="LG", ancestral=True, iModelType=iModels[InModel], pypy=False)
						transProbsL=transProbs
						stateProbsL=stateProbsN
						probsL=probs1
						
					if InModel<7:
						LK, SMALL1, SMALL2 = pairwise_alignment_new.SMALLneedle3Fast(seq1, seq2, transProbsL, stateProbsL, probsL, LGfreqs, epsilon=-9.0, alphabet="AA")
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedleFast(s1=al1.replace("-",""), s2=al2.replace("-",""), t1=BLi, t2=BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai) #, rates=args.rates
						found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						#fileSMALL.write(str(found)+" "+str(missed)+" "+str(extra)+"\n")
				
						#LK, SMALL1, SMALL2 = pairwise_alignment.SMALLneedle3Fast(s1=seq1, s2=seq2, t1=2*BLi, epsilon=-15, iModelType="approx", eModelType="approx", ri=ratei, rd=ratei, gi=gapPi, gd=gapPi, model="HKY", freqs=[0.3, 0.2, 0.2, 0.3], pi=[0.3, 0.2, 0.2, 0.3], kappa=kappai)
						#found, missed, extra = compareAlignments(SMALL1, SMALL2, al1, al2)
						fileSMALL3.write(methods[InModel]+" "+str(found)+" "+str(missed)+" "+str(extra)+" "+str(LK)+"\n")
					
					if InModel==7:
					
						file=open(folderSimu+"reference_AA2"+brText+".fa","w")
						file.write(">A\n")
						file.write(seq1+"\n")
						file.close()
						file=open(folderSimu+"query_AA2"+brText+".fa","w")
						file.write(">B\n")
						file.write(seq2+"\n")
						file.close()
					
						#REPEAT both with and without end gap penalties: does it affect the results?
						os.system("cd "+folderSimu+"; "+embossPath+" -gapopen 10.0 -gapextend 0.5 -asequence "+folderSimu+"reference_AA2"+brText+".fa -bsequence "+folderSimu+"query_AA2"+brText+".fa -outfile "+folderSimu+"EMBOSS_out_AA2"+brText+".txt -sprotein1 true -sprotein2 true -aformat3 fasta")
						file=open(folderSimu+"EMBOSS_out_AA2"+brText+".txt")
						line=file.readline()
						line=file.readline()
						EMBOSS1=""
						while line!=">B\n":
							EMBOSS1+=line.replace("\n","")
							line=file.readline()
						line=file.readline()
						#print EMBOSS1
						EMBOSS2=""
						while len(line.split())>0:
							EMBOSS2+=line.replace("\n","")
							line=file.readline()
						#print EMBOSS2
						file.close()
						found, missed, extra = compareAlignments(EMBOSS1, EMBOSS2, al1, al2)
						fileSMALL3.write("EMBOSS "+str(found)+" "+str(missed)+" "+str(extra)+"\n")
					
					
				#fileSMALL.close()
				fileSMALL3.close()
				#fileEMBOSS.close()
				#fileEMBOSS2.close()

			
			
	
	
			
	if doPlots:
		
		js=[1,5,3,3,3,3,3,3]
		iss=[3,3,2,3,3,3,3,3]
		TKF91no=2
		if tree:
			iss=[2,2,1,2,2,2,2,2]
			js=[2,5,3,3,3,3,3,3]
			TKF91no=1
		res=[]
		for b in range(len(BLs)):
			res.append([])
			for r in range(len(rates)):
				res[b].append([])
				res[b][r].append([])
				rate=rates[r]
				BL=BLs[b]
				fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_simulated.txt")
				line =fileO.readline()
				linelist=line.split()
				for i in range(iss[0]):
					res[b][r][0].append(float(linelist[i+js[0]]))
				fileO.close()
				
				iModels=['fixedAl','TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0'] 
				for InModel in range(len(iModels)):
					res[b][r].append([])
					mod=iModels[InModel]
					estimates=[]
					maxLK=float("-inf")
					LKi=-1
					for inTheta in range(3):
						fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+mod+"_"+str(inTheta)+".txt")
						line =fileO.readline()
						line =fileO.readline()
						LK=float(line.replace("\n",""))
						if LK>maxLK:
							maxLK=LK
							LKi=inTheta
						fileO.close()
					fileO=open(folderSimu+"ML_AA"+brText+"_BL"+str(BL)+"_rate"+str(rate)+"_"+mod+"_"+str(LKi)+".txt")
					line =fileO.readline()
					line =fileO.readline()
					line =fileO.readline()
					linelist=line.split()
					for i in range(iss[InModel+1]):
						res[b][r][InModel+1].append(float(linelist[i]))
					fileO.close()

		
		par=["Blen","IndRate","gapExt"]
		pars=[BLs,	rates,	gapL]
		if tree:
			par=["IndRate","gapExt"]
			pars=[rates,	gapL]
		mets=["fixed",'TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0'] #"fixed","forward3"
		#metInd=[1,2,3,4,5,6]
		linErr=[]
		for p in range(len(par)):
			linErr.append([])
			for m in range(len(mets)):
				lin=0.0
				quad=0.0
				bias=0.0
				propErrLin=0.0
				propBias=0.0
				if p!=TKF91no or m!=1:
					for b in range(len(BLs)):
						for r in range(len(rates)):
					#for j in range(len(res[p][0])):
							lin+=abs(res[b][r][0][p]-res[b][r][m+1][p])
							quad+=(res[b][r][0][p]-res[b][r][m+1][p])**2
							bias+=(res[b][r][m+1][p]-res[b][r][0][p])
							propErrLin+=abs((res[b][r][0][p]-res[b][r][m+1][p])/res[b][r][0][p])
							propBias+=(res[b][r][m+1][p]-res[b][r][0][p])/res[b][r][0][p]
					print "\n parameter "+par[p]+ " method "+mets[m]
					print "linear error: "+ str(lin)
					print "quad error "+str(math.sqrt(quad))
					print "bias "+str(bias)
					print "linear error proportional "+str(propErrLin)
					print "prop bias "+str(propBias)

		#times=[]
		#xAxis=[]
		for i in range(len(par)):
			for r in range(len(rates)):
				values=[]
				for m in range(len(mets)+1):
					values.append([])
					if i!=TKF91no or m!=2:
						for b in range(len(BLs)):
							values[m].append([res[b][r][m][i]])

				ticks=BLs

				def set_box_color(bp, color):
					plt.setp(bp['boxes'], color=color)
					plt.setp(bp['whiskers'], color=color)
					plt.setp(bp['caps'], color=color)
					plt.setp(bp['medians'], color=color)

				plt.figure()
				space=14

				if tree:
					bords=[0.0,1.0]
				else:
					bords=[0.0,0.18,0.4,0.60,0.82,1.0]
				for tr in range(len(values[0])):
					#plt.axhline(y=values[0][tr], xmin=(float(tr))/len(values[0]), xmax=(tr+1.0)/len(values[0]), linestyle="--", color='lightblue')#(tr+1)*space
					plt.axhline(y=values[0][tr], xmin=bords[tr], xmax=bords[tr+1], linestyle="--", color='lightblue')#(tr+1)*space

				
				plt.plot(np.array(xrange(len(values[1])))*space-3.5, values[1], c='cyan', label='Fixed alignment', linewidth=0.5, markersize=8, marker='o',linestyle="")
				if i!=TKF91no:
					plt.plot(np.array(xrange(len(values[2])))*space-2.5, values[2], c='blue', label='TKF91', linewidth=0.5, markersize=8, marker='v',linestyle="")
				plt.plot(np.array(xrange(len(values[3])))*space-1.5, values[3], c='green', label='CumIndel', linewidth=0.5, markersize=8, marker='^',linestyle="")
				plt.plot(np.array(xrange(len(values[4])))*space-0.5, values[4], c='orange', label='TKF92', linewidth=0.5, markersize=8, marker='s',linestyle="")
				plt.plot(np.array(xrange(len(values[5])))*space+0.5, values[5], c='purple', label='RS07', linewidth=0.5, markersize=8, marker='p',linestyle="")
				plt.plot(np.array(xrange(len(values[6])))*space+1.5, values[6], c='black', label='PRANK', linewidth=0.5, markersize=8, marker='D',linestyle="")
				plt.plot(np.array(xrange(len(values[7])))*space+2.5, values[7], c='grey', label='PRANK_gamma=0', linewidth=0.5, markersize=8, marker='D',linestyle="")
				plt.plot([], c='lightblue', label='simulated', linestyle="--")
				plt.legend()
				
				plt.xticks(xrange(0, len(ticks) * space, space), ticks)
				plt.xlim(-5, len(ticks)*space-9.5)
				#plt.ylim(0, yl)
				plt.tight_layout()
				#plt.yscale("log")
				plt.savefig(folderPlots+"parameter_inference_AA"+brText+"_rate"+str(rates[r])+"_par"+str(par[i])+"_morePRANK.pdf")
				
				if par[i]=="IndRate":
					ticks=BLs

					def set_box_color(bp, color):
						plt.setp(bp['boxes'], color=color)
						plt.setp(bp['whiskers'], color=color)
						plt.setp(bp['caps'], color=color)
						plt.setp(bp['medians'], color=color)

					plt.figure()
					space=14
					if tree:
						bords=[0.0,1.0]
					else:
						bords=[0.0,0.18,0.4,0.60,0.82,1.0]
					for tr in range(len(values[0])):
						#plt.axhline(y=values[0][tr], xmin=(float(tr))/len(values[0]), xmax=(tr+1.0)/len(values[0]), linestyle="--", color='lightblue')#(tr+1)*space
						plt.axhline(y=values[0][tr], xmin=bords[tr], xmax=bords[tr+1], linestyle="--", color='lightblue')#(tr+1)*space

					plt.plot(np.array(xrange(len(values[1])))*space-3.5, values[1], c='cyan', label='Fixed alignment', linewidth=0.5, markersize=8, marker='o',linestyle="")
					#if i!=TKF91no:
					#	plt.plot(np.array(xrange(len(values[2])))*space-2.5, values[2], c='blue', label='TKF91', linewidth=0.5, markersize=8, marker='v',linestyle="")
					plt.plot(np.array(xrange(len(values[3])))*space-1.5, values[3], c='green', label='CumIndel', linewidth=0.5, markersize=8, marker='^',linestyle="")
					plt.plot(np.array(xrange(len(values[4])))*space-0.5, values[4], c='orange', label='TKF92', linewidth=0.5, markersize=8, marker='s',linestyle="")
					plt.plot(np.array(xrange(len(values[5])))*space+0.5, values[5], c='purple', label='RS07', linewidth=0.5, markersize=8, marker='p',linestyle="")
					plt.plot(np.array(xrange(len(values[6])))*space+1.5, values[6], c='black', label='PRANK', linewidth=0.5, markersize=8, marker='D',linestyle="")
					plt.plot(np.array(xrange(len(values[7])))*space+2.5, values[7], c='grey', label='PRANK_gamma=0', linewidth=0.5, markersize=8, marker='D',linestyle="")
					plt.plot([], c='lightblue', label='simulated', linestyle="--")
					plt.legend()
				
					plt.xticks(xrange(0, len(ticks) * space, space), ticks)
					plt.xlim(-5, len(ticks)*space-9.5)
					#plt.ylim(0, yl)
					plt.tight_layout()
					#plt.yscale("log")
					plt.savefig(folderPlots+"parameter_inference_AA"+brText+"_rate"+str(rates[r])+"_par"+str(par[i])+"_morePRANK_noTKF91.pdf")
				
		#exit()
				
		#print("arriving here at testband flag?")
		if testBand3 or testBand2 or testBand:
			#fileSMALL3=open("/needle_simu/SMALL3_piecesMissing_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+".txt","w")
			#Alignment test2 figures
			mets=['Normal_15','Normal_30','First100_15','First100_30','Middle100_15','Middle100_30','Last100_15','Last100_30','First250_15','First250_30','Middle250_15','Middle250_30','Last250_15','Last250_30']
			for r in range(len(rates)):
				values=[]
				for m in range(len(mets)):
					values.append([[],[],[],[],[],[],[]])
				for b in range(len(BLs)):
					fileSMALL3=open(folderSimu+"SMALL3_piecesMissing_AA"+brText+"_results_new_"+str(BLs[b])+"_"+str(rates[r])+".txt")
					#values=[]
					#for m in range(len(mets)+1):
					#	values.append([[],[]])
					for m in range(len(mets)):
						values[m][0].append([])
						values[m][1].append([])
						values[m][2].append([])
						values[m][3].append([])
						values[m][4].append([])
						values[m][5].append([])
						values[m][6].append([])
					line=fileSMALL3.readline()
					while line!="\n" and line!="":
						for m in range(len(mets)):
							linelist=line.split()
							line=fileSMALL3.readline()
							values[m][0][b].append(float(linelist[4]))
							values[m][1][b].append(float(linelist[5]))
							values[m][2][b].append(float(linelist[3]))
							values[m][3][b].append(float(linelist[7]))
							values[m][4][b].append(float(linelist[8]))
							values[m][5][b].append(float(linelist[6]))
							values[m][6][b].append(float(linelist[9]))
				ticks=BLs
				def set_box_color(bp, color):
					plt.setp(bp['boxes'], color=color)
					plt.setp(bp['whiskers'], color=color)
					plt.setp(bp['caps'], color=color)
					plt.setp(bp['medians'], color=color)

				plt.figure()
				space=18
				#print data
				bp0 = plt.boxplot(values[0][0], positions=np.array(xrange(len(values[0][0])))*space-7, sym='', widths=0.8)
				bp1 = plt.boxplot(values[1][0], positions=np.array(xrange(len(values[1][0])))*space-6, sym='', widths=0.8)
				bp2 = plt.boxplot(values[2][0], positions=np.array(xrange(len(values[2][0])))*space-5, sym='', widths=0.8)
				bp3 = plt.boxplot(values[3][0], positions=np.array(xrange(len(values[3][0])))*space-4, sym='', widths=0.8)
				bp4 = plt.boxplot(values[4][0], positions=np.array(xrange(len(values[4][0])))*space-3, sym='', widths=0.8)
				bp5 = plt.boxplot(values[5][0], positions=np.array(xrange(len(values[5][0])))*space-2, sym='', widths=0.8)
				bp6 = plt.boxplot(values[6][0], positions=np.array(xrange(len(values[6][0])))*space-1, sym='', widths=0.8)
				bp7 = plt.boxplot(values[7][0], positions=np.array(xrange(len(values[7][0])))*space, sym='', widths=0.8)
				bp8 = plt.boxplot(values[8][0], positions=np.array(xrange(len(values[8][0])))*space+1, sym='', widths=0.8)
				bp9 = plt.boxplot(values[9][0], positions=np.array(xrange(len(values[9][0])))*space+2, sym='', widths=0.8)
				bp10 = plt.boxplot(values[10][0], positions=np.array(xrange(len(values[10][0])))*space+3, sym='', widths=0.8)
				bp11 = plt.boxplot(values[11][0], positions=np.array(xrange(len(values[11][0])))*space+4, sym='', widths=0.8)
				bp12 = plt.boxplot(values[12][0], positions=np.array(xrange(len(values[12][0])))*space+5, sym='', widths=0.8)
				bp13 = plt.boxplot(values[13][0], positions=np.array(xrange(len(values[13][0])))*space+6, sym='', widths=0.8)
				set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
				set_box_color(bp1, 'cyan')
				set_box_color(bp2, 'blue')
				set_box_color(bp3, 'blue')
				set_box_color(bp4, 'green')
				set_box_color(bp5, 'green')
				set_box_color(bp6, 'orange')
				set_box_color(bp7, 'orange')
				set_box_color(bp8, 'purple')
				set_box_color(bp9, 'purple')
				set_box_color(bp10, 'black')
				set_box_color(bp11, 'black')
				set_box_color(bp12, 'red')
				set_box_color(bp13, 'red')
				#set_box_color(bp7, 'brown')

				plt.plot([], c='cyan', label='Normal alignment')
				plt.plot([], c='blue', label='Missing first 100')
				plt.plot([], c='green', label='Missing middle 100')
				plt.plot([], c='orange', label='Missing last 100')
				plt.plot([], c='purple', label='Missing first 250')
				plt.plot([], c='black', label='Missing middle 250')
				plt.plot([], c='red', label='Missing last 250')
				#plt.plot([], c='brown', label='EMBOSS')
				#plt.legend()
				#for tr in range(len(values[0])):
					#tr=0
				#	plt.axhline(y=values[0][tr], xmin=(float(tr))/len(values[0]), xmax=(tr+1.0)/len(values[0]), linestyle="--", color='lightblue')#(tr+1)*space

				plt.xticks(xrange(0, len(ticks) * space, space), ticks)
				plt.xlim(-8.5, len(ticks)*space-10.5)
				#plt.ylim(0, yl)
				plt.tight_layout()
				#plt.yscale("log")
				plt.savefig(folderPlots+"alignment_pieces_AA"+brText+"_rate"+str(rates[r])+"_missed.png")
			
				plt.figure()
				space=18
				#print data
				bp0 = plt.boxplot(values[0][1], positions=np.array(xrange(len(values[0][1])))*space-7, sym='', widths=0.8)
				bp1 = plt.boxplot(values[1][1], positions=np.array(xrange(len(values[1][1])))*space-6, sym='', widths=0.8)
				bp2 = plt.boxplot(values[2][1], positions=np.array(xrange(len(values[2][1])))*space-5, sym='', widths=0.8)
				bp3 = plt.boxplot(values[3][1], positions=np.array(xrange(len(values[3][1])))*space-4, sym='', widths=0.8)
				bp4 = plt.boxplot(values[4][1], positions=np.array(xrange(len(values[4][1])))*space-3, sym='', widths=0.8)
				bp5 = plt.boxplot(values[5][1], positions=np.array(xrange(len(values[5][1])))*space-2, sym='', widths=0.8)
				bp6 = plt.boxplot(values[6][1], positions=np.array(xrange(len(values[6][1])))*space-1, sym='', widths=0.8)
				bp7 = plt.boxplot(values[7][1], positions=np.array(xrange(len(values[7][1])))*space, sym='', widths=0.8)
				bp8 = plt.boxplot(values[8][1], positions=np.array(xrange(len(values[8][1])))*space+1, sym='', widths=0.8)
				bp9 = plt.boxplot(values[9][1], positions=np.array(xrange(len(values[9][1])))*space+2, sym='', widths=0.8)
				bp10 = plt.boxplot(values[10][1], positions=np.array(xrange(len(values[10][1])))*space+3, sym='', widths=0.8)
				bp11 = plt.boxplot(values[11][1], positions=np.array(xrange(len(values[11][1])))*space+4, sym='', widths=0.8)
				bp12 = plt.boxplot(values[12][1], positions=np.array(xrange(len(values[12][1])))*space+5, sym='', widths=0.8)
				bp13 = plt.boxplot(values[13][1], positions=np.array(xrange(len(values[13][1])))*space+6, sym='', widths=0.8)
				set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
				set_box_color(bp1, 'cyan')
				set_box_color(bp2, 'blue')
				set_box_color(bp3, 'blue')
				set_box_color(bp4, 'green')
				set_box_color(bp5, 'green')
				set_box_color(bp6, 'orange')
				set_box_color(bp7, 'orange')
				set_box_color(bp8, 'purple')
				set_box_color(bp9, 'purple')
				set_box_color(bp10, 'black')
				set_box_color(bp11, 'black')
				set_box_color(bp12, 'red')
				set_box_color(bp13, 'red')
				#set_box_color(bp7, 'brown')

				plt.plot([], c='cyan', label='Normal alignment')
				plt.plot([], c='blue', label='Missing first 100')
				plt.plot([], c='green', label='Missing middle 100')
				plt.plot([], c='orange', label='Missing last 100')
				plt.plot([], c='purple', label='Missing first 250')
				plt.plot([], c='black', label='Missing middle 250')
				plt.plot([], c='red', label='Missing last 250')
				#plt.plot([], c='brown', label='EMBOSS')
				plt.legend()
				#for tr in range(len(values[0])):
					#tr=0
				#	plt.axhline(y=values[0][tr], xmin=(float(tr))/len(values[0]), xmax=(tr+1.0)/len(values[0]), linestyle="--", color='lightblue')#(tr+1)*space

				plt.xticks(xrange(0, len(ticks) * space, space), ticks)
				plt.xlim(-8.5, len(ticks)*space-10.5)
				#plt.ylim(0, yl)
				plt.tight_layout()
				#plt.yscale("log")
				plt.savefig(folderPlots+"alignment_pieces_AA"+brText+"_rate"+str(rates[r])+"_extra.png")
			
			
			
				plt.figure()
				space=18
				#print data
				bp0 = plt.boxplot(values[0][2], positions=np.array(xrange(len(values[0][2])))*space-7, sym='', widths=0.8)
				bp1 = plt.boxplot(values[1][2], positions=np.array(xrange(len(values[1][2])))*space-6, sym='', widths=0.8)
				bp2 = plt.boxplot(values[2][2], positions=np.array(xrange(len(values[2][2])))*space-5, sym='', widths=0.8)
				bp3 = plt.boxplot(values[3][2], positions=np.array(xrange(len(values[3][2])))*space-4, sym='', widths=0.8)
				bp4 = plt.boxplot(values[4][2], positions=np.array(xrange(len(values[4][2])))*space-3, sym='', widths=0.8)
				bp5 = plt.boxplot(values[5][2], positions=np.array(xrange(len(values[5][2])))*space-2, sym='', widths=0.8)
				bp6 = plt.boxplot(values[6][2], positions=np.array(xrange(len(values[6][2])))*space-1, sym='', widths=0.8)
				bp7 = plt.boxplot(values[7][2], positions=np.array(xrange(len(values[7][2])))*space, sym='', widths=0.8)
				bp8 = plt.boxplot(values[8][2], positions=np.array(xrange(len(values[8][2])))*space+1, sym='', widths=0.8)
				bp9 = plt.boxplot(values[9][2], positions=np.array(xrange(len(values[9][2])))*space+2, sym='', widths=0.8)
				bp10 = plt.boxplot(values[10][2], positions=np.array(xrange(len(values[10][2])))*space+3, sym='', widths=0.8)
				bp11 = plt.boxplot(values[11][2], positions=np.array(xrange(len(values[11][2])))*space+4, sym='', widths=0.8)
				bp12 = plt.boxplot(values[12][2], positions=np.array(xrange(len(values[12][2])))*space+5, sym='', widths=0.8)
				bp13 = plt.boxplot(values[13][2], positions=np.array(xrange(len(values[13][2])))*space+6, sym='', widths=0.8)
				set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
				set_box_color(bp1, 'cyan')
				set_box_color(bp2, 'blue')
				set_box_color(bp3, 'blue')
				set_box_color(bp4, 'green')
				set_box_color(bp5, 'green')
				set_box_color(bp6, 'orange')
				set_box_color(bp7, 'orange')
				set_box_color(bp8, 'purple')
				set_box_color(bp9, 'purple')
				set_box_color(bp10, 'black')
				set_box_color(bp11, 'black')
				set_box_color(bp12, 'red')
				set_box_color(bp13, 'red')
				#set_box_color(bp7, 'brown')

				plt.plot([], c='cyan', label='Normal alignment')
				plt.plot([], c='blue', label='Missing first 100')
				plt.plot([], c='green', label='Missing middle 100')
				plt.plot([], c='orange', label='Missing last 100')
				plt.plot([], c='purple', label='Missing first 250')
				plt.plot([], c='black', label='Missing middle 250')
				plt.plot([], c='red', label='Missing last 250')
				#plt.plot([], c='brown', label='EMBOSS')
				#plt.legend()
				#for tr in range(len(values[0])):
					#tr=0
				#	plt.axhline(y=values[0][tr], xmin=(float(tr))/len(values[0]), xmax=(tr+1.0)/len(values[0]), linestyle="--", color='lightblue')#(tr+1)*space

				plt.xticks(xrange(0, len(ticks) * space, space), ticks)
				plt.xlim(-8.5, len(ticks)*space-10.5)
				#plt.ylim(0, yl)
				plt.tight_layout()
				#plt.yscale("log")
				plt.savefig(folderPlots+"alignment_pieces_AA"+brText+"_rate"+str(rates[r])+"_found_.png")
			
				#exit()
			
			
			
			
		#print("arriving here at alignment plotting?")
	
		#Alignment figures
		iModels=['fixedAl','TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0','EMBOSS']
		#fileO=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BL)+"_"+str(rate)+"_"+mod+".txt")
		mets=["fixed",'TKF91','cumIndels','TKF92','RS07','PRANK','PRANK0','EMBOSS']
		for r in range(len(rates)):
			values=[]
			for m in range(len(mets)):
				values.append([[],[],[],[]])
			for b in range(len(BLs)):
				#fileSMALL3=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BLs[b])+"_"+str(rates[r])+"_morePRANK.txt")
				for m in range(len(mets)):
					values[m][0].append([])
					values[m][1].append([])
					values[m][2].append([])
					values[m][3].append([])
					fileSMALL3=open(folderSimu+"SMALL3_AA"+brText+"_results_new_"+str(BLs[b])+"_"+str(rates[r])+"_"+iModels[m]+".txt")
					line=fileSMALL3.readline()
					while line!="\n" and line!="":
						#for m in range(len(mets)):
						linelist=line.split()
						line=fileSMALL3.readline()
						values[m][0][b].append(float(linelist[2]))
						values[m][1][b].append(float(linelist[3]))
						values[m][2][b].append(float(linelist[1]))
						if m!=7:
							values[m][3][b].append(float(linelist[4]))
					fileSMALL3.close()
			ticks=BLs

			def set_box_color(bp, color):
				plt.setp(bp['boxes'], color=color)
				plt.setp(bp['whiskers'], color=color)
				plt.setp(bp['caps'], color=color)
				plt.setp(bp['medians'], color=color)
			
			if tree:
				print("means: PRANK0 "+str(np.mean(values[6][0][0]))+" "+str(np.mean(values[6][1][0]))+" "+str(np.mean(values[6][2][0]))+" CumIndel:  "+str(np.mean(values[2][0][0]))+" "+str(np.mean(values[2][1][0]))+" "+str(np.mean(values[6][2][0]))+"\n")	

			plt.figure()
			space=14
			#print data
			bp0 = plt.boxplot(values[0][0], positions=np.array(xrange(len(values[0][0])))*space-3, sym='', widths=0.8)
			bp1 = plt.boxplot(values[1][0], positions=np.array(xrange(len(values[1][0])))*space-2, sym='', widths=0.8)
			bp2 = plt.boxplot(values[2][0], positions=np.array(xrange(len(values[2][0])))*space-1, sym='', widths=0.8)
			bp3 = plt.boxplot(values[3][0], positions=np.array(xrange(len(values[3][0])))*space, sym='', widths=0.8)
			bp4 = plt.boxplot(values[4][0], positions=np.array(xrange(len(values[4][0])))*space+1, sym='', widths=0.8)
			bp5 = plt.boxplot(values[5][0], positions=np.array(xrange(len(values[5][0])))*space+2, sym='', widths=0.8)
			bp6 = plt.boxplot(values[6][0], positions=np.array(xrange(len(values[6][0])))*space+3, sym='', widths=0.8)
			bp7 = plt.boxplot(values[7][0], positions=np.array(xrange(len(values[7][0])))*space+4, sym='', widths=0.8)
			set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
			set_box_color(bp1, 'blue')
			set_box_color(bp2, 'green')
			set_box_color(bp3, 'orange')
			set_box_color(bp4, 'purple')
			set_box_color(bp5, 'black')
			set_box_color(bp6, 'grey')
			set_box_color(bp7, 'brown')

			plt.plot([], c='cyan', label='Parameters from fixed alignment')
			plt.plot([], c='blue', label='TKF91')
			plt.plot([], c='green', label='CumIndel')
			plt.plot([], c='orange', label='TKF92')
			plt.plot([], c='purple', label='RS07')
			plt.plot([], c='black', label='PRANK')
			plt.plot([], c='grey', label='PRANK_gamma=0')
			plt.plot([], c='brown', label='EMBOSS')
			plt.legend()
			

			plt.xticks(xrange(0, len(ticks) * space, space), ticks)
			plt.xlim(-4, len(ticks)*space-9.5)
			#plt.ylim(0, yl)
			plt.tight_layout()
			#plt.yscale("log")
			#print(folderPlots+"alignment_inference_AA"+brText+"_rate"+str(rates[r])+"_missed_morePRANK.png")
			plt.savefig(folderPlots+"alignment_inference_AA"+brText+"_rate"+str(rates[r])+"_missed_morePRANK.png")
			
			
			
			plt.figure()
			space=14
			#print data
			bp0 = plt.boxplot(values[0][1], positions=np.array(xrange(len(values[0][1])))*space-3, sym='', widths=0.8)
			bp1 = plt.boxplot(values[1][1], positions=np.array(xrange(len(values[1][1])))*space-2, sym='', widths=0.8)
			bp2 = plt.boxplot(values[2][1], positions=np.array(xrange(len(values[2][1])))*space-1, sym='', widths=0.8)
			bp3 = plt.boxplot(values[3][1], positions=np.array(xrange(len(values[3][1])))*space, sym='', widths=0.8)
			bp4 = plt.boxplot(values[4][1], positions=np.array(xrange(len(values[4][1])))*space+1, sym='', widths=0.8)
			bp5 = plt.boxplot(values[5][1], positions=np.array(xrange(len(values[5][1])))*space+2, sym='', widths=0.8)
			bp6 = plt.boxplot(values[6][1], positions=np.array(xrange(len(values[6][1])))*space+3, sym='', widths=0.8)
			bp7 = plt.boxplot(values[7][1], positions=np.array(xrange(len(values[7][1])))*space+4, sym='', widths=0.8)
			set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
			set_box_color(bp1, 'blue')
			set_box_color(bp2, 'green')
			set_box_color(bp3, 'orange')
			set_box_color(bp4, 'purple')
			set_box_color(bp5, 'black')
			set_box_color(bp6, 'grey')
			set_box_color(bp7, 'brown')

			plt.plot([], c='cyan', label='Parameters from fixed alignment')
			plt.plot([], c='blue', label='TKF91')
			plt.plot([], c='green', label='CumIndel')
			plt.plot([], c='orange', label='TKF92')
			plt.plot([], c='purple', label='RS07')
			plt.plot([], c='black', label='PRANK')
			plt.plot([], c='grey', label='PRANK_gamma=0')
			plt.plot([], c='brown', label='EMBOSS')
			plt.legend()

			plt.xticks(xrange(0, len(ticks) * space, space), ticks)
			plt.xlim(-4, len(ticks)*space-9.5)
			#plt.ylim(0, yl)
			plt.tight_layout()
			#plt.yscale("log")
			plt.savefig(folderPlots+"alignment_inference_AA"+brText+"_rate"+str(rates[r])+"_extra_morePRANK.png")
			
			
			
			
			
			plt.figure()
			space=14
			#print data
			bp0 = plt.boxplot(values[0][2], positions=np.array(xrange(len(values[0][2])))*space-3, sym='', widths=0.8)
			bp1 = plt.boxplot(values[1][2], positions=np.array(xrange(len(values[1][2])))*space-2, sym='', widths=0.8)
			bp2 = plt.boxplot(values[2][2], positions=np.array(xrange(len(values[2][2])))*space-1, sym='', widths=0.8)
			bp3 = plt.boxplot(values[3][2], positions=np.array(xrange(len(values[3][2])))*space, sym='', widths=0.8)
			bp4 = plt.boxplot(values[4][2], positions=np.array(xrange(len(values[4][2])))*space+1, sym='', widths=0.8)
			bp5 = plt.boxplot(values[5][2], positions=np.array(xrange(len(values[5][2])))*space+2, sym='', widths=0.8)
			bp6 = plt.boxplot(values[6][2], positions=np.array(xrange(len(values[6][2])))*space+3, sym='', widths=0.8)
			bp7 = plt.boxplot(values[7][2], positions=np.array(xrange(len(values[7][2])))*space+4, sym='', widths=0.8)
			set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
			set_box_color(bp1, 'blue')
			set_box_color(bp2, 'green')
			set_box_color(bp3, 'orange')
			set_box_color(bp4, 'purple')
			set_box_color(bp5, 'black')
			set_box_color(bp6, 'grey')
			set_box_color(bp7, 'brown')

			plt.plot([], c='cyan', label='Parameters from fixed alignment')
			plt.plot([], c='blue', label='TKF91')
			plt.plot([], c='green', label='CumIndel')
			plt.plot([], c='orange', label='TKF92')
			plt.plot([], c='purple', label='RS07')
			plt.plot([], c='black', label='PRANK')
			plt.plot([], c='grey', label='PRANK_gamma=0')
			plt.plot([], c='brown', label='EMBOSS')
			plt.legend()

			plt.xticks(xrange(0, len(ticks) * space, space), ticks)
			plt.xlim(-4, len(ticks)*space-9.5)
			#plt.ylim(0, yl)
			plt.tight_layout()
			#plt.yscale("log")
			plt.savefig(folderPlots+"alignment_inference_AA"+brText+"_rate"+str(rates[r])+"_found_morePRANK.png")
			
			
			
			
			
			plt.figure()
			space=14
			#print data
			
			for j7 in range(len(values[0][3])):
				for k7 in range(len(values[0][3][j7])):
					max=float("-inf")
					for i7 in range(7):
						if values[i7][3][j7][k7]>max:
							max=values[i7][3][j7][k7]
					for i7 in range(7):
						if i7!=1:
							values[i7][3][j7][k7]=values[i7][3][j7][k7]-max
			bp0 = plt.boxplot(values[0][3], positions=np.array(xrange(len(values[0][3])))*space-2, sym='', widths=0.8)
			#bp1 = plt.boxplot(values[1][2], positions=np.array(xrange(len(values[1][2])))*space-2, sym='', widths=0.8)
			bp2 = plt.boxplot(values[2][3], positions=np.array(xrange(len(values[2][3])))*space-1, sym='', widths=0.8)
			bp3 = plt.boxplot(values[3][3], positions=np.array(xrange(len(values[3][3])))*space, sym='', widths=0.8)
			bp4 = plt.boxplot(values[4][3], positions=np.array(xrange(len(values[4][3])))*space+1, sym='', widths=0.8)
			bp5 = plt.boxplot(values[5][3], positions=np.array(xrange(len(values[5][3])))*space+2, sym='', widths=0.8)
			bp6 = plt.boxplot(values[6][3], positions=np.array(xrange(len(values[6][3])))*space+3, sym='', widths=0.8)
			#bp6 = plt.boxplot(values[6][3], positions=np.array(xrange(len(values[6][3])))*space+3, sym='', widths=0.8)
			set_box_color(bp0, 'cyan') # colors are from http://colorbrewer2.org/
			#set_box_color(bp1, 'blue')
			set_box_color(bp2, 'green')
			set_box_color(bp3, 'orange')
			set_box_color(bp4, 'purple')
			set_box_color(bp5, 'black')
			set_box_color(bp6, 'grey')
			#set_box_color(bp6, 'brown')

			plt.plot([], c='cyan', label='Parameters from fixed alignment')
			#plt.plot([], c='blue', label='TKF91')
			plt.plot([], c='green', label='CumIndel')
			plt.plot([], c='orange', label='TKF92')
			plt.plot([], c='purple', label='RS07')
			plt.plot([], c='black', label='PRANK')
			plt.plot([], c='grey', label='PRANK_gamma=0')
			#plt.plot([], c='brown', label='EMBOSS')
			plt.legend()

			plt.xticks(xrange(0, len(ticks) * space, space), ticks)
			plt.xlim(-4, len(ticks)*space-9.5)
			#plt.ylim(0, yl)
			plt.tight_layout()
			#plt.yscale("log")
			plt.savefig(folderPlots+"alignment_inference_AA"+brText+"_rate"+str(rates[r])+"_LK_morePRANK.png")
		
		
		
		
	
	exit()









































#Check how well inserts lengths fit cumulative indel model
insertL=0
TKF91case=0
onlyTable=0
if insertL:

	numsI=[]
	numsD=[]
	numsID=[]
	numsM=[]
	meansI=[]
	meansD=[]
	
	#new statistics
	numsChop=[]
	varianceI=[]
	varianceD=[]
	meanChop=[]
	varianceChop=[]
	medianChop=[]
	medianI=[]
	medianD=[]

	runSims=1
	runSims2=0
	reDoPlots=1
	reDoPlots2=0
	newPlotsOnly=True
	intervalB=0.015 #0.01
	Nintervals=100 #150
	genomesize=2000000
	
	if reDoPlots2:
		gi=0.75
		gd=0.75
		ri=1.0
		rd=1.0
		y0=[0.0,0.0,0.0,0.0,500000,0.0]
		def func(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] surviving homology columns followed by deletions, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
			dLen=rd/(1.0-gd)
			if y[0]<0.000000000001:
				gti=gi
			else:
				gti=1.0-y[0]/y[2]
			PineritIn=(y[0]/y[4])*(1.0-gd)/((1.0-(1.0-y[0]/y[4])*gd)*(1.0-gd*gti))
			PinsLost=(1.0-gti)*(1.0-gd)/((1.0-gd*(1.0-y[0]/y[4]))*(1.0-gti*gd))
			return [  (y[4]-y[0])*ri - y[0]*dLen + (y[4]-y[0])*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + rd*(y[0]-y[5])*gd/(1.0-gd*gti)      ,    (y[4]+y[2])*ri/(1.0-gi) - y[2]*dLen     ,    y[4]*dLen       ,    -y[4]*dLen     ,      (y[1]-y[5])*ri - y[5]*dLen + (y[0]-y[5])*rd/(1.0-gti*gd) + (y[4]-y[0])*rd*PineritIn + (y[0]-y[5])*rd*PineritIn*gd*(1.0-gti)/(1.0-gti*gd) - y[5]*rd*PinsLost  ]   #
		tapp = np.arange(0,1.5, 0.001)
		y = integ.odeint(func, y0, tapp)
		
		intervalB=0.1
		Nintervals=10
		Nreps=20
		for i2 in range(Nintervals):
			for j in range(Nreps):
				if runSims2:
					print("\n\n\n"+str(intervalB*(i2+1)))
					file=open("/geometricSim/control.txt","w")
					file.write("[TYPE] NUCLEOTIDE 1\n")
					file.write("[MODEL]    modelname\n")
					file.write("  [submodel]     JC  \n")
					file.write("  [indelmodel]   NB  0.75 1  \n")
					file.write("  [insertrate]   1.0  \n")
					file.write("  [deleterate]   1.0 \n")
					file.write("[TREE] treename  (A:0.0,B:"+str(intervalB*(i2+1))+"); \n")
					file.write("[PARTITIONS] partitionname   \n")
					file.write("  [treename modelname 100000]  \n")
					file.write("[EVOLVE] partitionname 1 outputname_indelible_"+str(intervalB*(i2+1))+"_rep"+str(j)+" \n")
					file.close()
	
					os.system("cd geometricSim/; "+"INDELibleV1.03/bin/indelible ")
		#for i2 in range(Nintervals):
			insL=[]
			deleL=[]
			histI=[]
			histD=[]
			for j in range(Nreps):
				file=open("/geometricSim/outputname_indelible_"+str(intervalB*(i2+1))+"_rep"+str(j)+"_TRUE.phy")
				line=file.readline()
				lenS=int(line.split()[1])
				line=file.readline()
				seq1=line.split()[1]
				line=file.readline()
				seq2=line.split()[1]
				ins=0
				dele=0
				i=0
				while seq1[i]=="-" or seq2[i]=="-":
					i+=1
				i+=1
				numM=1
				numID=0
				while i <lenS:
					if seq1[i]=="-" and seq2[i]!="-":
						ins+=1
					elif seq2[i]=="-" and seq1[i]!="-":
						dele+=1
					elif seq2[i]!="-" and seq1[i]!="-":
						if ins>0:
							insL.append(ins)
							l=len(histI)
							if ins>l:
								for k in range(ins-l):
									histI.append(0)
							histI[ins-1]+=1
						if dele>0:
							deleL.append(dele)
							if ins>0:
								numID+=1
							l=len(histD)
							if dele>l:
								for k in range(dele-l):
									histD.append(0)
							histD[dele-1]+=1
						numM+=1
						ins=0
						dele=0
					i+=1
			numI=len(insL)
			numD=len(deleL)
			
			
			theoryI=[]
			theoryD=[]
			index=int((intervalB*(i2+1))/0.001)
			g=1.0-float(y[index][0])/y[index][2]
			pow=numI*(1.0-g)
			for k in range(len(histI)):
				theoryI.append(pow)
				pow=pow*g
			g=1.0-float(y[index][1])/y[index][3]
			pow=numD*(1.0-g)
			for k in range(len(histD)):
				theoryD.append(pow)
				pow=pow*g
			geomI=[]
			g=1.0-numI/float(sum(insL))
			pow=numI*(1.0-g)
			for k in range(len(histI)):
				geomI.append(pow)
				pow=pow*g
			geomD=[]
			g=1.0-numD/float(sum(deleL))
			pow=numD*(1.0-g)
			for k in range(len(histD)):
				geomD.append(pow)
				pow=pow*g
			print sum(histI)
			print sum(theoryI)
			print sum(geomI)
			ave=0.0
			for k in range(len(histI)):
				ave+=histI[k]*(k+1)
			print ave/sum(histI)
			ave=0.0
			for k in range(len(geomI)):
				ave+=geomI[k]*(k+1)
			print ave/sum(geomI)
			ave=0.0
			for k in range(len(theoryI)):
				ave+=theoryI[k]*(k+1)
			print ave/sum(theoryI)
			print"\n\n"
			
			def norm(h):
				tot=0
				h2=[]
				for i in range(len(h)):
					tot+=h[i]
				for i in range(len(h)):
					h2.append(float(h[i])/tot)
				return h2
			
			maxVal=0
			while maxVal<len(histI) and histI[maxVal]>0:
				maxVal+=1
			#maxVal-=1
			maxVal=40
			
			histIp=norm(histI)
			theoryIp=norm(theoryI)
			geomIp=norm(geomI)
			yAxis=histI
			xAxis=[]
			for k in range(maxVal):
				xAxis.append(k+1)
			plt.plot(xAxis, histIp[:maxVal], label="simulated")
			plt.plot(xAxis, theoryIp[:maxVal], 'r--', label="HMM approximation")
			plt.plot(xAxis, geomIp[:maxVal], 'g--', label="geometric from simulated")
			plt.legend()
			plt.yscale('log')
			plt.savefig("/geometric_insertions_"+str(intervalB*(i2+1))+"_new.pdf")
			plt.close()
			
			maxVal=0
			while maxVal<len(histD) and histD[maxVal]>0:
				maxVal+=1
			maxVal=40
			
			histDp=norm(histD)
			theoryDp=norm(theoryD)
			geomDp=norm(geomD)
			yAxis=histD
			xAxis=[]
			for k in range(maxVal):
				xAxis.append(k+1)
			plt.plot(xAxis, histDp[:maxVal], label="simulated")
			plt.plot(xAxis, theoryDp[:maxVal], 'r--', label="HMM approximation")
			plt.plot(xAxis, geomDp[:maxVal], 'g--', label="geometric simulated")
			plt.legend()
			plt.yscale('log')
			plt.savefig("/geometric_deletions_"+str(intervalB*(i2+1))+"_new.pdf")
			plt.close()


		
		
	
	if runSims:
		for i in range(Nintervals):
			print("\n\n\n"+str(intervalB*(i+1)))
			file=open("/indelible_outputs/control.txt","w")
			file.write("[TYPE] NUCLEOTIDE 1\n")
			file.write("[MODEL]    modelname\n")
			file.write("  [submodel]     JC  \n")
			if TKF91case==1:
				file.write("  [indelmodel]   NB  0.000001 1  \n")
			else:
				file.write("  [indelmodel]   NB  0.75 1  \n")
			file.write("  [insertrate]   1.0  \n")
			file.write("  [deleterate]   1.0 \n")
			file.write("[TREE] treename  (A:0.0,B:"+str(intervalB*(i+1))+"); \n")
			file.write("[PARTITIONS] partitionname   \n")
			file.write("  [treename modelname "+str(genomesize)+"]  \n")
			if TKF91case==1:
				file.write("[EVOLVE] partitionname 1 outputname_indelible_TKF91_"+str(intervalB*(i+1))+" \n")
			else:
				file.write("[EVOLVE] partitionname 1 outputname_indelible_"+str(intervalB*(i+1))+" \n")
			file.close()
	
			os.system("cd /indelible_outputs/; "+"/INDELibleV1.03/bin/indelible ")
	#else:
		for i in range(Nintervals):
			if TKF91case==1:
				file=open("/indelible_outputs/outputname_indelible_TKF91_"+str(intervalB*(i+1))+"_TRUE.phy")
			else:
				file=open("/indelible_outputs/outputname_indelible_"+str(intervalB*(i+1))+"_TRUE.phy")
			line=file.readline()
			lenS=int(line.split()[1])
			line=file.readline()
			seq1=line.split()[1]
			line=file.readline()
			seq2=line.split()[1]
	
	
			ins=0
			dele=0
			indeles=0
			insL=[]
			deleL=[]
			chopL=[]
			#numM=0
			i=0
			while seq1[i]=="-" or seq2[i]=="-":
				i+=1
			i+=1
			numM=1
			numID=0
			while i <lenS:
				if seq1[i]=="-" and seq2[i]!="-":
					ins+=1
				elif seq2[i]=="-" and seq1[i]!="-":
					dele+=1
				elif seq2[i]!="-" and seq1[i]!="-":
					indeles=ins+dele
					if indeles>0:
						chopL.append(indeles)
					if ins>0:
						insL.append(ins)
					if dele>0:
						deleL.append(dele)
						if ins>0:
							numID+=1
					numM+=1
					ins=0
					dele=0
					indeles=0
				i+=1
			sumD=sum(deleL)
			sumI=sum(insL)
			numI=len(insL)
			numD=len(deleL)
			numChop=len(chopL)
			sumChop=sum(chopL)
			print numI
			print numD
			print sumI/float(numI)
			print sumD/float(numD)
			#print numChop
			#print sumChop/float(numChop)
			numsI.append(numI)
			numsD.append(numD)
			numsM.append(numM)
			meansI.append(sumI/float(numI))
			meansD.append(sumD/float(numD))
			numsID.append(numID)
			numsChop.append(numChop)
			varianceI.append(np.var(insL))
			varianceD.append(np.var(deleL))
			meanChop.append(sumChop/float(numChop))
			varianceChop.append(np.var(chopL))
			medianChop.append(np.median(chopL))
			medianI.append(np.median(insL))
			medianD.append(np.median(deleL))

		#intervalB=0.01
		#Nintervals=10
		xAxis=[]
		for i in range(Nintervals):
			xAxis.append(intervalB*(i+1))
		
		if TKF91case==1:
			file=open('insertionMeans_TKF91.txt',"w")
		else:
			file=open('insertionMeans.txt',"w")
		for i in range(Nintervals):
			file.write(str(xAxis[i])+" "+str(meansI[i])+"\n")
		file.close()
		
		if TKF91case==1:
			file=open('deletionMeans_TKF91.txt',"w")
		else:
			file=open('deletionMeans.txt',"w")
		for i in range(Nintervals):
			file.write(str(xAxis[i])+" "+str(meansD[i])+"\n")
		file.close()
		
		if TKF91case==1:
			file=open('indelNumbers_TKF91.txt',"w")
		else:
			file=open('indelNumbers.txt',"w")
		file.write("Blength numMatches numIns numDels numInDel numChop meanI meanD meanChop medianI medianD medianChop varianceI varianceD varianceChop\n")
		for i in range(Nintervals):
			file.write(str(xAxis[i])+" "+str(numsM[i])+" "+str(numsI[i])+" "+str(numsD[i])+" "+str(numsID[i])+" "+str(numsChop[i])+" "+str(meansI[i])+" "+str(meansD[i])+" "+str(meanChop[i])+" "+str(medianI[i])+" "+str(medianD[i])+" "+str(medianChop[i])+" "+str(varianceI[i])+" "+str(varianceD[i])+" "+str(varianceChop[i])+"\n")
		file.close()

	else:
		numsI=[]
		numsD=[]
		numsM=[]
		meansI=[]
		meansD=[]
		numsID=[]
		
		#new statistics
		numsChop=[]
		varianceI=[]
		varianceD=[]
		meanChop=[]
		varianceChop=[]
		medianChop=[]
		medianI=[]
		medianD=[]

		xAxis=[]
		for i in range(Nintervals):
			xAxis.append(intervalB*(i+1))
		
		if TKF91case==1:
			file=open('indelNumbers_TKF91.txt')
		else:
			file=open('indelNumbers.txt')
		line=file.readline().split()
		#file.write("Blength numMatches numIns numDels\n")
		for i in range(Nintervals):
			line=file.readline().split()
			#Blength numMatches numIns numDels numInDel numChop meanI meanD meanChop medianI medianD medianChop varianceI varianceD varianceChop
			numsM.append(float(line[1]))
			numsI.append(float(line[2]))
			numsD.append(float(line[3]))
			numsID.append(float(line[4]))
			numsChop.append(float(line[5]))
			meansI.append(float(line[6]))
			meansD.append(float(line[7]))
			meanChop.append(float(line[8]))
			medianI.append(float(line[9]))
			medianD.append(float(line[10]))
			medianChop.append(float(line[11]))
			varianceI.append(float(line[12]))
			varianceD.append(float(line[13]))
			varianceChop.append(float(line[14]))
			#file.write(str(xAxis[i])+" "+str(numsM[i])+" "+str(numsI[i])+" "+str(numsD[i])+"\n")
		file.close()

	
	print "numsI:"
	print numsI
	print "numsD:"
	print numsD
	print "numsM:"
	print numsM
	print "meansI"
	print meansI
	print "meansD"
	print meansD
	print "numsID:"
	print numsID
	print("numsChop:")
	print(numsChop)
	print("meanChop:")
	print(meanChop)
	print("medianI:")
	print(medianI)
	print("medianD:")
	print(medianD)
	print("medianChop:")
	print(medianChop)
	print("varianceI:")
	print(varianceI)
	print("varianceD:")
	print(varianceD)
	print("varianceChop:")
	print(varianceChop)
	
	#exit()
	
	gi=0.75
	gd=0.75
	if TKF91case==1:
		gi=0.0
		gd=0.0
	ri=1.0
	rd=1.0
	
	
	if not newPlotsOnly:
		nTimes=1
		start=time.time()
		for j in range(nTimes):
			#approximations
			y0=[0.0,0.0,0.0,0.0,500000,0.0]
			#My model
			def func(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] surviving homology columns followed by deletions, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
				dLen=rd/(1.0-gd)
				if y[0]<0.000000000001:
					gti=gi
				else:
					gti=1.0-y[0]/y[2]
				PineritIn=(y[0]/y[4])*(1.0-gd)/((1.0-(1.0-y[0]/y[4])*gd)*(1.0-gd*gti))
				PinsLost=(1.0-gti)*(1.0-gd)/((1.0-gd*(1.0-y[0]/y[4]))*(1.0-gti*gd))
				return [  (y[4]-y[0])*ri - y[0]*dLen + (y[4]-y[0])*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + rd*(y[0]-y[5])*gd/(1.0-gd*gti)      ,    (y[4]+y[2])*ri/(1.0-gi) - y[2]*dLen     ,    y[4]*dLen       ,    -y[4]*dLen     ,      (y[1]-y[5])*ri - y[5]*dLen + (y[0]-y[5])*rd/(1.0-gti*gd) + (y[4]-y[0])*rd*PineritIn + (y[0]-y[5])*rd*PineritIn*gd*(1.0-gti)/(1.0-gti*gd) - y[5]*rd*PinsLost  ]   #
			tapp = np.arange(0,1.5, 0.001)
			y = integ.odeint(func, y0, tapp)
		elapsedTime = time.time() - start
		print(elapsedTime)
	
		start=time.time()
		for j in range(nTimes):
			#ADD TO TEXT AND SCRIPT!
			ngd=(1.0-gd)
			ngi=1.0-gi
			y02=[0.0,0.0,0.0,0.0,1.0]
			dLen=rd/(1.0-gd)
			iLen=ri/ngi
			alpha=-rd/ngd
			def func2(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] surviving homology columns followed by deletions, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
				if y[0]<0.000000000001:
					gti=gi
					propPi=1.0
				else:
					propPi=(y[0]-y[3])/y[0]
					gti=1.0-y[0]/y[2]
				nPiUp=y[4]-y[0]
				denom2=1.0/(1.0-gti*gd)
				Pi=y[0]/y[4]
				nPi=1.0-Pi
			
				denom=denom2/(1.0-gd*nPi)
				PineritIn=Pi*ngd*denom
				PinsLost=(1.0-gti)*ngd*denom #+ propPi*y[2]*(1.0-gti)*rd*gd*denom2 
				return [  nPiUp*ri - y[0]*dLen + nPiUp*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + propPi*y[2]*(1.0-gti)*rd*gd*denom2      ,    (y[4]+y[2])*iLen - y[2]*dLen     ,    (y[1]-y[3])*ri - y[3]*dLen + (y[0]-y[3])*rd*denom2 + nPiUp*rd*PineritIn - y[3]*rd*PinsLost + (y[0]-y[3])*rd*PineritIn*gd*(1.0-gti)*denom2      ,    y[4]*alpha       ]   #
			y2 = integ.odeint(func2, y02, tapp)
		elapsedTime = time.time() - start
		print(elapsedTime)
	
	
		y03=[0.0,0.0,0.0,0.0,1.0]
		ngd=(1.0-gd)
		ngi=1.0-gi
		alpha=-rd/ngd
		dLen=rd/ngd
		iLen=ri/ngi
		def func3(y,t): #y[0] is P^t_i, y[1] P^t_d, y[2] L^t_i, y[3] P^t_{id} y[4] P^t_m
			if y[0]<0.000000000001:
				gti=gi
			else:
				gti=1.0-y[0]/y[2]
			nPiUp=y[4]-y[0]
			Pi=y[0]/y[4]
			nPi=1.0-Pi
			denom2=1.0/(1.0-gti*gd)
			denom=denom2/(1.0-gd*nPi)
			PineritIn=Pi*ngd*denom
			PinsLost=(1.0-gti)*ngd*denom
			return [  nPiUp*ri - y[0]*dLen + nPiUp*rd*PineritIn - y[0]*rd*PinsLost   ,   (y[4]-y[1])*rd - y[1]*dLen + (y[0]-y[3])*rd*gd*denom2   ,   (y[4]+y[2])*iLen - y[2]*dLen   ,     (y[1]-y[3])*ri - y[3]*dLen + (y[0]-y[3])*rd*denom2 + nPiUp*rd*PineritIn - y[3]*rd*(1.0-gti)*denom2 + y[0]*rd*PineritIn*gd*(1.0-gti)*denom2        , alpha*y[4] ]
			#return [      ,         ,         ,           ,        ,      (y[1]-y[5])*ri - y[5]*dLen + (y[0]-y[5])*rd/(1.0-gti*gd) + (y[4]-y[0])*rd*PineritIn + (y[0]-y[5])*rd*PineritIn*gd*(1.0-gti)/(1.0-gti*gd) - y[5]*rd*PinsLost  ]   #
		y3 = integ.odeint(func3, y03, tapp)
		step=1400
		print [y[step][0]/y[step][4],y[step][1]/y[step][4],y[step][2]/y0[4],y[step][3]/y0[4],y[step][4]/y0[4], y[step][5]/y[step][4]]
		print [y2[step][0]/y2[step][4],y2[step][1]/y2[step][4],y2[step][2],1.0-y2[step][4],y2[step][4],y2[step][3]/y2[step][4]]
		print [y3[step][0]/y3[step][4],y3[step][1]/y3[step][4],y3[step][2],1.0-y3[step][4],y3[step][4],y3[step][3]/y3[step][4]]
		y=y2

		#cumulative indel zoomed in near 0
		start=time.time()
		for j in range(nTimes):
			#ADD TO TEXT AND SCRIPT!
			ngd=(1.0-gd)
			ngi=1.0-gi
			y02=[0.0,0.0,0.0,0.0,1.0]
			dLen=rd/(1.0-gd)
			iLen=ri/ngi
			alpha=-rd/ngd
			def func2(y,t): #y[0] is number of insertion iserts, y[1] deletion iserts, y[2] total surviving inserted bases, y[3] surviving homology columns followed by deletions, y[4] homology columns surviving, y[5] homology columns followed by both insertion insert and deletion indel
				if y[0]<0.000000000001:
					gti=gi
					propPi=1.0
				else:
					propPi=(y[0]-y[3])/y[0]
					gti=1.0-y[0]/y[2]
				nPiUp=y[4]-y[0]
				denom2=1.0/(1.0-gti*gd)
				Pi=y[0]/y[4]
				nPi=1.0-Pi
			
				denom=denom2/(1.0-gd*nPi)
				PineritIn=Pi*ngd*denom
				PinsLost=(1.0-gti)*ngd*denom #+ propPi*y[2]*(1.0-gti)*rd*gd*denom2 
				return [  nPiUp*ri - y[0]*dLen + nPiUp*rd*PineritIn - y[0]*rd*PinsLost    ,   (y[4]-y[1])*rd - y[1]*dLen + propPi*y[2]*(1.0-gti)*rd*gd*denom2      ,    (y[4]+y[2])*iLen - y[2]*dLen     ,    (y[1]-y[3])*ri - y[3]*dLen + (y[0]-y[3])*rd*denom2 + nPiUp*rd*PineritIn - y[3]*rd*PinsLost + (y[0]-y[3])*rd*PineritIn*gd*(1.0-gti)*denom2      ,    y[4]*alpha       ]   #
			tapp9 = np.arange(0,0.0001, 0.000001)
			y2 = integ.odeint(func2, y02, tapp9)
		elapsedTime = time.time() - start
		print(elapsedTime)
	
		gi2=0.75894506
		gd2=0.75894506
		ri2=0.06566545
		rd2=0.06566545
		if onlyTable:
			rd=-1000
			ri=-1000
			gi=-1000
			gd=-1000
		start=time.time()
		tapp7 = np.arange(0,0.587804*2, 0.000001)
		for j in range(nTimes):
			ngd=(1.0-gd2)
			ngi=1.0-gi2
			y07=[0.0,0.0,0.0,0.0,1.0]
			dLen=rd2/(1.0-gd2)
			iLen=ri2/ngi
			alpha=-rd2/ngd
			def func7(y,t): #y[0] P^t_i, y[1] P^t_d, y[2] L^t_i, y[3] P^t_{id}, y[4] P^t_m .
				if y[0]<0.000000000001:
					gti=gi2
					propPi=1.0
				else:
					propPi=(y[0]-y[3])/y[0]
					gti=1.0-y[0]/y[2]
				nPiUp=y[4]-y[0]
				denom2=1.0/(1.0-gti*gd2)
				Pi=y[0]/y[4]
				nPi=1.0-Pi
			
				denom=denom2/(1.0-gd2*nPi)
				PineritIn=Pi*ngd*denom
				PinsLost=(1.0-gti)*ngd*denom #+ propPi*y[2]*(1.0-gti)*rd*gd*denom2 
				return [  nPiUp*ri2 - y[0]*dLen + nPiUp*rd2*PineritIn - y[0]*rd2*PinsLost    ,   (y[4]-y[1])*rd2 - y[1]*dLen + propPi*y[2]*(1.0-gti)*rd2*gd2*denom2      ,    (y[4]+y[2])*iLen - y[2]*dLen     ,    (y[1]-y[3])*ri2 - y[3]*dLen + (y[0]-y[3])*rd2*denom2 + nPiUp*rd2*PineritIn - y[3]*rd2*PinsLost + (y[0]-y[3])*rd2*PineritIn*gd2*(1.0-gti)*denom2      ,    y[4]*alpha       ]   #
			y7 = integ.odeint(func7, y07, tapp7)
		elapsedTime = time.time() - start
		print(elapsedTime)
		print(y7)
		#t, ri, gi
		#[0.01175608, 0.06566545, 0.75894506]
		#ptm, ati, pti, atd, ptd, Lti, gti, gtd, atid, ptid
		#[0.9968026862777203, 0.0007697286183390363, 0.0007721975762458785, 0.0007697289115293807, 0.0007721978703766511, 0.00319731372227959, 0.7592577128182959, 0.7592576211193449, 0.9968026862777203, 1.7012512296812903e-06]
		if onlyTable:
			print("t, ri, gi")
			print([0.587804*2,0.06566545,0.75894506])
			print("ptm, ati, pti, atd, ptd, Lti, gti, gtd, atid, ptid")
			print([y7[-1][4],y7[-1][0],y7[-1][0]/y7[-1][4],y7[-1][1],y7[-1][1]/y7[-1][4],y7[-1][2],1.0-y7[-1][0]/y7[-1][2],1.0-y7[-1][1]/(1.0-y7[-1][4]),y7[-1][3],y7[-1][3]/y7[-1][4]])
			exit()
	
	
		#TKF91
		lam=ri
		mu=rd
		TKF91=[]
		for i in range(int(1.5/0.001)):	
			t=i*0.001
			if lam==mu:
				btM=lam*t/(1.0+lam*t)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*t))/(mu-lam*math.exp((lam-mu)*t))
			gtM=1.0-math.exp(-mu*t)
			TKF91.append((1.0-btM)*(1.0-gtM))

		#TKF92, return the probability that no insertion or deletion happens
		TKF92=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			if lam==mu:
				btM=lam*t/(1.0+lam*t)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*t))/(mu-lam*math.exp((lam-mu)*t))
			gtM=1.0-math.exp(-mu*t)
			TKF92.append((1.0-btM)*(1.0-gtM)*(1.0-gi)+gi)
	
		#TKF92 re-parameterized so that the expected number of indels at t almost 0 is correct
		TKF92b=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			tb=t/(1.0-gi)
			if lam==mu:
				btM=lam*tb/(1.0+lam*tb)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*tb))/(mu-lam*math.exp((lam-mu)*tb))
			gtM=1.0-math.exp(-mu*tb)
			TKF92b.append((1.0-btM)*(1.0-gtM)*(1.0-gi)+gi)

		#BAli-Phy model
		BAli=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta1=1.0-math.exp(-lam*t/(1.0-gi))
			delta=delta1/(1.0+delta1)
			BAli.append(gi+(1.0-gi)*(1.0-2*delta))
		
		#PRANK
		PRANK=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta=1.0-math.exp(-ri*t)
			PRANK.append(gi+(1.0-gi)*(1.0-2*delta))
		
		#PRANK with corrected indel rate
		PRANKb=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta=1.0-math.exp(-ri*t/(1.0-gi))
			if (0.0<(1.0-2*delta)) and ((1.0-2*delta)<1.0):
				PRANKb.append(gi+(1.0-gi)*(1.0-2*delta))
			else:
				PRANKb.append(float("NaN"))
		
		#PRANK with corrected indel rate and gamma=0
		PRANKc=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			gamma=0.0
			delta=1.0-math.exp(-ri*t/(1.0-gamma))
			if (0.0<(1.0-2*delta)) and ((1.0-2*delta)<1.0):
				PRANKc.append(gamma+(1.0-gamma)*(1.0-2*delta))
			else:
				PRANKc.append(float("NaN"))
		
		TKF91id=[]
		for i in range(int(1.5/0.001)):	
			t=i*0.001
			#if lam==mu:
			btM=lam*t/(1.0+lam*t)
			#else:
			#	btM=lam*(1.0-math.exp((lam-mu)*t))/(mu-lam*math.exp((lam-mu)*t))
			gtM=1.0-math.exp(-mu*t)
			if t<0.000000001:
				btD=1.0/(1.0+lam*t)
			else:
				btD=1.0-btM/gtM
			DI=btD/(btD+(1.0-btD)*(1.0-gtM))
			ID=(1.0-btM)*gtM/(1.0-btM)
			sum1=1.0/(1.0-DI*ID)
			sum2=2*DI*ID/(1.0-DI*ID)
			if t<0.000000001:
				div=0.5
			else:
				div=((1.0-btM)*gtM + btM)
			MD=(1.0-btM)*gtM/div
			MI=btM/div
			DM=(1.0-btD)*(1.0-gtM)/(btD+(1.0-btD)*(1.0-gtM))
			IM=(1.0-btM)*(1.0-gtM)/(1.0-btM)
			len1=1.0/(1.0-btM)
			tot=(len1*MI*((DM*ID+IM)*sum2 + (IM+2.0*DM*ID)*sum1) + len1*MD*((IM*DI+DM)*sum2 + (DM+2.0*IM*DI)*sum1))
			if i>0.000000001:
				TKF91id.append(tot)
			
		#TKF92, return the expected length of a continuous series of indels
		TKF92id=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			if lam==mu:
				btM=lam*t/(1.0+lam*t)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*t))/(mu-lam*math.exp((lam-mu)*t))
			gtM=1.0-math.exp(-mu*t)
			if t<0.000000001:
				btD=1.0/(1.0+lam*t)
			else:
				btD=1.0-btM/gtM
			DI=btD/(btD+(1.0-btD)*(1.0-gtM))
			ID=(1.0-btM)*gtM/(1.0-btM)
			sum1=1.0/(1.0-DI*ID)
			sum2=2*DI*ID/(1.0-DI*ID)
			if t<0.000000001:
				div=0.5
			else:
				div=((1.0-btM)*gtM + btM)
			MD=(1.0-btM)*gtM/div
			MI=btM/div
			DM=(1.0-btD)*(1.0-gtM)/(btD+(1.0-btD)*(1.0-gtM))
			IM=(1.0-btM)*(1.0-gtM)/(1.0-btM)
			len1=1.0/(1.0-(gi+(1.0-gi)*btM))
			tot=(len1*MI*((DM*ID+IM)*sum2 + (IM+2.0*DM*ID)*sum1) + len1*MD*((IM*DI+DM)*sum2 + (DM+2.0*IM*DI)*sum1))
			if i>0.000000001:
				TKF92id.append(tot)
			
		#TKF92, return the expected length of a continuous series of indels, now with rate corrected
		TKF92idb=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			tb=t/(1.0-gi)
			if lam==mu:
				btM=lam*tb/(1.0+lam*tb)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*tb))/(mu-lam*math.exp((lam-mu)*tb))
			gtM=1.0-math.exp(-mu*tb)
			if t<0.000000001:
				#btD=1.0/(1.0+lam*tb)
				btD=0.0
			else:
				btD=1.0-btM/gtM
			DI=btD/(btD+(1.0-btD)*(1.0-gtM))
			ID=(1.0-btM)*gtM/(1.0-btM)
			sum1=1.0/(1.0-DI*ID)
			sum2=2*DI*ID/(1.0-DI*ID)
			if t<0.000000001:
				div=0.5
			else:
				div=((1.0-btM)*gtM + btM)
			MD=(1.0-btM)*gtM/div
			MI=btM/div
			DM=(1.0-btD)*(1.0-gtM)/(btD+(1.0-btD)*(1.0-gtM))
			IM=(1.0-btM)*(1.0-gtM)/(1.0-btM)
			len1=1.0/(1.0-(gi+(1.0-gi)*btM))
			tot=(len1*MI*((DM*ID+IM)*sum2 + (IM+2.0*DM*ID)*sum1) + len1*MD*((IM*DI+DM)*sum2 + (DM+2.0*IM*DI)*sum1))
			if i>0.000000001:
				TKF92idb.append(tot)
			
		#BAli-Phy model
		BAliid=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta1=1.0-math.exp(-lam*t/(1.0-gi))
			delta=delta1/(1.0+delta1)
			BAliid.append(1.0/((1.0-gi)*(1.0-2*delta)))

		#PRANK
		PRANKid=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta=1.0-math.exp(-ri*t)
			if i>0.000000001:
				if ((1.0-gi)*(1.0-2*delta))<0.0 or ((1.0-gi)*(1.0-2*delta))>1.0:
					PRANKid.append(float("NaN"))
				else:
					PRANKid.append(1.0/((1.0-gi)*(1.0-2*delta)))
	
		#PRANK with corrected rate
		PRANKidb=[]
		for i in range(int(1.5/0.001)):
			t=i*0.001
			delta=1.0-math.exp(-ri*t/(1.0-gi))
			if i>0.000000001:
				if ((1.0-gi)*(1.0-2*delta))<0.0 or ((1.0-gi)*(1.0-2*delta))>1.0:
					PRANKidb.append(float("NaN"))
				else:
					PRANKidb.append(1.0/((1.0-gi)*(1.0-2*delta)))
	
	
		#TKF92, return the expected length of a continuous series of indels, now with rate corrected - zoomed at 0
		TKF92idb2=[]
		for i in range(100):
			t=(i+1)*0.000001
			tb=t/(1.0-gi)
			if lam==mu:
				btM=lam*tb/(1.0+lam*tb)
			else:
				btM=lam*(1.0-math.exp((lam-mu)*tb))/(mu-lam*math.exp((lam-mu)*tb))
			gtM=1.0-math.exp(-mu*tb)
			if t<0.000000001:
				#btD=1.0/(1.0+lam*tb)
				btD=0.0
			else:
				btD=1.0-btM/gtM
			DI=btD/(btD+(1.0-btD)*(1.0-gtM))
			ID=(1.0-btM)*gtM/(1.0-btM)
			sum1=1.0/(1.0-DI*ID)
			sum2=2*DI*ID/(1.0-DI*ID)
			if t<0.000000001:
				div=0.5
			else:
				div=((1.0-btM)*gtM + btM)
			MD=(1.0-btM)*gtM/div
			MI=btM/div
			DM=(1.0-btD)*(1.0-gtM)/(btD+(1.0-btD)*(1.0-gtM))
			IM=(1.0-btM)*(1.0-gtM)/(1.0-btM)
			len1=1.0/(1.0-(gi+(1.0-gi)*btM))
			tot=(len1*MI*((DM*ID+IM)*sum2 + (IM+2.0*DM*ID)*sum1) + len1*MD*((IM*DI+DM)*sum2 + (DM+2.0*IM*DI)*sum1))
			if t>0.000000001:
				TKF92idb2.append(tot)
	
		#BAli-Phy model
		BAliid2=[]
		for i in range(100):
			t=(i+1)*0.000001
			delta1=1.0-math.exp(-lam*t/(1.0-gi))
			delta=delta1/(1.0+delta1)
			BAliid2.append(1.0/((1.0-gi)*(1.0-2*delta)))
	
		#PRANK with corrected rate
		PRANKidb2=[]
		for i in range(100):
			t=(i+1)*0.000001
			delta=1.0-math.exp(-ri*t/(1.0-gi))
			if t>0.000000001:
				if ((1.0-gi)*(1.0-2*delta))<0.0 or ((1.0-gi)*(1.0-2*delta))>1.0:
					PRANKidb2.append(float("NaN"))
				else:
					PRANKidb2.append(1.0/((1.0-gi)*(1.0-2*delta)))
	
	
	
	
	
	
	
	

	
	import pairHMM_parameters
	
	def simulatePath(transProbs):
		#MID
		steps=genomesize
		#path=np.zeros(steps, dtype=int)
		state=0
		indel=0
		ins=0
		dels=0
		numM=1
		Ilens=[]
		Dlens=[]
		IDlens=[]
		chopLens=[]
		for i in range(steps):
			#probs=transProbs[state]
			newState=np.random.choice(3,p=transProbs[state])
			if newState==0:
				if ins>0:
					Ilens.append(ins)
					if dels>0:
						Dlens.append(dels)
						IDlens.append(ins+dels)
				elif dels>0:
					Dlens.append(dels)
				if ins+dels>0:
					chopLens.append(ins+dels)
				ins=0
				dels=0
				numM+=1
			elif newState==1:
				ins+=1
			elif newState==2:
				dels+=1
			state=newState
			#path[i]=state
		#print(path[:100])
		return Ilens, Dlens, chopLens, numM, IDlens #path
	
	
	#new way to calculate things by simulating under each pairHMM
	models=['TKF91','TKF92','RS07','PRANK','PRANK0','cumIndels']
	if TKF91case==1:
		models=['TKF91','RS07','PRANK','cumIndels']
	Ilens=[]
	Dlens=[]
	IDlens=[]
	chopLens=[]
	numM=[]
	print("simulating pairHMM paths")
	for interval in range(Nintervals):
		print("interval "+str(interval))
		Ilens.append([])
		Dlens.append([])
		IDlens.append([])
		chopLens.append([])
		numM.append([])
		t1=intervalB*(interval+1)
		for mod in models:
			print(mod)
			#M I D
			#MM MI MD IM II ID DM DI DD
			transProbs, stateProbs, probs1 = pairHMM_parameters.getAllProbs3(t1,ri,rd,gi,gd, "JC", [0.25,0.25,0.25,0.25], 1.0, [0.2,0.4,0.2,0.2,0.4,0.2], True, mod)
			print(transProbs)
			if len(transProbs)==0:
				Il=[]
				Dl=[]
				IDl=[]
				cL=[]
				nM=[]
			else:
				Il, Dl, cL, nM, IDl=simulatePath(transProbs)
			Ilens[interval].append(Il)
			Dlens[interval].append(Dl)
			IDlens[interval].append(IDl)
			chopLens[interval].append(cL)
			numM[interval].append(nM)
			#exit()

	
	
	

	
	
	
	

	if reDoPlots:
		
		if not newPlotsOnly:
			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(numsID[i]/float(numsM[i]))
			plt.semilogy(xAxis, yAxis)

			iM=[]
			for i in range(len(tapp)):
				iM.append(y[i][3]/y[i][4])

			plt.semilogy(tapp, iM, 'r--')
			if TKF91case==1:
				plt.savefig('TKF91_numID.pdf')
			else:
				plt.savefig('numID.pdf')
			plt.close()
		
		
			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(numsID[i]/float(numsM[i]))
			plt.semilogy(xAxis, yAxis)
			iM2=[]
			for i in range(len(tapp)):
				iM2.append(y[i][0]*y[i][1]/(float(y[i][4])*float(y[i][4])))
			plt.semilogy(tapp, iM2, 'g--')
			iM=[]
			for i in range(len(tapp)):
				iM.append(y[i][3]/y[i][4])
			plt.semilogy(tapp, iM, 'r--')
			if TKF91case==1:
				plt.savefig('TKF91_numID_plus.pdf')
			else:
				plt.savefig('numID_plus.pdf')
			plt.close()
		
			ratios=[]
			for i in range(len(tapp7)):
				if i>0:
					ratios.append((y7[i][3]*float(y7[i][4]))/(float(y7[i][0])*y7[i][1]))
			plt.plot(tapp7[:-1], ratios)
			print(ratios)
			print(max(ratios))
			plt.savefig('numID_ratio.pdf')
			plt.close()



			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(numsM[i]/float(y0[4]))
			plt.semilogy(xAxis, yAxis)
			#t = np.arange(0., 1.5, 0.001)
			iM=[]
			for i in range(len(tapp)):
				iM.append(y[i][4])

			plt.semilogy(tapp, iM, 'r--')
			if TKF91case==1:
				plt.savefig('TKF91_numM.pdf')
			else:
				plt.savefig('numM.pdf')
			plt.close()

	
	
			#plt.scatter(xAxis,meansI)
			plt.semilogy(xAxis, meansI)
			t = np.arange(0., 1.5, 0.001)
			iM=[1.0/(1.0-gi)]
			for i in range(len(tapp)-1):
				iM.append(y[i+1][2]/y[i+1][0])
			#print iM
			plt.semilogy(tapp, iM, 'r--')

			if TKF91case==1:
				plt.savefig('TKF91_insertionMeans.pdf')
			else:
				plt.savefig('insertionMeans.pdf')
			#plt.show()
			plt.close()

			#plt.scatter(xAxis,meansD)
			plt.semilogy(xAxis, meansD)
			t = np.arange(0., 1.5, 0.001)
			iM=[1.0/(1.0-gd)]
			for i in range(len(tapp)-1):
				iM.append((1.0-y[i+1][4])/y[i+1][1])
			#print iM
			plt.semilogy(tapp, iM, 'r--')

			if TKF91case==1:
				plt.savefig('TKF91_deletionMeans.pdf')
			else:
				plt.savefig('deletionMeans.pdf')
			#plt.show()
			plt.close()

			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(numsI[i]/float(numsM[i]))
			#print yAxis
			plt.plot(xAxis,yAxis, 'b--')

			iM=[]
			for i in range(len(tapp)):
				iM.append(y[i][0]/y[i][4])
			#print iM
			plt.plot(tapp, iM, 'r--')

			if TKF91case==1:
				plt.savefig('TKF91_insertionProportions.pdf')
			else:
				plt.savefig('insertionProportions.pdf')
			plt.close()

			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(numsD[i]/float(numsM[i]))

			plt.plot(xAxis,yAxis, 'b--')

			iM=[]
			for i in range(len(tapp)):
				iM.append(y[i][1]/y[i][4])
			#print iM
			plt.plot(tapp, iM, 'r--')

			if TKF91case==1:
				plt.savefig('TKF91_deletionProportions.pdf')
			else:
				plt.savefig('deletionProportions.pdf')
			plt.close()
	
			yAxis=[]
			for i in range(Nintervals):
				yAxis.append((numsM[i]-(numsI[i]+numsI[i])+numsID[i])/float(numsM[i]))
			#print yAxis
			#plt.scatter(xAxis,yAxis)
			plt.plot(xAxis, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6)
			t = np.arange(0., 1.5, 0.001)
			iM=[]
			for i in range(len(tapp)):
				iM.append((y[i][4]-(y[i][0]+y[i][1])+y[i][3])/y[i][4])
			#print iM
			plt.plot(tapp, iM, '--', color="green")
			plt.plot(tapp, TKF91, '--', color="blue")
			#plt.plot(tapp, TKF92, 'm--')
			plt.plot(tapp, TKF92b, '--', color="orange")
			plt.plot(tapp, BAli, '--', color="purple")
			#plt.plot(tapp, PRANK, 'y--')
			plt.plot(tapp, PRANKb, '--', color="black")
			if TKF91case==1:
				plt.savefig('TKF91_noIndelProportions.pdf')
			else:
				plt.savefig('noIndelProportions_new.pdf')
			plt.close()
		
		
			yAxis=[]
			for i in range(Nintervals):
				yAxis.append((numsM[i]-(numsI[i]+numsI[i])+numsID[i])/float(numsM[i]))
			#print yAxis
			#plt.scatter(xAxis,yAxis)
			plt.plot(xAxis, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6)
			t = np.arange(0., 1.5, 0.001)
			iM=[]
			for i in range(len(tapp)):
				iM.append((y[i][4]-(y[i][0]+y[i][1])+y[i][3])/y[i][4])
			#print iM
			plt.plot(tapp, iM, '--', label="CumIndel", color="green")
			plt.plot(tapp, TKF91, '--',label="TKF91", color="blue")
			#plt.plot(tapp, TKF92, 'm--')
			plt.plot(tapp, TKF92b, '--',label="TKF92", color="orange")
			plt.plot(tapp, BAli, '--',label="RS07", color="purple")
			#plt.plot(tapp, PRANK, 'y--')
			plt.plot(tapp, PRANKb, '--',label="PRANK_gamma=eps", color="black")
			plt.plot(tapp, PRANKc, '--',label="PRANK_gamma=0", color="red")
			plt.legend()
			if TKF91case==1:
				plt.savefig('TKF91_noIndelProportions_morePrank.pdf')
			else:
				plt.savefig('noIndelProportions_morePrank.pdf')
			plt.close()
		
		

		
	
	
			yAxis=[]
			for i in range(Nintervals):
				yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			print len(yAxis)
			print yAxis[0]
			print yAxis[-1]
			#plt.scatter(xAxis,yAxis)
			#plt.semilogy(xAxis, yAxis, label="simulated")
			xAxis2=[]
			yAxis2=[]
			for i in range(len(yAxis)/1):
				xAxis2.append(xAxis[i*1])
				yAxis2.append(yAxis[i*1])
			plt.semilogy(xAxis2, yAxis2, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
			t = np.arange(0., 1.5, 0.001)
			idM=[]
			for j in range(len(tapp)-1):
				i=j+1
				if i!=0:
					idM.append((y[i][2]+(1.0-y[i][4]))/float(y[i][0]+y[i][1]-y[i][3]))
				else:
					idM.append(0.0)
			tapp2=tapp[1:]
			plt.semilogy(tapp2, idM, '--', label="CumIndel", color="green")
			plt.semilogy(tapp2, TKF91id, '--',label="TKF91", color="blue")
			#plt.semilogy(tapp2, TKF92id, 'm--',label="TKF92")
			plt.semilogy(tapp2, TKF92idb, '--',label="TKF92", color="orange")
			BAliid=BAliid[:-1]
			plt.semilogy(tapp2, BAliid, '--',label="RS07", color="purple")
			#plt.semilogy(tapp2, PRANKid, 'y--',label="PRANK")
			plt.semilogy(tapp2, PRANKidb, '--',label="PRANK", color="black")
			plt.legend()
			if TKF91case==1:
				plt.savefig('TKF91_indelMeanLength_new.pdf')
			else:
				plt.savefig('indelMeanLength_new.pdf')
			plt.close()
		
			plt.semilogy(xAxis2, yAxis2, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
			plt.semilogy(tapp2, idM, '--', label="CumIndel", color="green")
			plt.semilogy(tapp2, TKF91id, '--',label="TKF91", color="blue")
			plt.semilogy(tapp2, TKF92idb, '--',label="TKF92", color="orange")
			plt.semilogy(tapp2, BAliid, '--',label="RS07", color="purple")
			plt.semilogy(tapp2, PRANKidb, '--',label="PRANK", color="black")
			if TKF91case==1:
				plt.savefig('TKF91_indelMeanLength_noLegend.pdf')
			else:
				plt.savefig('indelMeanLength_noLegend.pdf')
			plt.close()
		
		
		
			factor=1
			yAxis=[]
			t = np.arange(0., 0.0001/factor, 0.000001)
			idM=[]
			for j in range(100):
				i=j+1
				#if i!=0:
				idM.append((y2[i][2]+(1.0-y2[i][4]))/float(y2[i][0]+y2[i][1]-y2[i][3]))
				#else:
				#	idM.append(0.0)
			numPoi=(len(tapp9))-1
			tapp2=tapp9[1:len(tapp9)]
			print(len(tapp9))
			print(len(tapp2))
			print(tapp9[0])
			print(tapp2[0])
			print(len(idM))
			print(len(TKF92idb2))
			print(len(BAliid2))
			print(len(PRANKidb2))
			plt.plot(tapp2, idM[:numPoi], '--', label="CumIndel", color="green")
			#plt.semilogy(tapp2, TKF91id[:numPoi], '--',label="TKF91", color="blue")
			#plt.semilogy(tapp2, TKF92id, 'm--',label="TKF92")
			plt.plot(tapp2, TKF92idb2[:numPoi], '--',label="TKF92", color="orange")
			BAliid=BAliid[:-1]
			plt.plot(tapp2, BAliid2[:numPoi], '-.',label="RS07", color="purple")
			#plt.semilogy(tapp2, PRANKid, 'y--',label="PRANK")
			plt.plot(tapp2, PRANKidb2[:numPoi], '--',label="PRANK", color="black")
			plt.legend()
			plt.savefig('indelMeanLength_focus.pdf')
			plt.close()
		
		
		
		
		
		#NEW PLOTS WITH MEDIAN AND VARIANCE, SIMULATING FROM pairHMM transition probabilities
		
		#models=['TKF91','TKF92','RS07','PRANK','PRANK0','cumIndels']
		#Ilens[interval].append(Il)
		#Dlens[interval].append(Dl)
		#chopLens[interval].append(cL)
		#IDlens
		#numM
		colors=["blue","orange","purple","black","grey","green"]
		if TKF91case==1:
			#models=['TKF91','RS07','PRANK','cumIndels']
			colors=["blue","purple","black","green"]
		
		t=np.arange(1,Nintervals+1)*intervalB
		print(t)
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( varianceChop[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.var(chopLens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_variance_chopZone_newer.pdf')
		else:
			plt.savefig('variance_chopZone_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( varianceI[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.var(Ilens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_variance_insertion_newer.pdf')
		else:
			plt.savefig('variance_insertion_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( varianceD[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.var(Dlens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_variance_deletion_newer.pdf')
		else:
			plt.savefig('variance_deletion_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( medianChop[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.median(chopLens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_median_chopZone_newer.pdf')
		else:
			plt.savefig('median_chopZone_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( medianI[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.median(Ilens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_median_insertion_newer.pdf')
		else:
			plt.savefig('median_insertion_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( medianD[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.median(Dlens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_median_deletion_newer.pdf')
		else:
			plt.savefig('median_deletion_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( numsChop[i]/float(numsM[i]))
		plt.plot(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(len(chopLens[i][model])/float(numM[i][model]))
		for model in range(len(models)):
			plt.plot(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_proportion_chopZone_newer.pdf')
		else:
			plt.savefig('proportion_chopZone_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( numsI[i]/float(numsM[i]))
		plt.plot(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(len(Ilens[i][model])/float(numM[i][model]))
		for model in range(len(models)):
			plt.plot(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_proportion_insertions_newer.pdf')
		else:
			plt.savefig('proportion_insertions_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( numsD[i]/float(numsM[i]))
		plt.plot(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(len(Dlens[i][model])/float(numM[i][model]))
		for model in range(len(models)):
			plt.plot(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_proportion_deletions_newer.pdf')
		else:
			plt.savefig('proportion_deletions_newer.pdf')
		plt.close()
		
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( numsID[i]/float(numsM[i]))
		plt.plot(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(len(IDlens[i][model])/float(numM[i][model]))
		for model in range(len(models)):
			plt.plot(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_proportion_indeletions_newer.pdf')
		else:
			plt.savefig('proportion_indeletions_newer.pdf')
		plt.close()
		
		
		yAxis=[]
		for i in range(Nintervals):
			#yAxis.append(( (numsI[i]-numsID[i])*meansI[i] + (numsD[i] - numsID[i])*meansD[i] + numsID[i]*(meansI[i]+meansD[i]) )/float(numsI[i]+numsD[i]-numsID[i]) )
			yAxis.append( meanChop[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.mean(chopLens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_chopZoneMeanLength_newer.pdf')
		else:
			plt.savefig('chopZoneMeanLength_newer.pdf')
		plt.close()
		
		
		yAxis=[]
		for i in range(Nintervals):
			yAxis.append( meansI[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.mean(Ilens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_InsertionMeanLength_newer.pdf')
		else:
			plt.savefig('insertionMeanLength_newer.pdf')
		plt.close()
		
		yAxis=[]
		for i in range(Nintervals):
			yAxis.append( meansD[i])
		plt.semilogy(t, yAxis, '|-', markersize=5.0, markeredgewidth=0.6, linewidth=0.6, label="simulated")
		idM=[[],[],[],[],[],[]]
		for i in range(Nintervals):
			for model in range(len(models)):
				if len(Ilens[i][model])>0:
					idM[model].append(np.mean(Dlens[i][model]))
		for model in range(len(models)):
			plt.semilogy(t[:len(idM[model])], idM[model], '--', label=models[model], color=colors[model])
		plt.legend()
		if TKF91case==1:
			plt.savefig('TKF91_DeletionMeanLength_newer.pdf')
		else:
			plt.savefig('deletionMeanLength_newer.pdf')
		plt.close()

exit()

