Scripts for running simulation and inference for the cumulative indel model manuscript.

pairHMM_parameters.py contains functions for calculating parameters of pairHMMs, including the cumulative indel model.

pairwise_alignment_new.py includes functions for executing pairHMM algoirthms (alignment and parameter inference, for example) once the parameters of the pairHMM are specified.

simulations_indelible_new.py and forwadList.py are scripts that have been used for the simulation study, and are hard-coded and not meant for public use, but they can help in testing reproducibility of the results.

human-chimp_align.py is a script that was used for analysis of the human-chimp synteny block.

hg38.panTro6_longestSynteny_alignment-80.0.txt is the file containing parameter inference, original alignment, and cumulative indel alignment using an adaptive banding threshold of 80 log-likelihood units.

Supplement.pdf is the Sepplementary materila of the manuscript.